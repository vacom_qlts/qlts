﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bảng kê tình hình sử dụng thiết bị</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        
        .x-form-group .x-form-group-header-text {
        	background-color: #dfe8f6;
        }
        
        .x-label-text {
            font-weight: bold;
            font-size: 11px;
        }
        
        .orange {
            background-color:Orange ;
            color:Black;
        }
        
        .green {
            background-color:Green;
            color:Black;
        }
        
        .yellow
        {
            background-color:Yellow ;
            color:Black;
        }  
        .silver
         {
            background-color:Silver;
            color:Black;
         } 
          
        
    </style> 

    <script type="text/javascript">

        function getRowClass(record) {
            if (record.data.TRANG_THAI === 2) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 3) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 4) {
                return 'orange';
            }

            if (record.data.TRANG_THAI === 5) {
                return 'yellow';
            }

        };

        function InBaoCao() {
            window.location.href = "/Report/HienBaoCao.aspx?bc=thsdtb&ts=";
        }

    </script>

    <script runat="server">
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"       
                TrackMouseOver="true">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>   
                        
                           <ext:Button ID="btnTinhHinhSuDungThietBi" runat="server" Text="In báo cáo" Icon="PrinterAdd">                                                   
                                <Listeners>
                                    <Click Handler="InBaoCao()" />
                                </Listeners>                                                                           
                            </ext:Button>
                                  
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server" />

                            <ext:ComboBox 
                                                                ID="MA_DV" 
                                                                runat="server"
                                                                DataIndex="MA_DV"
                                                                TriggerAction="All"                                                 
                                                                Mode="Local" ItemSelector="div.search-item"               
                                                                EnableKeyEvents="true"
                                                                DisplayField="TEN_DV"     
                                                                ValueField="MA_DV" 
                                                                FieldLabel="Điều kiện lọc"
                                                                Width="300"
                                                                EmptyText="Chọn đơn vị..."
                                                                >
                                                            
                                                                <Store>
                                                                <ext:Store 
                                                                ID="dsDonvi" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsDonvi"/>
                                                                </Proxy>        
                                                                                                                     
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_DV" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_DV" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_DV"/>                                                                                                
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>  
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                                                                </BaseParams>                                                                                                             
                                                            </ext:Store>
                                                        </Store> 
                                                                <Template ID="Template4" runat="server">
                                                           <Html>
                                                <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_DV}</h3>                 
                                                </div>
                                                </tpl>
                                               </Html>
                                                        </Template>    
                                                                <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                <Listeners> 
                                                                     <TriggerClick Handler="this.setValue('');#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');" />
                                                                     <Select  Handler="#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');"/>
                                                                </Listeners>                                                                                                                          
                                                                </ext:ComboBox>



                            <ext:ComboBox 
                                                                ID="MA_PB" 
                                                                runat="server"
                                                                DataIndex="MA_PB"
                                                                TriggerAction="All"                                                 
                                                                Mode="Local" ItemSelector="div.search-item"                
                                                                EnableKeyEvents="true"
                                                                DisplayField="TEN_PB"     
                                                                ValueField="MA_PB" 
                                                                Width="200"
                                                                EmptyText="Chọn phòng ban..."
                                                                >
                                                            
                                                                <Store>
                                                                <ext:Store 
                                                                ID="TimKiemPhongBanTheoMaDonVi" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/TimKiemPhongBanTheoMaDonVi"/>
                                                                </Proxy>        
                                                                                                                     
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_PB" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_PB" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_PB"/>                                                                                                
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>   
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                                                                </BaseParams>                                                                                                             
                                                            </ext:Store>
                                                        </Store> 
                                                                <Template ID="Template1" runat="server">
                                                           <Html>
                                                <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_PB}</h3>                 
                                                </div>
                                                </tpl>
                                               </Html>
                                                        </Template>    
                                                                <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                <Listeners> 
                                                                     <TriggerClick Handler="this.setValue('');" />
                                                                </Listeners>                                                                                                                          
                                                                </ext:ComboBox>


                            <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tìm kiếm theo mã hoặc thên người sử dụng..." 
                                EnableKeyEvents="true" Width="300px" >
                                <Listeners>
                                    <KeyDown Buffer="300" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                    <TriggerClick  Handler="#{dsTinhHinhSuDungThietBi}.reload();"/>
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                            <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsTinhHinhSuDungThietBi}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>   
                 
                <Store>
                    <ext:Store 
                        ID="dsTinhHinhSuDungThietBi" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true" GroupField="TEN_DV">
                        <Proxy>
                            <ext:HttpProxy Url="/Baocao/dsTinhHinhSuDungThietBi/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_TB" />
                                    <ext:RecordField Name="SERIAL" /> 
                                    <ext:RecordField Name="TEN_TB" /> 
                                    <ext:RecordField Name="MA_KIEU" /> 
                                    <ext:RecordField Name="TEN_KIEU" /> 
                                    <ext:RecordField Name="MA_CL" /> 
                                    <ext:RecordField Name="TEN_CL" /> 
                                    <ext:RecordField Name="NGAY_BH" Type="Date" /> 
                                    <ext:RecordField Name="THOI_GIAN_BH" />
                                    <ext:RecordField Name="THOI_GIAN_BHCL" />
                                    <ext:RecordField Name="NGAY_KH" Type="Date" />    
                                    <ext:RecordField Name="THOI_GIAN_KH" />
                                    <ext:RecordField Name="THOI_GIAN_KHCL" />
                                    <ext:RecordField Name="TRANG_THAI" />   
                                    <ext:RecordField Name="NGUYEN_GIA" />
                                    <ext:RecordField Name="TEN_TT" />  
                                    <ext:RecordField Name="TEN_DV" /> 
                                    <ext:RecordField Name="TEN_PB" /> 
                                    <ext:RecordField Name="TEN_NV" />                                                                                                                         
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="loai" Value="0" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />       
                            <ext:Parameter Name="donvi" Value="#{MA_DV}.getValue()" Mode="Raw" />  
                            <ext:Parameter Name="phongban" Value="#{MA_PB}.getValue()" Mode="Raw" />                              
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>

                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                         <ext:Column ColumnID="TEN_PB" DataIndex="TEN_PB" Header="Phòng ban" Width="150"  />
                        <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150"  />
                        <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="150"/>
                        <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="150">
                        </ext:Column>  
                        <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200"  />  
                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150"  />
                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150"  />

                        <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="150">
                        <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" /> 
                        </ext:Column>
                        <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" 
                        Header="Ngày bảo hành" Width="100" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_BHCL" DataIndex="THOI_GIAN_BHCL" Header="Thời gian bảo hành còn lại" Width="150"  Align="Right" />

                        <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" 
                        Header="Ngày khấu hao" Width="100" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_KHCL" DataIndex="THOI_GIAN_KHCL" Header="Thời gian khấu hao còn lại" Width="150"  Align="Right" />
                        <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="150"  />
                        <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Đơn vị" Width="150"  />
                    </Columns>
                </ColumnModel>
                
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>

                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                      
                            <ext:StringFilter DataIndex="TEN_TB"/>      
                            <ext:StringFilter DataIndex="TEN_CL"/>
                            <ext:StringFilter DataIndex="TEN_KIEU"/>
                            <ext:StringFilter DataIndex="THOI_GIAN_BHCL"/>       
                            <ext:StringFilter DataIndex="THOI_GIAN_KHCL"/>
                            <ext:StringFilter DataIndex="NGUYEN_GIA"/>                        
                        </Filters>
                    </ext:GridFilters>
                </Plugins>

                <View>
                       <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                                   <GetRowClass Fn="getRowClass" />      
                       </ext:GroupingView>
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="" />
                </Listeners>
                </ext:GridPanel>

            </Items>            
        </ext:Viewport>   
         
    </div>
</body>
</html>
