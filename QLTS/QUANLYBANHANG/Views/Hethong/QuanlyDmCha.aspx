﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register src="ChooserDialog.ascx" tagname="ChooserDialog" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Quản lý danh mục cha</title>
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" Mode="Script" runat="server" />
    <link rel="stylesheet" type="text/css" href="../../Resources/css/main.css" />
    <link rel="stylesheet" type="text/css" href="../../Resources/css/chooser.css" />
    <script type="text/javascript" src="/Resources/js/ChooserDialog.js"></script>
    <script runat="server">
        private List<object> GetImages(string path)
        {
            string serverPath = Server.MapPath(path);
            string[] files = System.IO.Directory.GetFiles(serverPath);

            List<object> data = new List<object>(files.Length);

            foreach (string fileName in files)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
                long size = fi.Length;
                string strSize = size < 1024 ? size + " bytes" : (Math.Round(((size * 10.0) / 1024)) / 10) + " KB";
                data.Add(new
                {
                    name = fi.Name,
                    url = path + fi.Name,
                    sizeString = strSize,
                    lastmod = fi.LastAccessTime
                });
            }

            return data;
        }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!X.IsAjaxRequest)
        {           
            this.Button2.Listeners.Click.Handler = string.Format("openDialog({0}_class,{1},{2});", this.ChooserDialog1.ClientID, this.Store1.ClientID, this.ChooserDialog1.WindowID);
            //this.Button2.Listeners.Click.Handler = string.Format("openDialog({0}_class,{1},{2});", this.ChooserDialog2.ClientID, this.Store1.ClientID, this.ChooserDialog2.WindowID);
        }
        string themes = Session["themes"].ToString();
        ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
    }

    protected void Store1_RefreshData(object sender, StoreRefreshDataEventArgs e)
    {
        this.Store1.DataSource = this.GetImages(e.Parameters["folder"]);
        this.Store1.DataBind();
    }
</script>
        
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: "Kiểm tra thông tin nhập!",
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsDanhmuc.reload();
            danhmucForm.reset();
            wdDanhmuc.hide();
            dmcForm.reset();
            wdDanhmucC.hide();
            dsDanhmucCon.reload();
            id.setValue('');
            id1.setValue('');
            btnSua.disable();
            btnXoa.disable();
            btnSua1.disable();
            btnXoa1.disable();
            ten_anh.setValue('');
        };
        
        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_DANHMUC');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            danhmucForm.getForm().loadRecord(record);
            wdDanhmuc.show();
        };

        var sua1 = function () {
            var s = GridPanel2.getSelectionModel().getSelected().get('MA_DMC');
            id1.setValue(s);
            var record = GridPanel2.getSelectionModel().getSelected();
            dmcForm.getForm().loadRecord(record);
            wdDanhmucC.show();
        };

        var insertImage = function (data) {
            Ext.DomHelper.append("images", {
                tag: "img",
                src: data.url,
                style: "margin:10px;visibility:hidden;"
            }, true).show(true).frame();
        };

        var openDialog = function (dialog, store, window) {
            store.directEventConfig.eventMask.customTarget = window.body;
            dialog.show();
        };

        var getSelectedNames = function (view) {
            var records = view.getSelectedRecords();
            var names = [];
            for (var i = 0, len = records.length; i < len; i++) {
                names[names.length] = records[i].data.name;
            }
            ten_anh.setValue(names);
            ten_anh.setValue(ten_anh.getValue().split('.', 1));            
            ImageChooserDialog.hide();
        };
         
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel12" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="TEN_DANHMUC_JP" RowHeight="0.4">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{danhmucForm}.reset();#{wdDanhmuc}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Hethong/xoaDanhmuc" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsDanhmuc}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_DANHMUC')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>                       
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsDanhmuc" 
                        runat="server"                         
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsQlDmCha/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_DANHMUC" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_DANHMUC" />
                                    <ext:RecordField Name="TEN_DANHMUC_VN" />
                                    <ext:RecordField Name="TEN_DANHMUC_US" />   
                                    <ext:RecordField Name="TEN_DANHMUC_CN"/>    
                                    <ext:RecordField Name="TEN_DANHMUC_JP" />                                                                                                                              
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>  
                        <SortInfo Field="MA_DANHMUC" Direction="ASC" />                                          
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_DANHMUC" DataIndex="MA_DANHMUC" Header="Mã danh mục" />  
                        <ext:Column ColumnID="TEN_DANHMUC_VN" DataIndex="TEN_DANHMUC_VN" Header="Tên danh mục VN" Width="150" />                    
                        <ext:Column ColumnID="TEN_DANHMUC_US" DataIndex="TEN_DANHMUC_US" Header="Tên danh mục US" Width="150"/>             
                        <ext:Column ColumnID="TEN_DANHMUC_CN" DataIndex="TEN_DANHMUC_CN" Header="Tên danh mục CN" Width="150" />
                        <ext:Column ColumnID="TEN_DANHMUC_JP" DataIndex="TEN_DANHMUC_JP" Header="Tên danh mục JP"  />                                               
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>
                            <ext:DateFilter DataIndex="MA_DANHMUC" />
                            <ext:StringFilter DataIndex="TEN_DANHMUC_VN" />
                            <ext:StringFilter DataIndex="TEN_DANHMUC_US" />  
                            <ext:StringFilter DataIndex="TEN_DANHMUC_CN" />
                            <ext:StringFilter DataIndex="TEN_DANHMUC_JP" />                          
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                    <ext:BufferView ID="BufferView1" runat="server" ScrollDelay="0" />
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="30" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>            
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
                <ext:Window ID="wdDanhmuc" runat="server" Hidden="true" Title="<%$Resources:lang, THONGTIN_DM %>" 
                Icon="Information" Width="420" Height="230" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="danhmucForm" runat="server" Height="200" Width="410"
                        Border="false" Url="/Hethong/saveDanhmuc" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />
                                        <ext:Hidden ID="ten_anh" runat="server" />
                                        <ext:TextField 
                                         ID="MA_DANHMUC" 
                                         DataIndex="MA_DANHMUC"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="60%"                            
                                         FieldLabel="Mã danh mục               "
                                         AllowBlank="false"/>

                                         <ext:TextField 
                                         ID="TEN_DANHMUC_VN"
                                         DataIndex="TEN_DANHMUC_VN"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên danh mục" EmptyText="Tiếng Việt..."
                                         AllowBlank="false"/>

                                         <ext:TextField 
                                         ID="TEN_DANHMUC_US" 
                                         DataIndex="TEN_DANHMUC_US"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"
                                         EmptyText="Tiếng Anh..."                                   
                                         AllowBlank="false"/>
                                         
                                         <ext:TextField 
                                         ID="TEN_DANHMUC_CN" 
                                         DataIndex="TEN_DANHMUC_CN"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         EmptyText="Tiếng Trung..."      
                                         AllowBlank="false"/>
                                         
                                         <ext:TextField 
                                         ID="TEN_DANHMUC_JP" 
                                         DataIndex="TEN_DANHMUC_JP"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         EmptyText="Tiếng Nhật..."      
                                         AllowBlank="false"/>  
                                         
                                                                                       
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server" />  
                            <ext:Button ID="Button2" runat="server" Text="Chọn icon" Icon="EmoticonHappy" StyleSpec="display:inline" >
                                
                            </ext:Button>                                                                            
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{danhmucForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{danhmucForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>

                <ext:GridPanel 
                ID="GridPanel2" Icon="BookOpenMark" 
                runat="server" Border="false" Title="Danh sách danh mục con"            
                TrackMouseOver="true" AutoExpandColumn="LINK_DMC" RowHeight="0.6">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar3" runat="server">
                        <Items>
                            <ext:Button ID="btn" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{dmcForm}.reset();#{wdDanhmucC}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua1" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua1" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa1" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Hethong/xoaDanhmucCon" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsDanhmucCon}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel2}.getSelectionModel().getSelected().get('MA_DMC')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>                       
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsDanhmucCon" 
                        runat="server"                         
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsDanhmuc/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_DMC" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_DMC" />
                                    <ext:RecordField Name="TEN_DMC_VN" />
                                    <ext:RecordField Name="TEN_DMC_EN" />   
                                    <ext:RecordField Name="TEN_DMC_CN"/>    
                                    <ext:RecordField Name="TEN_DMC_JP" />   
                                    <ext:RecordField Name="LINK_DMC" />                                                                                                                              
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>  
                        <SortInfo Field="MA_DMC" Direction="ASC" />                                          
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_DMC" DataIndex="MA_DMC" Header="Mã DM con" Width="80"/>  
                        <ext:Column ColumnID="TEN_DMC_VN" DataIndex="TEN_DMC_VN" Header="Tên dm con VN" Width="150" />                    
                        <ext:Column ColumnID="TEN_DMC_EN" DataIndex="TEN_DMC_EN" Header="Tên dm con US" Width="100"/>             
                        <ext:Column ColumnID="TEN_DMC_CN" DataIndex="TEN_DMC_CN" Header="Tên dm con CN" Width="100"/>
                        <ext:Column ColumnID="TEN_DMC_JP" DataIndex="TEN_DMC_JP" Header="Tên dm con JP"  Width="100"/> 
                        <ext:Column ColumnID="LINK_DMC" DataIndex="LINK_DMC" Header="Đường dẫn"  />                                               
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" >
                        <Listeners>
                             <RowSelect Handler="#{btnSua1}.enable(); #{btnXoa1}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                        <Filters>
                            <ext:DateFilter DataIndex="MA_DMC" />
                            <ext:StringFilter DataIndex="TEN_DMC_VN" />
                            <ext:StringFilter DataIndex="TEN_DMC_EN" />  
                            <ext:StringFilter DataIndex="TEN_DMC_CN" />
                            <ext:StringFilter DataIndex="TEN_DMC_JP" />  
                            <ext:StringFilter DataIndex="LINK_DMC" />                          
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                    <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="30" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>            
                    <RowClick Handler="#{btnSua1}.enable(); #{btnXoa1}.enable();" />
                </Listeners>              
                </ext:GridPanel>

                <ext:Window ID="wdDanhmucC" runat="server" Hidden="true" Title="<%$Resources:lang, THONGTIN_DM %>" 
                Icon="Information" Width="470" Height="260" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="dmcForm" runat="server" Height="230" Width="460"
                        Border="false" Url="/Hethong/saveDanhmucCon" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet2" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id1" runat="server" />
                                        <ext:TextField 
                                         ID="MA_DMC"  
                                         DataIndex="MA_DMC"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="60%"                            
                                         FieldLabel="Mã dm con"
                                         AllowBlank="false"/>

                                         <ext:TextField 
                                         ID="TEN_DMC_VN" 
                                         DataIndex="TEN_DMC_VN"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên dm con"
                                         EmptyText="Tiếng Việt..."
                                         AllowBlank="false"/>

                                         <ext:TextField 
                                         ID="TEN_DMC_EN"
                                         DataIndex="TEN_DMC_EN"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         EmptyText="Tiếng Anh..."
                                         AllowBlank="false"/>
                                         
                                         <ext:TextField 
                                         ID="TEN_DMC_CN" 
                                         DataIndex="TEN_DMC_CN"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         EmptyText="Tiếng Trung..."
                                         AllowBlank="false"/>
                                         
                                         <ext:TextField 
                                         ID="TEN_DMC_JP" 
                                         DataIndex="TEN_DMC_JP"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         EmptyText="Tiếng Nhật..."
                                         AllowBlank="false"/> 
                                         
                                         <ext:TextField 
                                         ID="LINK_DMC" 
                                         DataIndex="LINK_DMC"
                                         runat="server" IndicatorText="(*)"         
                                         IndicatorCls="red" AnchorHorizontal="90%"                          
                                         FieldLabel="Đường dẫn"
                                         AllowBlank="false"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar4" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="Button6" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{dmcForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button7" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{dmcForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua1}.disable(); #{btnXoa1}.disable();" />
                    </Listeners>
                </ext:Window>
            </Items>            
        </ext:Viewport> 
        <ext:Store 
            ID="Store1" 
            runat="server" 
            AutoLoad="false" 
            OnRefreshData="Store1_RefreshData">
            <Reader>
                <ext:JsonReader>
                    <Fields>
                        <ext:RecordField Name="name" />
                        <ext:RecordField Name="url" />      
                        <ext:RecordField Name="sizeString" />
                        <ext:RecordField Name="lastmod" Type="Date" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
            <DirectEventConfig>
                <EventMask ShowMask="true" Target="CustomTarget" />
            </DirectEventConfig>
        </ext:Store>       
        <uc1:ChooserDialog 
            ID="ChooserDialog1" 
            runat="server" 
            StoreID="Store1" 
            Callback="insertImage" 
            Folder="../Resources/Images/Icon/" />   
    </div>
</body>
</html>
