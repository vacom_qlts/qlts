﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />    
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: action.result.extraParams.msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhanvien.reload();
            nvForm.reset();
            wdNv.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_NHOM');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            wdNv.show();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };

        var selectedRow = function (record) {
            var recordindex = GridPanel3.store.indexOf(record);
            if (record.get('Check') == true)
                GridPanel3.getSelectionModel().selectRow(recordindex, true, false);

        };
</script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {          
            
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }            
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false"              
                TrackMouseOver="true" RowHeight="0.3" 
                AutoExpandColumn="GHICHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();#{wdNv}.show();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Hethong/xoaNhomUser" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsNhanvien}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NHOM')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>                       
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsNhanvien" 
                        runat="server" 
                        RemoteSort="true"  
                        UseIdConfirmation="true" >
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsNhomUser/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_NHOM" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_NHOM"/>                                    
                                    <ext:RecordField Name="TEN_NHOM" />                                                                           
                                    <ext:RecordField Name="GHICHU" />                                                                                                                                                                             
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="10000" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams> 
                        <SortInfo Field="MA_NHOM" Direction="DESC" />                                           
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />                           
                        <ext:Column ColumnID="TEN_NHOM" DataIndex="TEN_NHOM" Header="Tên nhóm" Width="180">
                        </ext:Column>              
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Diễn giải" >                           
                        </ext:Column>                                       
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="MA_NHOM" />
                            <ext:StringFilter DataIndex="TEN_NHOM" />                                                      
                            <ext:StringFilter DataIndex="GHICHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                    <ext:BufferView ID="GroupingView1" runat="server" />
                </View>                                        
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();#{GridPanel2}.setTitle('Chi tiết nhóm quyền ' + #{GridPanel1}.getSelectionModel().getSelected().get('TEN_NHOM'));#{Store1}.reload();"  />
                </Listeners>              
                </ext:GridPanel>
                
                <ext:GridPanel 
                ID="GridPanel2" 
                runat="server" Icon="GroupAdd" Title="Chi tiết nhóm quyền"              
                TrackMouseOver="true" RowHeight="0.7" >    
                <TopBar>
                    <ext:Toolbar ID="Toolbar3" runat="server">
                        <Items>
                            <ext:Button ID="Button2" runat="server" Text="Chỉnh sửa" Icon="PageEdit">                                                   
                                <Listeners>
                                    <Click Handler="#{winPhanQuyen}.show();#{Store2}.reload();" />
                                </Listeners>                                                                             
                            </ext:Button>                                               
                                                 
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="Store1" 
                        runat="server" 
                        RemoteSort="true"  
                        UseIdConfirmation="true" >
                        <Proxy>
                            <ext:HttpProxy Url="/Hethong/dsCTNhomUser/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_CT" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_CT"/>
                                    <ext:RecordField Name="MA_NHOM"/>                                    
                                    <ext:RecordField Name="TEN_NHOM" />                                                                           
                                    <ext:RecordField Name="MA_DMC" />       
                                    <ext:RecordField Name="TEN_DMC" />       
                                    <ext:RecordField Name="THEM" />       
                                    <ext:RecordField Name="SUA" />        
                                    <ext:RecordField Name="XOA" />       
                                    <ext:RecordField Name="USER" />                                                                                                                                                                            
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="limit" Value="10000" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />  
                            <ext:Parameter Name="ma_nhom" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NHOM')" Mode="Raw" />                            
                        </BaseParams> 
                        <SortInfo Field="MA_NHOM" Direction="DESC" />                                           
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />     
                        <ext:Column ColumnID="TEN_DMC" DataIndex="TEN_DMC" Header="Danh mục" Width="250">
                        </ext:Column>                         
                        <ext:Column ColumnID="TEN_NHOM" DataIndex="TEN_NHOM" Header="Tên nhóm" Align="Center">
                        </ext:Column>              
                        <ext:CheckColumn ColumnID="THEM" DataIndex="THEM" Header="Thêm" Width="50" Align="Center"/>                           
                     
                        <ext:CheckColumn ColumnID="SUA" DataIndex="SUA" Header="Sửa" Width="50" Align="Center"/>   
                        <ext:CheckColumn ColumnID="XOA" DataIndex="XOA" Header="Xóa" Width="50" Align="Center"/>   
                        <ext:CheckColumn ColumnID="USER" DataIndex="USER" Header="User" Width="50" Align="Center"/>                                 
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="MA_NHOM" />
                            <ext:StringFilter DataIndex="TEN_NHOM" />                                                      
                            <ext:StringFilter DataIndex="GHICHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                    <ext:BufferView ID="BufferView1" runat="server" />
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="15" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
        
        <ext:Window ID="wdNv" runat="server" Hidden="true" Title="Thông tin nhóm người dùng" 
                Icon="Information" Width="450" Height="200" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="nvForm" runat="server" Height="170" Width="440"
                        Border="false" Url="/Hethong/saveNhomUser" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />        
                                         
                                         <ext:TextField 
                                         ID="TEN_NHOM" IndicatorText="*"   
                                         DataIndex="TEN_NHOM" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Tên nhóm"/>                                    
                                         
                                         <ext:TextArea 
                                         ID="GHICHU" 
                                         DataIndex="GHICHU"
                                         runat="server"        
                                         FieldLabel="Diễn giải"
                                         AnchorHorizontal="95%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill1" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{nvForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window> 

        <ext:Window ID="winPhanQuyen" Modal="true" Hidden="true" runat="server" Title="<center>Phân quyền</center>" Height="440" Width="600" >
        <Items>           
            <ext:TabPanel  ID="tabPanel1" runat="server" AnchorHorizontal="100%"   AnchorVertical="100%" >
                    <Items>
                        <ext:Panel runat="server" ID="panelFunction" Height="380"  AnchorHorizontal="100%" Title="Phân quyền chức năng" Layout="FitLayout">
                           <Items>
                                <ext:GridPanel  
                                 ID="GridPanel3" 
                                 runat="server" 
                                 Header="false"
                                 Border="false"
                                 Selectable="true"
                                 TrackMouseOver="true"  AnchorHorizontal="100%" AutoWidth="true"  >                                 
                                <Store>
                                  <ext:Store ID="Store2" runat="server" GroupField="TEN_DANHMUC_VN" RefreshAfterSaving="Always" WarningOnDirty="false">
                                    <Proxy>
                                        <ext:HttpProxy Url="/Hethong/dsCTQuyen/" />
                                    </Proxy>
                                    <Reader>
                                                <ext:JsonReader IDProperty="MA_DMC" Root="data" TotalProperty="total" >
                                                    <Fields>
                                                        <ext:RecordField Name="Check" />
                                                        <ext:RecordField Name="MA_DMC" />
                                                        <ext:RecordField Name="TEN_DMC_VN" />
                                                        <ext:RecordField Name="MA_DANHMUC" />
                                                        <ext:RecordField Name="TEN_DANHMUC_VN" />
                                                        <ext:RecordField Name="NEW" Type="Boolean"  />
                                                        <ext:RecordField Name="EDIT" Type="Boolean" />
                                                        <ext:RecordField Name="DEL" Type="Boolean" />
                                                        <ext:RecordField Name="USER" Type="Boolean" />
                                                        <ext:RecordField Name="LINK_DMC" />
                                                    </Fields>
                                                </ext:JsonReader>
                                    </Reader>
                                    <BaseParams>
                                        <ext:Parameter Name="ma_nhom" Mode="Raw" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NHOM')" />
                                    </BaseParams>                                    
                                    </ext:Store>
                                </Store>
                                <ColumnModel ID="ColumnModel3" runat="server">
                                        <Columns>
                                            <ext:CheckColumn ColumnID="Check"  Hidden="true" DataIndex="Check" Header="Check" >                                            
                                            </ext:CheckColumn>
                                            <ext:Column ColumnID="MenuID" Hidden="true" DataIndex="MA_DANHMUC" Header="Danh mục gốc" Width="100" />                                            
                                            <ext:Column ColumnID="MenuName" DataIndex="TEN_DANHMUC_VN" Header="Danh mục" Width="100" />
                                            <ext:Column ColumnID="MenuItemName" DataIndex="TEN_DMC_VN" Header="Chức năng" Width="600" />
                                            <ext:CheckColumn ColumnID="New" DataIndex="NEW" Editable="true" Header="Thêm" />
                                            <ext:CheckColumn ColumnID="Edit" DataIndex="EDIT" Editable="true" Header="Sửa" />
                                            <ext:CheckColumn ColumnID="Del" DataIndex="DEL" Editable="true" Header="Xóa" />
                                            <ext:CheckColumn ColumnID="User" DataIndex="USER" Editable="true" Header="User" />
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel >
                                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server"  />                                                               
                                    </SelectionModel>                
                                    <View>
                                       <ext:GroupingView  
                                            ID="GroupingView2"
                                            HideGroupedColumn="true"
                                            ShowGroupName="false"
                                            runat="server" 
                                            ForceFit="true"
                                            StartCollapsed="false"
                                            GroupTextTpl='<span id="ColorCode-{[values.rs[0].data.ColorCode]}"></span>{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                                            EnableRowBody="true">
                                         <GetRowClass Fn="selectedRow" />
                                        </ext:GroupingView>
                                     </View>
                                    <LoadMask ShowMask="true" />
                                    <SaveMask ShowMask="true" />
                                    <BottomBar>
                                        <ext:Toolbar ID="Toolbar4" runat="server">
                                            <Items>
                                                <ext:ToolbarFill />
                                                <ext:Button ID="btnNhan" Text="Ghi" runat="server" Icon="Disk" >
                                                    <DirectEvents>
                                                        <Click 
                                                        Url="/Hethong/AddQuyenUser" 
                                                        CleanRequest="true"
                                                        Method="POST"
                                                        Failure="Ext.Msg.show({title:'Error',msg: result.errorMessage,buttons: Ext.Msg.OK,icon: Ext.Msg.ERROR});" 
                                                        Success="#{winPhanQuyen}.hide(); #{Store1}.reload();">
                                                        <ExtraParams>                                                            
                                                            <ext:Parameter Name="ma_nhom" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NHOM')" Mode="Raw" />
                                                            <ext:Parameter Name="json" Value="Ext.encode(#{GridPanel3}.getRowsValues({selectedOnly:true}))" Mode="Raw" />
                                                        </ExtraParams>
                                                    </Click>
                                                    </DirectEvents>
                                                </ext:Button>                                               
                                            </Items>
                                        </ext:Toolbar>
                                    </BottomBar>
                            </ext:GridPanel>
                           </Items>
                        </ext:Panel>
                    </Items>
                </ext:TabPanel>                
        </Items>
   </ext:Window>
    </div>
</body>
</html>



