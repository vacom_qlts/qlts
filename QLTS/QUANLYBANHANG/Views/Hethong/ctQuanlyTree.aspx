﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="System.Collections.Generic" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>  
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />  
    <script runat="server">       
    protected void btnSave_Click(object sender, DirectEventArgs e)
    {        
        List<string> list;        
        if (Session["check"] == null)
        {
            list = new List<string>();
        }
        else
        {
            list = (List<string>)Session["check"];
        }
       
        RowSelectionModel sm = this.GridPanel2.SelectionModel.Primary as RowSelectionModel;

        foreach (SelectedRow row in sm.SelectedRows)
        {
            string s = row.RecordID;
            list.Add(s);
        }
        this.Session["check"] = list;
        X.Js.Call("check");        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string themes = Session["themes"].ToString();
        ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
    }
</script>

    <script type="text/javascript">
        var check = function () {
            Ext.Ajax.request({
                url: '/Hethong/getCheck',
                params: { madm: MA_DANHMUC.getValue(), kieu: KIEU_DM.getValue(), kieukhac_vn: KIEU_DM_KHAC_VN.getValue(), kieukhac_en: KIEU_DM_KHAC_EN.getValue(), kieukhac_cn: KIEU_DM_KHAC_CN.getValue(), kieukhac_jp: KIEU_DM_KHAC_JP.getValue() },
                method: 'GET',
                success: function (response, request) {
                    var jsonData = Ext.util.JSON.decode(response.responseText);
                    parent.wdDm.hide();
                    parent.dsDanhmuc.reload();
                    
                },
                failure: function (result, request) {
                    alert('Lỗi!');
                }
            });
        };   
</script>
   
</head>
<body>
    <form id="Form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />    
               
        <ext:Panel ID="wdDanhmuc" runat="server" 
                Width="483" Height="465" Resizable="false" Layout="RowLayout">
                    <Items>
                        <ext:FormPanel ID="danhmucForm" runat="server" RowHeight="0.28"
                        Border="false" Url="/Danhmuc/saveDanhmucCT" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />
                                        <ext:ComboBox 
                                                    ID="MA_DANHMUC" 
                                                    runat="server"
                                                    DataIndex="MA_DANHMUC"
                                                    TriggerAction="All"
                                                    FieldLabel="Danh mục gốc"
                                                    Mode="Local"
                                                    AnchorHorizontal="95%"
                                                    DisplayField="TEN_DANHMUC_VN"
                                                    ValueField="MA_DANHMUC"
                                                    >
                                                    <Store>
                                                        <ext:Store 
                                                            ID="dsQlDmCha" 
                                                            runat="server" 
                                                            RemoteSort="true" 
                                                            UseIdConfirmation="true" >
                                                            <Proxy>
                                                                <ext:HttpProxy Url="/Hethong/dsQlDmCha/" />
                                                            </Proxy>                                                            
                                                            <Reader>
                                                                <ext:JsonReader IDProperty="MA_DANHMUC" Root="data" TotalProperty="total">
                                                                    <Fields>
                                                                        <ext:RecordField Name="MA_DANHMUC" SortDir="ASC"/>
                                                                        <ext:RecordField Name="TEN_DANHMUC_VN" />                                    
                                                                    </Fields>
                                                                </ext:JsonReader>
                                                            </Reader>
                                                            <BaseParams>
                                                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                                                            </BaseParams>                                                                                                                                                                                  
                                                        </ext:Store>                                                        
                                                    </Store>             
                                                    <Listeners>
                                                         <Select Handler="#{stKieudm}.reload();#{dsDanhmuc}.reload();" />
                                                    </Listeners>                                        
                                                  </ext:ComboBox>
                                        <ext:ComboBox 
                                                    ID="KIEU_DM" 
                                                    runat="server"
                                                    DataIndex="KIEU_DM"
                                                    TriggerAction="All"
                                                    FieldLabel="Kiểu danh mục"
                                                    Mode="Local"
                                                    AnchorHorizontal="95%"
                                                    DisplayField="KIEU_DM_VN"
                                                    ValueField="KIEU_DM_VN"
                                                    >
                                                    <Store>
                                                        <ext:Store 
                                                            ID="stKieudm" 
                                                            runat="server" 
                                                            RemoteSort="true" 
                                                            UseIdConfirmation="true" >
                                                            <Proxy>
                                                                <ext:HttpProxy Url="/Hethong/dsKieuDm/" />
                                                            </Proxy>                                                            
                                                            <Reader>
                                                                <ext:JsonReader IDProperty="KIEU_DM_VN" Root="data" TotalProperty="total">
                                                                    <Fields>
                                                                        <ext:RecordField Name="KIEU_DM_VN" SortDir="ASC"/>                                                                                                        
                                                                    </Fields>
                                                                </ext:JsonReader>
                                                            </Reader>
                                                            <BaseParams>
                                                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />   
                                                                <ext:Parameter Name="dm" Value="#{MA_DANHMUC}.getValue()" Mode="Raw" />                            
                                                            </BaseParams>                                                                                                                        
                                                        </ext:Store>
                                                    </Store> 
                                                    <Triggers>
                                                        <ext:FieldTrigger Icon="SimpleAdd" Qtip="Kiểu danh mục khác" />
                                                    </Triggers> 
                                                    <Listeners>
                                                        <TriggerClick Handler="#{KIEU_DM_KHAC}.enable();#{CompositeField1}.enable();" />
                                                    </Listeners>                                                  
                                                  </ext:ComboBox> 
                                                  <ext:CompositeField runat="server" Disabled="true"
                                                  AnchorHorizontal="95%" ID="KIEU_DM_KHAC" FieldLabel="Kiểu khác">
                                                        <Items>
                                                            <ext:TextField 
                                                                 ID="KIEU_DM_KHAC_VN"                                                  
                                                                 runat="server" Flex="1"
                                                                 EmptyText="Tiếng Việt..."/>   
                                                            <ext:TextField 
                                                                 ID="KIEU_DM_KHAC_EN"                                                  
                                                                 runat="server" Flex="1"
                                                                 EmptyText="Tiếng Anh..."/>   
                                                        </Items>
                                                  </ext:CompositeField>  
                                                  <ext:CompositeField runat="server" Disabled="true"
                                                  AnchorHorizontal="95%" ID="CompositeField1">
                                                        <Items>
                                                            <ext:TextField 
                                                                 ID="KIEU_DM_KHAC_CN"                                                  
                                                                 runat="server" Flex="1"
                                                                 EmptyText="Tiếng Trung..."/>   
                                                            <ext:TextField 
                                                                 ID="KIEU_DM_KHAC_JP"                                                  
                                                                 runat="server" Flex="1"
                                                                 EmptyText="Tiếng Nhật..."/>   
                                                        </Items>
                                                  </ext:CompositeField>                                                                                                                                                 
                                       </Items>
                                </ext:FieldSet>                    
                            </Items>                                                
                        </ext:FormPanel>
                        <ext:GridPanel  
                             ID="GridPanel2"    
                             runat="server" StripeRows="true" 
                             RowHeight="0.72" Border="false"
                             Title="Danh sách danh mục con" Icon="BookOpenMark"
                             Selectable="true" AutoExpandColumn="TEN_DMC_VN"
                             TrackMouseOver="true">                      
                            <Store>
                                <ext:Store 
                                        ID="dsDanhmuc" 
                                        runat="server" 
                                        RemoteSort="true" 
                                        UseIdConfirmation="true">
                                        <Proxy>
                                            <ext:HttpProxy Url="/Hethong/dsDanhmuc/" />
                                        </Proxy>                        
                                        <Reader>
                                            <ext:JsonReader IDProperty="MA_DMC" Root="data" TotalProperty="total">
                                                <Fields>
                                                    <ext:RecordField Name="MA_DMC" SortDir="ASC"/>
                                                    <ext:RecordField Name="TEN_DMC_VN" />                                                                                     
                                                </Fields>
                                            </ext:JsonReader>
                                        </Reader>
                                        <BaseParams>
                                            <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                            <ext:Parameter Name="start" Value="0" Mode="Raw" />  
                                            <ext:Parameter Name="ma" Value="#{MA_DANHMUC}.getValue()" Mode="Raw" />                            
                                        </BaseParams>
                                        <SortInfo Field="MA_DMC" Direction="ASC" />      
                                        <DirectEventConfig IsUpload="true" />
                                </ext:Store>
                            </Store>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                    <Columns>
                                        <ext:RowNumbererColumn />                        
                                        <ext:Column ColumnID="MA_DMC" DataIndex="MA_DMC" Header="Mã danh mục con" />
                                        <ext:Column ColumnID="TEN_DMC_VN" DataIndex="TEN_DMC_VN" Header="Tên danh mục con"/>                                                                                                             
                                    </Columns>
                             </ColumnModel>      
                             <Plugins>
                                    <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                                        <Filters>
                                            <ext:StringFilter DataIndex="MA_DMC" />
                                            <ext:StringFilter DataIndex="TEN_DMC_VN" />                                                                               
                                        </Filters>
                                    </ext:GridFilters>
                             </Plugins> 
                            <BottomBar>
                                <ext:PagingToolbar ID="PagingToolBar2" runat="server" PageSize="100" DisplayInfo="false" />
                            </BottomBar>
                            <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" />
                            </SelectionModel>            
                        </ext:GridPanel>        
                    </Items>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">  
                             <Items>                                                                                                        
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                               
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                        </ext:Toolbar>
                    </TopBar>                    
        </ext:Panel>        
    </form>
  </body>
</html>
    