﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
    </style>
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhapKho.reload();
            nkForm.reset();
            wdNhapKho.hide();
            id.setValue('');
            btnSua.disable();
        };

        var successHandler1 = function (form, action) {
            Store_dsThietBi.reload();
            dmtbForm.reset();
            wdDMTB.hide();
        };

        var sua = function () {
            var s = gpNhapKho.getSelectionModel().getSelected().get('MA_NK');
            id.setValue(s);
            var record = gpNhapKho.getSelectionModel().getSelected();
            nkForm.getForm().loadRecord(record);
            wdNhapKho.show();
            MA_NK.disable();
            Store_TimKiemThietBiTheoMaNhapKho.reload();
            Store_dsThietBi.reload();
        };



        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };

        var ChonLua = function () {
            var data = gpDanhSachThietBi.getRowsValues({ selectedOnly: true });
            $.each(data, function (i, r) {
                gpDanhSachThietBiDaChon.insertRecord(0,
                     {

                         MA_TB: r.MA_TB, TEN_TB: r.TEN_TB, SERIAL: r.SERIAL,
                         NGUYEN_GIA: r.NGUYEN_GIA, MA_CL: r.MA_CL, TEN_CL: r.TEN_CL, MA_KIEU: r.MA_KIEU,
                         TEN_KIEU: r.TEN_KIEU, TRANG_THAI: r.TRANG_THAI, TEN_TT: r.TEN_TT,
                         NGAY_BH: r.NGAY_BH, THOI_GIAN_BH: r.THOI_GIAN_BH, NGAY_KH: r.NGAY_KH, THOI_GIAN_KH: r.THOI_GIAN_KH
                     });
            });

            var sm = gpDanhSachThietBi.getSelectionModel();
            var sel = sm.getSelections();
            for (i = 0; i < sel.length; i++) {
                gpDanhSachThietBi.store.remove(sel[i]);
            }

            wdDanhSachThietBi.hide();
        }





        var Huy = function () {
            var data = gpDanhSachThietBiDaChon.getRowsValues({ selectedOnly: true });
            $.each(data, function (i, r) {
                gpDanhSachThietBi.insertRecord(0,
                     {
                         MA_TB: r.MA_TB, TEN_TB: r.TEN_TB, SERIAL: r.SERIAL,
                         NGUYEN_GIA: r.NGUYEN_GIA, MA_CL: r.MA_CL, TEN_CL: r.TEN_CL, MA_KIEU: r.MA_KIEU,
                         TEN_KIEU: r.TEN_KIEU, TRANG_THAI: r.TRANG_THAI, TEN_TT: r.TEN_TT,
                         NGAY_BH: r.NGAY_BH, THOI_GIAN_BH: r.THOI_GIAN_BH, NGAY_KH: r.NGAY_KH, THOI_GIAN_KH: r.THOI_GIAN_KH
                     });
            });

            var sm = gpDanhSachThietBiDaChon.getSelectionModel();
            var sel = sm.getSelections();
            for (i = 0; i < sel.length; i++) {
                gpDanhSachThietBiDaChon.store.remove(sel[i]);
            }
        }
        var ThemMoiChungLoai = function () {
            wdChungLoai.show();
            MA_CL.setValue('');
            TimKiemKieuThemMaChungLoai.reload();
            MA_KIEU.setValue('');
        };


        var ThemMoiKieu = function () {
            wdKieu.show();
            MA_CL_KIEU.setValue(MA_CL.getValue());
        };


        var successHandler_chungloai = function () {
            dsChungloai.reload();
            setTimeout(function () {
                MA_CL.setValue(MA_CL_TB.getValue());
                clForm.reset();
                wdChungLoai.hide();
                btnThemMoiKieu.enable();
            }, 1000);

        };


        var successHandler_kieu = function (form, action) {
            TimKiemKieuThemMaChungLoai.reload();
            setTimeout(function () {
                MA_KIEU.setValue(action.result.extraParams.MA_KIEU);
                kieuForm.reset();
                wdKieu.hide();
            }, 1000);
        };

        function MaPhieu() {
            $.post("/Danhmuc/TaoMaTuDong", { Ma: "NK" }, function (data) {
                MA_NK.setValue(data);
            }, "json");
        }
    </script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }

            this.dsCHAT_LUONG.DataSource = new object[]
            {
                new object[] {1,"Tốt"},
                new object[] {2,"Ổn định"},
                new object[] {3,"Hư hỏng"}    
            };
            this.dsCHAT_LUONG.DataBind();
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:GridPanel ID="gpNhapKho" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">
                                    <Listeners>
                                        <Click Handler="#{nkForm}.reset();#{wdNhapKho}.show();#{MA_NK}.disable();#{id}.setValue('');
                                    #{Store_TimKiemThietBiTheoMaNhapKho}.reload();
                                    #{Store_dsThietBi}.reload();MaPhieu();
                                    " />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>"
                                    Icon="BookEdit">
                                    <Listeners>
                                        <Click Fn="sua" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Tìm kiếm theo mã nhập kho hoặc mã nhân viên..."
                                    EnableKeyEvents="true" Width="400px">
                                    <Listeners>
                                        <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["id"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsNhapKho}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsNhapKho" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsNhapKho/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_NK" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_NK" />
                                        <ext:RecordField Name="MA_HD" />
                                        <ext:RecordField Name="MA_DVBH" />
                                        <ext:RecordField Name="MA_NKP" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="MA_DVBH" />
                                        <ext:RecordField Name="NGAY_NHAP" Type="Date" />
                                        <ext:RecordField Name="NGAY_XUAT" Type="Date" />
                                        <ext:RecordField Name="CHAT_LUONG" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                        <ext:RecordField Name="TEN_HD" />
                                        <ext:RecordField Name="NGAY_BD" Type="Date" />
                                        <ext:RecordField Name="NGAY_KT" Type="Date" />
                                        <ext:RecordField Name="TEN_NKP" />
                                        <ext:RecordField Name="TEN_DVBH" />
                                        <ext:RecordField Name="TEN_NV" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                <ext:Parameter Name="loai" Value="1" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_NK" DataIndex="MA_NK" Header="Mã số" Width="100" />
                            <ext:DateColumn ColumnID="NGAY_NHAP" DataIndex="NGAY_NHAP" Header="Ngày nhập kho"
                                Width="100" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="TEN_HD" DataIndex="TEN_HD" Header="Hợp đồng" Width="150" />
                            <ext:Column ColumnID="TEN_DVBH" DataIndex="TEN_DVBH" Header="Đơn vị bảo hành" Width="150" />
                            <ext:Column ColumnID="TEN_NKP" DataIndex="TEN_NKP" Header="Nguồn kinh phí" Width="150" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người nhập kho" Width="150" />
                            <ext:Column ColumnID="CHAT_LUONG" DataIndex="CHAT_LUONG" Header="Chất lượng" Width="100" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="#{btnSua}.enable(); setTimeout(function(){TimKiemThietBiTheoMaNhapKho.reload();},500);
                             #{id}.setValue(#{gpNhapKho}.getSelectionModel().getSelected().get('MA_NK'));" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_HD" />
                                <ext:StringFilter DataIndex="TEN_DBVH" />
                                <ext:StringFilter DataIndex="TEN_NV" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView1" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="#{btnSua}.enable(); setTimeout(function(){TimKiemThietBiTheoMaNhapKho.reload();},500);
                    #{id}.setValue(#{gpNhapKho}.getSelectionModel().getSelected().get('MA_NK'));" />
                    </Listeners>
                </ext:GridPanel>
                <ext:GridPanel ID="gpTimKiemThietBiTheoMaNhapKho" RowHeight="0.4" Title="Chi tiết Nhập kho"
                    runat="server" Icon="Information" TrackMouseOver="true">
                    <Store>
                        <ext:Store ID="TimKiemThietBiTheoMaNhapKho" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/TimKiemThietBiTheoMaNhapKho/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_CT" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_CT" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="NGAY_BH" Type="Date" />
                                        <ext:RecordField Name="THOI_GIAN_KH" />
                                        <ext:RecordField Name="NGAY_TAO" Type="Date" />
                                        <ext:RecordField Name="NGAY_SUA" />
                                        <ext:RecordField Name="CHAT_LUONG" />
                                        <ext:RecordField Name="NGUYEN_GIA" />
                                        <ext:RecordField Name="THOI_GIAN_KH" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="TEN_TT" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                <ext:Parameter Name="maNK" Value="#{id}.getValue()" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã TB" Width="80" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên TB" Width="150" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="150" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                            <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" Header="Ngày KH" Width="100" />
                            <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian KH"
                                Width="100" />
                            <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá">
                                <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" />
                            </ext:Column>
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="MA_CL" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdNhapKho" runat="server" Hidden="true" Title="Nhập kho" Icon="Information"
            Width="810" Height="550">
            <Items>
                <ext:FormPanel ID="nkForm" runat="server" Height="520" Width="800" Border="false"
                    ButtonAlign="Center" Url="/Danhmuc/saveNhapkho">
                    <Items>
                        <ext:Hidden ID="id" runat="server" />
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:CompositeField ID="CompositeField2" runat="server" FieldLabel="Số phiếu" AnchorHorizontal="44.5%">
                                    <Items>
                                        <ext:TextField ID="MA_NK" DataIndex="MA_NK" runat="server" IndicatorText="*" AllowBlank="false"
                                            IndicatorCls="red" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField3" runat="server" FieldLabel="Nguồn kinh phí"
                                    AnchorHorizontal="98%">
                                    <Items>
                                        <ext:ComboBox ID="MA_NKP" runat="server" DataIndex="MA_NKP" TriggerAction="All" Mode="Local"
                                            ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                            EnableKeyEvents="true" DisplayField="TEN_NKP" PageSize="10" ValueField="MA_NKP"
                                            Flex="3" EmptyText="Chọn nguồn kinh phí...">
                                            <Store>
                                                <ext:Store ID="dsNguonKinhPhi" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/dsNguonKinhPhi" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_NKP" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_NKP" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_NKP" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_NKP}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="10" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template1" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_NKP}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <ext:DisplayField ID="DisplayField1" runat="server" Margins="0 0 0 10" Text="Chất lượng"
                                            Flex="2" />
                                        <ext:ComboBox ID="CHAT_LUONG" AllowBlank="false" DataIndex="CHAT_LUONG" runat="server"
                                            EmptyText="Chọn chất lượng..." Mode="Local" ValueField="name_v" DisplayField="name_d"
                                            Flex="3">
                                            <Store>
                                                <ext:Store ID="dsCHAT_LUONG" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Reader>
                                                        <ext:ArrayReader>
                                                            <Fields>
                                                                <ext:RecordField Name="name_v" />
                                                                <ext:RecordField Name="name_d" />
                                                            </Fields>
                                                        </ext:ArrayReader>
                                                    </Reader>
                                                    <SortInfo Field="name_v" Direction="ASC" />
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField1" runat="server" FieldLabel="Hợp đồng" AnchorHorizontal="98%">
                                    <Items>
                                        <ext:ComboBox ID="MA_HD" runat="server" DataIndex="MA_HD" TriggerAction="All" Mode="Local"
                                            ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                            EnableKeyEvents="true" DisplayField="TEN_HD" ValueField="MA_HD" PageSize="10"
                                            Flex="3" EmptyText="Chọn hợp đồng...">
                                            <Store>
                                                <ext:Store ID="dsHopdong" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/dsHopdong" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_HD" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_HD" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_HD" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_HD}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="10" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template4" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_HD}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <ext:DisplayField ID="DisplayField2" runat="server" Margins="0 0 0 10" Text="Đơn vị bảo hành"
                                            Flex="2" />
                                        <ext:ComboBox ID="MA_DVBH" runat="server" DataIndex="MA_DVBH" TriggerAction="All"
                                            Mode="Local" ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false"
                                            IndicatorCls="red" EnableKeyEvents="true" DisplayField="TEN_DVBH" ValueField="MA_DVBH"
                                            Flex="3" EmptyText="Chọn đơn bảo hành...">
                                            <Store>
                                                <ext:Store ID="dsDonViBaoHanh" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/dsDonViBaoHanh" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_DVBH" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_DVBH" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_DVBH" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_DVBH}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="10" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template2" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_DVBH}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:CompositeField>
                            </Items>
                        </ext:FieldSet>
                        <ext:Container ID="Container1" runat="server" Layout="HBoxLayout">
                            <Items>
                                <ext:FieldSet ID="FieldSet3" runat="server" Margins="0 0 0 0" Flex="1" Border="false"
                                    Layout="AnchorLayout" Width="797" Height="380">
                                    <Items>
                                        <ext:GridPanel ID="gpDanhSachThietBiDaChon" RowHeight="0.6" Title="Danh sách thiết bị đã chọn"
                                            runat="server" Border="true" Height="370" Selectable="true" Header="true" TrackMouseOver="true">
                                            <TopBar>
                                                <ext:Toolbar ID="Toolbar4" runat="server">
                                                    <Items>
                                                        <ext:Button ID="btnHuy" runat="server" Text="Hủy" Icon="Delete" IconAlign="Left">
                                                            <Listeners>
                                                                <Click Fn="Huy" />
                                                            </Listeners>
                                                        </ext:Button>
                                                        <ext:ToolbarFill ID="ToolbarFill8" runat="server" />
                                                        <ext:Button ID="Button10" runat="server" Text="Chọn thiết bị" Icon="Accept" IconAlign="Left">
                                                            <Listeners>
                                                                <Click Handler="#{wdDanhSachThietBi}.show();" />
                                                            </Listeners>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:Toolbar>
                                            </TopBar>
                                            <Store>
                                                <ext:Store ID="Store_TimKiemThietBiTheoMaNhapKho" runat="server" RemoteSort="false"
                                                    UseIdConfirmation="false" WarningOnDirty="false" GroupField="TEN_CL">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/TimKiemThietBiTheoMaNhapKho/" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="Check" />
                                                                <ext:RecordField Name="MA_TB" />
                                                                <ext:RecordField Name="TEN_TB" />
                                                                <ext:RecordField Name="SERIAL" />
                                                                <ext:RecordField Name="NGUYEN_GIA" />
                                                                <ext:RecordField Name="MA_CL" />
                                                                <ext:RecordField Name="TEN_CL" />
                                                                <ext:RecordField Name="MA_KIEU" />
                                                                <ext:RecordField Name="TEN_KIEU" />
                                                                <ext:RecordField Name="TRANG_THAI" />
                                                                <ext:RecordField Name="TEN_TT" />
                                                                <ext:RecordField Name="NGAY_BH" Type="Date" />
                                                                <ext:RecordField Name="THOI_GIAN_BH" />
                                                                <ext:RecordField Name="NGAY_KH" Type="Date" />
                                                                <ext:RecordField Name="THOI_GIAN_KH" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                        <ext:Parameter Name="maNK" Value="#{id}.getValue()" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <ColumnModel ID="ColumnModel4" runat="server">
                                                <Columns>
                                                    <ext:CheckColumn ColumnID="Check" Hidden="true" DataIndex="Check" Header="Check">
                                                    </ext:CheckColumn>
                                                    <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100" />
                                                    <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200" />
                                                    <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100" />
                                                    <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100">
                                                        <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" />
                                                    </ext:Column>
                                                    <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                                                    <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200" />
                                                    <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200" />
                                                    <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" Header="Ngày bắt đầu bảo hành"
                                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                                    <ext:Column ColumnID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" Header="Thời gian bảo hành còn lại"
                                                        Width="150" />
                                                    <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" Header="Ngày bắt đầu khấu hao"
                                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                                    <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian khấu hao còn lại"
                                                        Width="150" />
                                                </Columns>
                                            </ColumnModel>
                                            <SelectionModel>
                                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" />
                                            </SelectionModel>
                                            <Plugins>
                                                <ext:GridFilters ID="GridFilters4" runat="server" Local="true">
                                                    <Filters>
                                                        <ext:StringFilter DataIndex="TEN_TB" />
                                                        <ext:StringFilter DataIndex="SERIAL" />
                                                        <ext:StringFilter DataIndex="NGUYEN_GIA" />
                                                        <ext:StringFilter DataIndex="TEN_CL" />
                                                        <ext:StringFilter DataIndex="TEN_KIEU" />
                                                        <ext:StringFilter DataIndex="NGAY_BH" />
                                                        <ext:StringFilter DataIndex="THOI_GIAN_BH" />
                                                        <ext:StringFilter DataIndex="NGAY_KH" />
                                                        <ext:StringFilter DataIndex="THOI_GIAN_KH" />
                                                    </Filters>
                                                </ext:GridFilters>
                                            </Plugins>
                                            <View>
                                                <ext:GroupingView ID="GroupingView2" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                                            </View>
                                            <LoadMask ShowMask="true" />
                                            <SaveMask ShowMask="true" />
                                        </ext:GridPanel>
                                    </Items>
                                </ext:FieldSet>
                            </Items>
                        </ext:Container>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{nkForm}.form.submit({waitMsg : 'Đang ghi...', params:{json: Ext.encode(#{gpDanhSachThietBiDaChon}.getRowsValues())},success : successHandler, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button5" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{nkForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable();" />
            </Listeners>
        </ext:Window>

        <ext:Window ID="wdDanhSachThietBi" runat="server" Hidden="true" Title="Danh sách thiết bị" Modal="true"
            Icon="Book" Width="700" Height="450" Resizable="false">
            <Items>
                <ext:GridPanel ID="gpDanhSachThietBi" RowHeight="0.6" Title="Danh sách thiết bị"
                    runat="server" Border="true" Height="370" Selectable="true" Header="true" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar3" runat="server">
                            <Items>
                                <ext:Button ID="Button8" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">
                                    <Listeners>
                                        <Click Handler="#{wdDMTB}.show();#{TRANG_THAI}.setValue(1); #{btnThemMoiKieu}.disable();
                                    #{MA_CL}.setValue(''); #{MA_TB}.enable();
                                    #{MA_KIEU}.setValue('');" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button9" runat="server" Text="Import File Excel" Icon="PageExcel">
                                    <Listeners>
                                        <Click Handler="#{importForm}.reset();#{wdImport}.show();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill3" runat="server" />
                                <ext:Button ID="btnChonLua" runat="server" Text="Chọn" Icon="Accept" IconAlign="Left">
                                    <Listeners>
                                        <Click Fn="ChonLua" />
                                    </Listeners>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="Store_dsThietBi" runat="server" RemoteSort="false" UseIdConfirmation="false"
                            WarningOnDirty="false" GroupField="TEN_CL">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsThietBi/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="Check" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="NGUYEN_GIA" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="TRANG_THAI" />
                                        <ext:RecordField Name="TEN_TT" />
                                        <ext:RecordField Name="NGAY_BH" Type="Date" />
                                        <ext:RecordField Name="THOI_GIAN_BH" />
                                        <ext:RecordField Name="NGAY_KH" Type="Date" />
                                        <ext:RecordField Name="THOI_GIAN_KH" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                <ext:Parameter Name="trangthai" Value="1" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                            <ext:CheckColumn ColumnID="Check" Hidden="true" DataIndex="Check" Header="Check">
                            </ext:CheckColumn>
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100" />
                            <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100">
                                <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" />
                            </ext:Column>
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200" />
                            <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200" />
                            <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" Header="Ngày bắt đầu bảo hành"
                                Width="150" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" Header="Thời gian bảo hành còn lại"
                                Width="150" />
                            <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" Header="Ngày bắt đầu khấu hao"
                                Width="150" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian khấu hao còn lại"
                                Width="150" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:CheckboxSelectionModel ID="CheckboxSelectionModel2" runat="server" />
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters3" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_TB" />
                                <ext:StringFilter DataIndex="SERIAL" />
                                <ext:StringFilter DataIndex="NGUYEN_GIA" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                <ext:StringFilter DataIndex="NGAY_BH" />
                                <ext:StringFilter DataIndex="THOI_GIAN_BH" />
                                <ext:StringFilter DataIndex="NGAY_KH" />
                                <ext:StringFilter DataIndex="THOI_GIAN_KH" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:GroupingView ID="GroupingView1" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Window>

        <ext:Window ID="wdDMTB" runat="server" Hidden="true" Title="Thêm mới thiết bị" Icon="Information"
            Width="530" Height="325" Resizable="false">
            <Items>
                <ext:FormPanel ID="dmtbForm" runat="server" Height="295" Width="520" Border="false"
                    Url="/Danhmuc/saveThietBi" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet4" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:Hidden ID="ID_MA_TB" runat="server" />
                                <ext:CompositeField ID="CompositeField5" runat="server" FieldLabel="Chủng loại" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:ComboBox ID="MA_CL" runat="server" DataIndex="MA_CL" TriggerAction="All" Mode="Local"
                                            Width="343" EnableKeyEvents="true" AllowBlank="false" IndicatorCls="red" EmptyText="Chọn chủng loại..."
                                            DisplayField="TEN_CL" ItemSelector="div.search-item" ValueField="MA_CL">
                                            <Store>
                                                <ext:Store ID="dsChungloai" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/dsChungloai" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_CL" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_CL" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_CL" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_CL}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template3" runat="server">
                                                <Html>
                                                    <tpl for=".">
						                                          <div class="search-item">
							                                         <h3><span>{MA_CL}</span>{TEN_CL}</h3>	                
						                                          </div>
					                                           </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');#{TimKiemKieuThemMaChungLoai}.reload();#{MA_KIEU}.setValue('');
                                                            if(this.getValue()==''){#{btnThemMoiKieu}.disable();} else {#{btnThemMoiKieu}.enable();};" />
                                                <Select Handler="#{TimKiemKieuThemMaChungLoai}.reload();#{MA_KIEU}.setValue('');
                                                            if(this.getValue()==''){#{btnThemMoiKieu}.disable();} else {#{btnThemMoiKieu}.enable();}; " />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <ext:DisplayField ID="DisplayField25" runat="server" Margins="0 0 0 0" Text="" Flex="3" />
                                        <ext:Button ID="btnThemMoiChungLoai" runat="server" Text="" Icon="Add">
                                            <Listeners>
                                                <Click Fn="ThemMoiChungLoai" />
                                            </Listeners>
                                        </ext:Button>
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField4" runat="server" FieldLabel="Kiểu" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:ComboBox ID="MA_KIEU" runat="server" DataIndex="MA_KIEU" TriggerAction="All"
                                            Mode="Local" ItemSelector="div.search-item" AllowBlank="false" IndicatorCls="red"
                                            EmptyText="Chọn kiểu..." Width="343" EnableKeyEvents="true" DisplayField="TEN_KIEU"
                                            ValueField="MA_KIEU">
                                            <Store>
                                                <ext:Store ID="TimKiemKieuThemMaChungLoai" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/TimKiemKieuThemMaChungLoai" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_KIEU" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_KIEU" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_KIEU" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="macl" Value="#{MA_CL}.getValue()" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template5" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3><span>{MA_KIEU}</span>{TEN_KIEU}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <ext:DisplayField ID="DisplayField3" runat="server" Margins="0 0 0 0" Text="" Flex="3" />
                                        <ext:Button ID="btnThemMoiKieu" runat="server" Text="" Icon="Add" Disabled="true">
                                            <Listeners>
                                                <Click Fn="ThemMoiKieu" />
                                            </Listeners>
                                        </ext:Button>
                                    </Items>
                                </ext:CompositeField>
                                <ext:TextField ID="MA_TB" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                    DataIndex="MA_TB" runat="server" AnchorHorizontal="95%" FieldLabel="Mã thiết bị" />
                                <ext:TextField ID="TEN_TB" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                    DataIndex="TEN_TB" runat="server" AnchorHorizontal="95%" FieldLabel="Tên thiết bị" />
                                <ext:TextField ID="SERIAL" IndicatorText="*" AllowBlank="false" DataIndex="SERIAL"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="50%" FieldLabel="Số Serial" />
                                <ext:TextField ID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" MaskRe="/[0-9\.]/" runat="server"
                                    AnchorHorizontal="50%" FieldLabel="Nguyên giá">
                                    <Listeners>
                                        <Change Handler="this.setValue(Ext.util.Format.number(newValue.replace(/[,]/g, ''), '0,0'));" />
                                    </Listeners>
                                </ext:TextField>
                                <ext:ComboBox ID="TRANG_THAI" runat="server" DataIndex="TRANG_THAI" TriggerAction="All"
                                    Mode="Local" FieldLabel="Trạng thái" AnchorHorizontal="95%" EnableKeyEvents="true"
                                    IndicatorText="*" AllowBlank="false" IndicatorCls="red" EmptyText="Chọn trạng thái..."
                                    DisplayField="TEN_TT" ValueField="MA_TT">
                                    <Store>
                                        <ext:Store ID="Store3" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                            <Proxy>
                                                <ext:HttpProxy Url="/Danhmuc/dsTrangthai" />
                                            </Proxy>
                                            <Reader>
                                                <ext:JsonReader IDProperty="MA_TT" Root="data" TotalProperty="total">
                                                    <Fields>
                                                        <ext:RecordField Name="MA_TT" SortDir="ASC" />
                                                        <ext:RecordField Name="TEN_TT" />
                                                    </Fields>
                                                </ext:JsonReader>
                                            </Reader>
                                            <BaseParams>
                                                <ext:Parameter Name="txtfilter" Value="#{TRANG_THAI}.getValue()" Mode="Raw" />
                                                <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                            </BaseParams>
                                        </ext:Store>
                                    </Store>
                                    <Triggers>
                                        <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                    </Triggers>
                                    <Listeners>
                                        <TriggerClick Handler="this.setValue('');" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:CompositeField ID="CompositeField15" runat="server" FieldLabel="Ngày bảo hành"
                                    AnchorHorizontal="95%">
                                    <Items>
                                        <ext:DateField ID="NGAY_BH" DataIndex="NGAY_BH" IndicatorText="*" AllowBlank="false"
                                            IndicatorCls="red" runat="server" AnchorHorizontal="35%" />
                                        <ext:DisplayField ID="DisplayField4" runat="server" Margins="0 0 0 10" Text="Thời gian còn lại"
                                            AnchorHorizontal="25%" AnchorVertical="center" Flex="1" />
                                        <ext:NumberField ID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" MaxLength="3" runat="server"
                                            AnchorHorizontal="35%" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField6" runat="server" FieldLabel="Ngày khấu hao"
                                    AnchorHorizontal="95%">
                                    <Items>
                                        <ext:DateField ID="NGAY_KH" DataIndex="NGAY_KH" IndicatorText="*" AllowBlank="false"
                                            IndicatorCls="red" runat="server" AnchorHorizontal="35%" />
                                        <ext:DisplayField ID="DisplayField5" runat="server" Margins="0 0 0 10" Text="Thời gian còn lại"
                                            AnchorHorizontal="25%" AnchorVertical="center" Flex="1" />
                                        <ext:NumberField ID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" MaxLength="3" runat="server"
                                            AnchorHorizontal="35%" />
                                    </Items>
                                </ext:CompositeField>
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar5" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill4" runat="server" />
                                <ext:Button ID="Button2" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{dmtbForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler1, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button13" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{dmtbForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
            </Listeners>
        </ext:Window>
        <ext:Window ID="wdChungLoai" runat="server" Hidden="true" Title="Thông tin Chủng loại thiết bị"
            Icon="Information" Width="530" Height="210" Resizable="false" Modal="true">
            <Items>
                <ext:FormPanel ID="clForm" runat="server" Height="180" Width="520" Border="false"
                    Url="/Danhmuc/saveChungloai_Thieti" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet5" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:Hidden ID="ID_MA_CL" runat="server" />
                                <ext:TextField ID="MA_CL_TB" IndicatorText="*" AllowBlank="false" DataIndex="MA_CL_TB"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="45%" FieldLabel="Mã chủng loại" />
                                <ext:TextField ID="TEN_CL" IndicatorText="*" AllowBlank="false" DataIndex="TEN_CL"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="90%" FieldLabel="Tên chủng loại" />
                                <ext:TextArea ID="GHICHU" DataIndex="GHICHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="90%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar6" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill5" runat="server" />
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{clForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler_chungloai, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button14" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{clForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer3" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
            </Listeners>
        </ext:Window>
        <ext:Window ID="wdKieu" runat="server" Hidden="true" Title="Thông tin Kiểu" Icon="Information"
            Width="530" Height="200" Resizable="false" Modal="true">
            <Items>
                <ext:FormPanel ID="kieuForm" runat="server" Height="170" Width="520" Border="false"
                    Url="/Danhmuc/saveKieu_ThietBi" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet6" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:Hidden ID="ID_MA_KIEU" runat="server" />
                                <ext:Hidden ID="MA_CL_KIEU" runat="server" />
                                <ext:NumberField ID="MA_KIEU_KIEU" IndicatorText="*" AllowBlank="false" DataIndex="MA_KIEU_KIEU"
                                    IndicatorCls="red" EmptyText="Chỉ nhập số..." runat="server" AnchorHorizontal="50%"
                                    FieldLabel="Mã kiểu" />
                                <ext:TextField ID="TEN_KIEU" IndicatorText="*" AllowBlank="false" DataIndex="TEN_KIEU"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="90%" FieldLabel="Tên kiểu" />
                                <ext:TextArea ID="GHI_CHU_TB" DataIndex="GHI_CHU_TB" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="90%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar7" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill6" runat="server" />
                                <ext:Button ID="Button4" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{kieuForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler_kieu, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button6" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{kieuForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer4" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
            </Listeners>
        </ext:Window>
        <ext:Window ID="wdImport" runat="server" Hidden="true" Title="Import File Excel"
            Icon="PageExcel" Width="400" Height="110" Resizable="false">
            <Items>
                <ext:FormPanel ID="importForm" runat="server" Height="80" Width="390" Border="false"
                    Url="/Danhmuc/save_ThietBi_Ex" ButtonAlign="Center" FileUpload="true" Enctype="multipart/form-data">
                    <Items>
                        <ext:FieldSet ID="FieldSet7" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:FileUploadField ID="TEN_FILE" DataIndex="TEN_FILE" Name="TEN_FILE" runat="server"
                                    Icon="Attach" FieldLabel="File thiết bị" AnchorHorizontal="100%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar8" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill7" runat="server" />
                                <ext:Button ID="Button7" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{importForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler1 });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer5" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
        </ext:Window>
    </div>
</body>
</html>
