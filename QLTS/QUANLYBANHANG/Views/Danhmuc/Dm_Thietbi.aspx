﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục thiết bị</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        .red {
            background-color:Red ;
            color:Black;
        }
        
        .green {
            background-color:Green;
            color:Black;
        }
        
        .yellow
        {
            background-color:Yellow ;
            color:Black;
        }  
        .silver
         {
            background-color:Silver;
            color:Black;
         } 
        .orange {
            background-color:Orange ;
            color:Black;
        }
        .blue {
            background-color:Blue ;
            color:Black;
        }
        .olive {
            background-color:Olive ;
            color:Black;
        }
        
        .lime {
            background-color:Lime ;
            color:Black;
        }
    </style> 
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };


        var failureHandler1 = function (form, action) {
            if (action.response.responseText == '<pre>{success:true}</pre>') {
                dsThietBi.reload();
                importForm.reset();
                wdImport.hide();
            }
            else {
                Ext.Msg.show({
                    title: "Thông báo",
                    msg: "Định dạng file không đúng",
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        }

        var successHandler = function (form, action) {
            dsThietBi.reload();
            dmtbForm.reset();
            wdDMTB.hide();

            MA_TB.setValue('');
            ID_MA_TB.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var successHandler_chungloai = function () {
            dsChungloai.reload();
            setTimeout(function () {
                MA_CL.setValue(MA_CL_TB.getValue());
                clForm.reset();
                wdChungLoai.hide();
                btnThemMoiKieu.enable();
            }, 1000);

        };


        var successHandler_kieu = function (form, action) {
            TimKiemKieuThemMaChungLoai.reload();
            setTimeout(function () {
                MA_KIEU.setValue(action.result.extraParams.MA_KIEU);
                kieuForm.reset();
                wdKieu.hide();
            }, 1000);
        };


        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_TB');
            MA_TB.setValue(s);
            ID_MA_TB.setValue(s);
            MA_TB.disable();
            var record = GridPanel1.getSelectionModel().getSelected();
            dmtbForm.getForm().loadRecord(record);
            wdDMTB.show();
            setTimeout(TimKiemKieuThemMaChungLoai.reload(), 900);
            var s1 = GridPanel1.getSelectionModel().getSelected().get('MA_KIEU');
            setTimeout(function () { MA_KIEU.setValue(s1); }, 1000);
            btnThemMoiKieu.enable();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };


        var ThemMoiChungLoai = function () {
            wdChungLoai.show();
            MA_CL.setValue('');
            TimKiemKieuThemMaChungLoai.reload();
            MA_KIEU.setValue('');
        };


        var ThemMoiKieu = function () {
            wdKieu.show();
            MA_CL_KIEU.setValue(MA_CL.getValue());
        };

        function kiemtra() {
            if (GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') == 1) {
                btnSua.enable(); btnXoa.enable();
            }else if (GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') == 8) {
                btnSua.disable(); btnXoa.disable(); 
            }
            else {
                btnSua.enable(); btnXoa.disable();
            }
        }

        function getRowClass(record) {
            if (record.data.TRANG_THAI === 1) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 2) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 3) {
                return 'orange';
            }

            if (record.data.TRANG_THAI === 9) {
                return 'yellow';
            }
            if (record.data.TRANG_THAI === 5) {
                return 'olive';
            }
            if (record.data.TRANG_THAI === 6) {
                return 'blue';
            }
            if (record.data.TRANG_THAI === 7) {
                return 'lime';
            }
            if (record.data.TRANG_THAI === 8) {
                return 'red';
            }

        };

</script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
            NGAY_BH.SetValue(DateTime.Now);
            NGAY_KH.SetValue(DateTime.Now);
            
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>

                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{dmtbForm}.reset();#{wdDMTB}.show();#{TRANG_THAI}.setValue(1); #{btnThemMoiKieu}.disable();
                                    #{MA_CL}.setValue(''); #{MA_TB}.enable();
                                    #{MA_KIEU}.setValue('');" />
                                </Listeners>                                                                             
                            </ext:Button> 
                                         
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                   
                            
                            <ext:Button ID="btnXoa" Disabled="true" runat="server"  Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Danhmuc/xoaThietBi" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsThietBi}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_TB')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   

                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server" />
                              <ext:Button ID="Button8" runat="server" Text="Import File Excel" Icon="PageExcel">                                                   
                                  <Listeners>
                                    <Click Handler="#{importForm}.reset();#{wdImport}.show();" />
                                  </Listeners>                                                                         
                            </ext:Button>



                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tìm kiếm theo số serial hoặc tên thiết bị..." 
                                EnableKeyEvents="true" Width="400px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                              <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["MA_TB"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsThietBi}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>    
                <Store>
                    <ext:Store 
                        ID="dsThietBi" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true"
                        GroupField="TEN_CL" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsThietBi/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_TB" />
                                    <ext:RecordField Name="TEN_TB" /> 
                                    <ext:RecordField Name="SERIAL" /> 
                                    <ext:RecordField Name="NGUYEN_GIA" /> 
                                    <ext:RecordField Name="MA_CL" />
                                    <ext:RecordField Name="TEN_CL" /> 
                                    <ext:RecordField Name="MA_KIEU" />    
                                    <ext:RecordField Name="TEN_KIEU" />   
                                    <ext:RecordField Name="TRANG_THAI" /> 
                                    <ext:RecordField Name="TEN_TT" />
                                    <ext:RecordField Name="NGAY_BH" Type="Date" /> 
                                    <ext:RecordField Name="THOI_GIAN_BH" />
                                    <ext:RecordField Name="THOI_GIAN_BHCL" />   
                                    <ext:RecordField Name="NGAY_KH" Type="Date" /> 
                                    <ext:RecordField Name="THOI_GIAN_KH" />
                                    <ext:RecordField Name="THOI_GIAN_KHCL" />                                                                                                                                
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100"  /> 
                        <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200"  /> 
                        <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100"  /> 
                        <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100" >
                            <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" /> 
                        </ext:Column>
                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200"/>
                        <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200"/>
                        <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" 
                        Header="Ngày bắt đầu bảo hành" Width="150" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_BHCL" DataIndex="THOI_GIAN_BHCL" Header="Thời gian bảo hành còn lại" Width="150"/>
                        <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" 
                        Header="Ngày bắt đầu khấu hao" Width="150" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_KHCL" DataIndex="THOI_GIAN_KHCL" Header="Thời gian khấu hao còn lại" Width="150" />    
                                                           
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="kiemtra();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>


                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="TEN_TB"/>
                            <ext:StringFilter DataIndex="SERIAL"/>     
                            <ext:StringFilter DataIndex="NGUYEN_GIA"/>
                            <ext:StringFilter DataIndex="TEN_CL"/>       
                            <ext:StringFilter DataIndex="TEN_KIEU"/>
                            <ext:StringFilter DataIndex="NGAY_BH"/>
                            <ext:StringFilter DataIndex="THOI_GIAN_BH"/>
                            <ext:StringFilter DataIndex="NGAY_KH"/>  
                            <ext:StringFilter DataIndex="THOI_GIAN_KH"/>                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>

                <View>
                       <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                                   <GetRowClass Fn="getRowClass" />      
                       </ext:GroupingView>
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="kiemtra();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
         
        <ext:Window ID="wdDMTB" runat="server" Hidden="true" Title="Thông tin thiết bị" 
                Icon="Information" Width="530" Height="325" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="dmtbForm" runat="server" Height="295" Width="520"
                        Border="false" Url="/Danhmuc/saveThietBi" ButtonAlign="Center" >                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="ID_MA_TB" runat="server" />
                                       
                                         <ext:CompositeField ID="CompositeField5" runat="server" FieldLabel="Chủng loại" AnchorHorizontal="95%">
                                              <Items>
                                                      <ext:ComboBox 
                                                        ID="MA_CL" 
                                                        runat="server"
                                                        DataIndex="MA_CL"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local"                                       
                                                        Width="343"    
                                                        EnableKeyEvents="true"
                                                        AllowBlank="false"  IndicatorCls="red"
                                                        EmptyText="Chọn chủng loại..."
                                                        DisplayField="TEN_CL" ItemSelector="div.search-item"
                                                        ValueField="MA_CL" >
                                                        <Store>
                                                            <ext:Store 
                                                                ID="dsChungloai" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsChungloai" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_CL" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_CL" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_CL"/>                                                                                                    
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_CL}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                                                                   
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store> 
                                                        <Template ID="Template1" runat="server">
                                                           <Html>
					                                           <tpl for=".">
						                                          <div class="search-item">
							                                         <h3><span>{MA_CL}</span>{TEN_CL}</h3>	                
						                                          </div>
					                                           </tpl>
				                                           </Html>
                                                        </Template>    
                                                        <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                        <Listeners>
                                                            <TriggerClick Handler="this.setValue('');#{TimKiemKieuThemMaChungLoai}.reload();#{MA_KIEU}.setValue('');
                                                            if(this.getValue()==''){#{btnThemMoiKieu}.disable();} else {#{btnThemMoiKieu}.enable();};" />
                                                            <Select  Handler="#{TimKiemKieuThemMaChungLoai}.reload();#{MA_KIEU}.setValue('');
                                                            if(this.getValue()==''){#{btnThemMoiKieu}.disable();} else {#{btnThemMoiKieu}.enable();}; "/>
                                                            
                                                        </Listeners>                                                                                                                                    
                                                      </ext:ComboBox>
                                                      <ext:DisplayField ID="DisplayField5" runat="server" Margins="0 0 0 0" Text="" Flex="3" />
                                                      <ext:Button ID="btnThemMoiChungLoai" runat="server" Text="" Icon="Add">                                                   
                                                        <Listeners>
                                                            <Click Fn="ThemMoiChungLoai"/>
                                                        </Listeners>                                                                             
                                                    </ext:Button> 
                                              </Items>
                                          </ext:CompositeField>


                                         <ext:CompositeField ID="CompositeField2" runat="server" FieldLabel="Kiểu" AnchorHorizontal="95%">
                                              <Items>
                                         <ext:ComboBox 
                                                        ID="MA_KIEU" 
                                                        runat="server"
                                                        DataIndex="MA_KIEU"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local" ItemSelector="div.search-item" 
                                                        AllowBlank="false"  IndicatorCls="red"
                                                        EmptyText="Chọn kiểu..."                         
                                                        Width="343"
                                                        EnableKeyEvents="true"
                                                        DisplayField="TEN_KIEU"     
                                                        ValueField="MA_KIEU" >
                                                        <Store>
                                                            <ext:Store 
                                                                ID="TimKiemKieuThemMaChungLoai" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/TimKiemKieuThemMaChungLoai" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_KIEU" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_KIEU" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_KIEU"/>                                                                                                    
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="macl" Value="#{MA_CL}.getValue()" Mode="Raw" />                                                            
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store> 
                                                        <Template ID="Template2" runat="server">
                                                           <Html>
                                                <tpl for=".">
                                                <div class="search-item">
                                                <h3><span>{MA_KIEU}</span>{TEN_KIEU}</h3>                 
                                                </div>
                                                </tpl>
                                               </Html>
                                                        </Template>    
                                                        <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                  <Listeners> 
                                                        <TriggerClick Handler="this.setValue('');" />
                                                        </Listeners>                                                                                                                          
                                         </ext:ComboBox>
                                         <ext:DisplayField ID="DisplayField1" runat="server" Margins="0 0 0 0" Text="" Flex="3" />
                                                      <ext:Button ID="btnThemMoiKieu" runat="server" Text="" Icon="Add" Disabled="true">                                                   
                                                        <Listeners>
                                                            <Click Fn="ThemMoiKieu"/>
                                                        </Listeners>                                                                             
                                                    </ext:Button> 
                                              </Items>
                                          </ext:CompositeField>

                                         <ext:TextField 
                                         ID="MA_TB" IndicatorText="*" AllowBlank="false"  IndicatorCls="red"  
                                         DataIndex="MA_TB"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Mã thiết bị"/>

                                         <ext:TextField 
                                         ID="TEN_TB" IndicatorText="*" AllowBlank="false"  IndicatorCls="red"  
                                         DataIndex="TEN_TB"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Tên thiết bị"/>

                                         <ext:TextField 
                                         ID="SERIAL" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="SERIAL" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="50%"                          
                                         FieldLabel="Số Serial"/>

                                         <ext:TextField 
                                         ID="NGUYEN_GIA" 
                                         DataIndex="NGUYEN_GIA" MaskRe="/[0-9\.]/"
                                         runat="server" AnchorHorizontal="50%"                          
                                         FieldLabel="Nguyên giá">
                                            <Listeners>     
                                                <Change Handler="this.setValue(Ext.util.Format.number(newValue.replace(/[,]/g, ''), '0,0'));" />
                                            </Listeners>
                                         </ext:TextField>

                                         <ext:ComboBox 
                                                        ID="TRANG_THAI" 
                                                        runat="server"
                                                        DataIndex="TRANG_THAI"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local"  
                                                        FieldLabel="Trạng thái"                                            
                                                        AnchorHorizontal="95%" 
                                                        EnableKeyEvents="true"
                                                        IndicatorText="*" AllowBlank="false"  IndicatorCls="red"
                                                        EmptyText="Chọn trạng thái..."
                                                        DisplayField="TEN_TT"     
                                                        ValueField="MA_TT" >
                                                        <Store>
                                                            <ext:Store 
                                                                ID="Store3" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsTrangthai" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_TT" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_TT" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_TT"/>                                                                                                    
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{TRANG_THAI}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                                                                   
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store> 
                                                       
                                                        <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                  <Listeners> 
                                                        <TriggerClick Handler="this.setValue('');" />
                                                        </Listeners>                                                                                                                          
                                         </ext:ComboBox>

                                         <ext:CompositeField ID="CompositeField1" runat="server" FieldLabel="Ngày bảo hành" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:DateField 
                                                     ID="NGAY_BH" 
                                                     DataIndex="NGAY_BH" IndicatorText="*" AllowBlank="false"  IndicatorCls="red"
                                                     runat="server" AnchorHorizontal="35%"                          
                                                     />

                                                    <ext:DisplayField ID="DisplayField2" runat="server" Margins="0 0 0 10" Text="Thời gian còn lại" 
                                                    AnchorHorizontal="25%" AnchorVertical="center" Flex="1"  />
                                                    
                                                    <ext:NumberField 
                                                     ID="THOI_GIAN_BH" MaxLength="3"
                                                     DataIndex="THOI_GIAN_BH"  
                                                     runat="server" AnchorHorizontal="35%"/>                                                                              
                                              </Items>
                                        </ext:CompositeField>   
                                         
                                         <ext:CompositeField ID="CompositeField3" runat="server" FieldLabel="Ngày khấu hao" AnchorHorizontal="95%">
                                              <Items>
                                                    <ext:DateField 
                                                     ID="NGAY_KH" 
                                                     DataIndex="NGAY_KH" IndicatorText="*" AllowBlank="false"  IndicatorCls="red"
                                                     runat="server" AnchorHorizontal="35%"                          
                                                     />

                                                    <ext:DisplayField ID="DisplayField3" runat="server" Margins="0 0 0 10" Text="Thời gian còn lại" 
                                                    AnchorHorizontal="25%" AnchorVertical="center" Flex="1"  />
                                                    
                                                    <ext:NumberField 
                                                     ID="THOI_GIAN_KH"   
                                                     DataIndex="THOI_GIAN_KH" MaxLength="3"
                                                     runat="server" AnchorHorizontal="35%"/>                                                                              
                                              </Items>
                                        </ext:CompositeField>                       
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{dmtbForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{dmtbForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>

       
        <ext:Window ID="wdChungLoai" runat="server" Hidden="true" Title="Thông tin Chủng loại thiết bị" 
                Icon="Information" Width="530" Height="210" Resizable="false" Modal="true">
                    <Items>
                        <ext:FormPanel ID="clForm" runat="server" Height="180" Width="520"
                        Border="false" Url="/Danhmuc/saveChungloai_Thieti" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet2" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="ID_MA_CL" runat="server" />

                                        <ext:TextField 
                                         ID="MA_CL_TB" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="MA_CL_TB" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="45%"                          
                                         FieldLabel="Mã chủng loại"/>

                                         <ext:TextField 
                                         ID="TEN_CL" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_CL" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên chủng loại"/>

                                         <ext:TextArea 
                                         ID="GHICHU" 
                                         DataIndex="GHICHU"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="90%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar3" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill3" runat="server" />                                                                         
                            <ext:Button ID="Button2" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{clForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler_chungloai, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button4" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{clForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>

        <ext:Window ID="wdKieu" runat="server" Hidden="true" Title="Thông tin Kiểu" 
                Icon="Information" Width="530" Height="200" Resizable="false" Modal="true">
                    <Items>
                        <ext:FormPanel ID="kieuForm" runat="server" Height="170" Width="520"
                        Border="false" Url="/Danhmuc/saveKieu_ThietBi" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet3" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="ID_MA_KIEU" runat="server" />

                                        <ext:Hidden ID="MA_CL_KIEU" runat="server" />

                                         <ext:NumberField 
                                         ID="MA_KIEU_KIEU" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="MA_KIEU_KIEU" IndicatorCls="red"   EmptyText="Chỉ nhập số..." 
                                         runat="server" AnchorHorizontal="50%"                          
                                         FieldLabel="Mã kiểu"/>

                                         <ext:TextField 
                                         ID="TEN_KIEU" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_KIEU" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên kiểu"/>

                                         <ext:TextArea 
                                         ID="GHI_CHU_TB" 
                                         DataIndex="GHI_CHU_TB"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="90%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar4" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill4" runat="server" />                                                                         
                            <ext:Button ID="Button5" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{kieuForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler_kieu, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button6" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{kieuForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer3" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>

        <ext:Window ID="wdImport" runat="server" Hidden="true" Title="Import File Excel" Icon="PageExcel" Width="400" Height="110" Resizable="false">
            <Items>
                 <ext:FormPanel ID="importForm" runat="server" Height="80" Width="390" Border="false"
                 Url="/Danhmuc/save_ThietBi_Ex" ButtonAlign="Center"  FileUpload="true" Enctype="multipart/form-data" >                
                    <Items> 
                      <ext:FieldSet ID="FieldSet4" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                               <Items>
                                    <ext:FileUploadField ID="TEN_FILE" DataIndex="TEN_FILE" Name="TEN_FILE" runat="server" Icon="Attach"  FieldLabel="File thiết bị"
                                    AnchorHorizontal="100%"/>
                               </Items>
                       </ext:FieldSet> 
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar5" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill5" runat="server" />
                                <ext:Button ID="Button7" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{importForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler1 });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer4" runat="server" />
                            </Items>
                        </ext:Toolbar>
           </BottomBar>
                </ext:FormPanel>                  
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
              
        </ext:Window>

    </div>
</body>
</html>
