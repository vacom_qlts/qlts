﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục nguồn kinh phí</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
    </style> 
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNguonKinhPhi.reload();
            nkpForm.reset();
            wdNguonKinhPhi.hide();
            MA_NKP.setValue('');
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_NKP');
            MA_NKP.setValue(s);
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nkpForm.getForm().loadRecord(record);
            wdNguonKinhPhi.show();
            MA_NKP.disable();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };     
</script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="GHI_CHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{nkpForm}.reset();#{wdNguonKinhPhi}.show();MA_NKP.enable();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Danhmuc/xoaNguonKinhPhi" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsNguonKinhPhi}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="MA_NKP" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NKP')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   
                              
                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              
                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tim theo mã hoặc tên..." 
                                EnableKeyEvents="true" Width="400px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                             <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["MA_NKP"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsNguonKinhPhi}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsNguonKinhPhi" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsNguonKinhPhi/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_NKP" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_NKP" />
                                    <ext:RecordField Name="TEN_NKP" /> 
                                    <ext:RecordField Name="MO_TA" />
                                    <ext:RecordField Name="GHI_CHU" />                                                                                                                                         
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_NKP" DataIndex="MA_NKP" Header="Mã nguồn kinh phí" Width="150" /> 
                        <ext:Column ColumnID="TEN_NKP" DataIndex="TEN_NKP" Header="Tên nguồn kinh phí" Width="200" /> 
                        <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" />   
                        <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" />                                       
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters> 
                            <ext:StringFilter DataIndex="MA_NKP" />                           
                            <ext:StringFilter DataIndex="TEN_NKP" />
                            <ext:StringFilter DataIndex="MO_TA" />
                            <ext:StringFilter DataIndex="GHI_CHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                       <ext:BufferView ID="BufferView1" runat="server" ScrollDelay="0" />
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
         
        <ext:Window ID="wdNguonKinhPhi" runat="server" Hidden="true" Title="Thông tin đơn vị tính" 
                Icon="Information" Width="530" Height="300" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="nkpForm" runat="server" Height="270" Width="520"
                        Border="false" Url="/Danhmuc/saveNguonKinhPhi" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        
                                        <ext:Hidden ID="id" runat="server" />

                                        <ext:TextField 
                                         ID="MA_NKP" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="MA_NKP" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="45%"                          
                                         FieldLabel="Mã nguồn kinh phí"/>

                                         <ext:TextField 
                                         ID="TEN_NKP" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_NKP" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên nguồn kinh phí"/>

                                         <ext:TextArea 
                                         ID="MO_TA" 
                                         DataIndex="MO_TA"
                                         runat="server"        
                                         FieldLabel="Mô tả"
                                         AnchorHorizontal="90%"/>

                                         <ext:TextArea 
                                         ID="GHI_CHU" 
                                         DataIndex="GHI_CHU"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="90%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{nkpForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{nkpForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>
    </div>
</body>
</html>
