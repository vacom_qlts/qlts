﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Xuất kho thiết bị</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
        
        .orange
        {
            background-color: Orange;
            color: Black;
        }
        
        .green
        {
            background-color: Green;
            color: Black;
        }
        
        .yellow
        {
            background-color: Yellow;
            color: Black;
        }
        .silver
        {
            background-color: Silver;
            color: Black;
        }
    </style>
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsYeuCauTongQuat.reload();
            xkycmForm.reset();
            wdXuatKho_YeuCauMoi.hide();

            xktlForm.reset();
            wdXuatKho_ThanhLy.hide();
            id.setValue('');
            AnHienChucNang(0, 0);
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_XK');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            Store6.reload();
            wdXuatKho_YeuCauMoi.show();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };

        function getRowClass(record) {
            if (record.data.TRANG_THAI === 2) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 3) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 4) {
                return 'orange';
            }

            if (record.data.TRANG_THAI === 5) {
                return 'yellow';
            }

        };


        var ChonLua = function () {
            var data = gpDanhSachThietBi.getRowsValues({ selectedOnly: true });
            $.each(data, function (i, r) {
                gpDanhSachThietBiDaChon.insertRecord(0,
                     {

                         MA_TB: r.MA_TB, TEN_TB: r.TEN_TB, SERIAL: r.SERIAL,
                         NGUYEN_GIA: r.NGUYEN_GIA, MA_CL: r.MA_CL, TEN_CL: r.TEN_CL, MA_KIEU: r.MA_KIEU,
                         TEN_KIEU: r.TEN_KIEU, TRANG_THAI: r.TRANG_THAI, TEN_TT: r.TEN_TT,
                         NGAY_BH: r.NGAY_BH, THOI_GIAN_BH: r.THOI_GIAN_BH, NGAY_KH: r.NGAY_KH, THOI_GIAN_KH: r.THOI_GIAN_KH
                     });
            });

            var sm = gpDanhSachThietBi.getSelectionModel();
            var sel = sm.getSelections();
            for (i = 0; i < sel.length; i++) {
                gpDanhSachThietBi.store.remove(sel[i]);
            }

            dstb_Form.reload();
            wd_DanhSachThietBi.hide();

        }


        var Huy = function () {
            var data = gpDanhSachThietBiDaChon.getRowsValues({ selectedOnly: true });
            $.each(data, function (i, r) {
                gpDanhSachThietBi.insertRecord(0,
                     {
                         MA_TB: r.MA_TB, TEN_TB: r.TEN_TB, SERIAL: r.SERIAL,
                         NGUYEN_GIA: r.NGUYEN_GIA, MA_CL: r.MA_CL, TEN_CL: r.TEN_CL, MA_KIEU: r.MA_KIEU,
                         TEN_KIEU: r.TEN_KIEU, TRANG_THAI: r.TRANG_THAI, TEN_TT: r.TEN_TT,
                         NGAY_BH: r.NGAY_BH, THOI_GIAN_BH: r.THOI_GIAN_BH, NGAY_KH: r.NGAY_KH, THOI_GIAN_KH: r.THOI_GIAN_KH
                     });
            });

            var sm = gpDanhSachThietBiDaChon.getSelectionModel();
            var sel = sm.getSelections();
            for (i = 0; i < sel.length; i++) {
                gpDanhSachThietBiDaChon.store.remove(sel[i]);
            }
        }

        $(document).ready(function () {
            AnHienChucNang(0, 0);
        });

        function AnHienChucNang(loai, trangthai) {
            $.post("/Danhmuc/AnHienChucNang_XuatKho", { trangthai: trangthai, loai: loai }, function (data) {


                if (data.CCTB == true) {
                    $('#btnXuatKhoChoCapThietBi').show();
                }
                else {
                    $('#btnXuatKhoChoCapThietBi').hide();
                }

                if (data.TM == true) {
                    $('#btnXuatKhoThemMoi').show();
                }
                else {
                    $('#btnXuatKhoThemMoi').hide();
                }

                if (data.TM_BC == true) {
                    $('#btnXuatKhoThemMoi_bc').show();
                }
                else {
                    $('#btnXuatKhoThemMoi_bc').hide();
                }

                if (data.SC == true) {
                    $('#btnXuatKhoSuaChua').show();
                }
                else {
                    $('#btnXuatKhoSuaChua').hide();
                }

                if (data.SC_BC == true) {
                    $('#btnXuatKhoSuaChua_bc').show();
                }
                else {
                    $('#btnXuatKhoSuaChua_bc').hide();
                }


                if (data.TH == true) {
                    $('#btnXuatKhoThanhLy').show();
                }
                else {
                    $('#btnXuatKhoThanhLy').hide();
                }

                if (data.TH_BC == true) {
                    $('#btnXuatKhoThanhLy_bc').show();
                }
                else {
                    $('#btnXuatKhoThanhLy_bc').hide();
                }
            });
        }

        function InBaoCao() {
            var ts = ID_PYC.getValue();
            window.location.href = "/Report/HienBaoCao.aspx?bc=xk_ycm&ts=" + ts;
        }

        function InPhieuXuatKho_TL() {
            var ts = ID_PYC.getValue();
            window.location.href = "/Report/HienBaoCao.aspx?bc=xk_tl&ts=" + ts;
        }

        function MaPhieu() {
            $.post("/Danhmuc/TaoMaTuDong", { Ma: "XKM" }, function (data) {
                MA_XK.setValue(data);
            }, "json");

            $.post("/Danhmuc/TaoMaTuDong", { Ma: "XKTL" }, function (data) {
                MA_XK_TL.setValue(data);
            }, "json");
        }
    </script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
            NGAY_XK.SetValue(DateTime.Now);
            NGAY_XK_TL.SetValue(DateTime.Now);

            this.Store1.DataSource = new object[]
            {
                new object[] {1,"Yêu cầu cấp mới"},
                new object[] {2,"Yêu cầu sửa"},
                new object[] {3,"Yêu cầu thu hồi"},
                new object[] {4,"Yêu cầu thanh lý"}       
            };
            this.Store1.DataBind();
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:Hidden ID="ID_PYC" runat="server" />
                <ext:GridPanel ID="GridPanel1" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="btnXuatKhoChoCapThietBi" runat="server" Text="Chờ cấp thiết bị" Icon="ReportGo">
                                    <DirectEvents>
                                        <Click Url="/Danhmuc/ChoCapThietBi" CleanRequest="true" Method="POST" Success="#{dsYeuCauTongQuat}.reload();">
                                            <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="Bạn có chắc chắc muốn thực hiện ?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="MA_PYC" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoThemMoi" runat="server" Text="Xuất kho" Icon="ApplicationGo">
                                    <Listeners>
                                        <Click Handler="#{xkycmForm}.reset(); #{wdXuatKho_YeuCauMoi}.show();MaPhieu();
                                    #{Store_dsThietBi}.reload();#{Store_TimKiemThietBiTheoMaNhapKho}.reload();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoThemMoi_bc" runat="server" Text="In phiếu" Icon="PrinterAdd">
                                    <Listeners>
                                        <Click Handler="InBaoCao()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoSuaChua" runat="server" Text="Xuất kho" Icon="ApplicationGo">
                                    <Listeners>
                                        <Click Handler="" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoSuaChua_bc" runat="server" Text="In phiếu" Icon="PrinterAdd">
                                    <Listeners>
                                        <Click Handler="" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoThanhLy" runat="server" Text="Xuất kho" Icon="ApplicationGo">
                                    <Listeners>
                                        <Click Handler="#{xktlForm}.reset(); #{wdXuatKho_ThanhLy}.show();#{TimKiemTheoMaYeuCau_TL}.reload();MaPhieu();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXuatKhoThanhLy_bc" runat="server" Text="In phiếu" Icon="PrinterAdd">
                                    <Listeners>
                                        <Click Handler="InPhieuXuatKho_TL()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:ComboBox ID="LOAI_YEU_CAU" AllowBlank="false" Flex="3" DataIndex="PHONG" runat="server"
                                    Mode="Local" ValueField="ID" DisplayField="NAME" FieldLabel="Điều kiện lọc" ItemSelector="div.search-item"
                                    EmptyText="Chọn loại yêu cầu..." Width="250">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                            <Reader>
                                                <ext:ArrayReader IDProperty="ID">
                                                    <Fields>
                                                        <ext:RecordField Name="ID" SortDir="ASC" />
                                                        <ext:RecordField Name="NAME" />
                                                    </Fields>
                                                </ext:ArrayReader>
                                            </Reader>
                                        </ext:Store>
                                    </Store>
                                    <Template ID="Template6" runat="server">
                                        <Html>
                                            <tpl for=".">
						                                          <div class="search-item">
							                                         <h3>{NAME}</h3>	                
						                                          </div>
					                                           </tpl>
                                        </Html>
                                    </Template>
                                    <Triggers>
                                        <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                    </Triggers>
                                    <Listeners>
                                        <TriggerClick Handler="this.setValue('');" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Nhập mã yêu cầu hoặc tên người đề nghị..."
                                    EnableKeyEvents="true" Width="300px">
                                    <Listeners>
                                        <KeyDown Buffer="300" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                        <TriggerClick Handler="#{dsYeuCauTongQuat}.reload();" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsYeuCauTongQuat}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsYeuCauTongQuat" runat="server" RemoteSort="true" UseIdConfirmation="true"
                            GroupField="LOAI_DP">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauTongQuat/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_PYC" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="LY_DO" />
                                        <ext:RecordField Name="NGAY_DE_NGHI" Type="Date" />
                                        <ext:RecordField Name="NGAY_CAP" Type="Date" />
                                        <ext:RecordField Name="LOAI" />
                                        <ext:RecordField Name="LOAI_DP" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                        <ext:RecordField Name="TRANG_THAI" />
                                        <ext:RecordField Name="TRANG_THAI_DP" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="loai" Value="#{LOAI_YEU_CAU}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="trangthai" Value="'4,5,6,10'" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_PYC" DataIndex="MA_PYC" Header="Mã số phiếu" Width="150" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người đề nghị" Width="150">
                            </ext:Column>
                            <ext:Column ColumnID="LY_DO" DataIndex="LY_DO" Header="Lý do yêu cầu" Width="200" />
                            <ext:DateColumn ColumnID="NGAY_DE_NGHI" DataIndex="NGAY_DE_NGHI" Header="Ngày đề nghị"
                                Width="100" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="LOAI_DP" DataIndex="LOAI_DP" Header="Loại" Width="200" />
                            <ext:Column ColumnID="TRANG_THAI_DP" DataIndex="TRANG_THAI_DP" Header="Trạng thái"
                                Width="200" />
                            <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" Width="200" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                             AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('LOAI'),#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'));
                             " />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="LY_DO" />
                                <ext:StringFilter DataIndex="NGAY_DE_NGHI" />
                                <ext:StringFilter DataIndex="MO_TA" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                            <GetRowClass Fn="getRowClass" />
                        </ext:GroupingView>
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="
                    setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                             AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('LOAI'),#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'));
                             " />
                    </Listeners>
                </ext:GridPanel>
                <ext:GridPanel ID="GridPanel2" RowHeight="0.6" Title="Chi tiết yêu cầu" runat="server"
                    Border="false" TrackMouseOver="true">
                    <Store>
                        <ext:Store ID="dsYeuCauChiTiet" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauChiTiet/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="ID" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="SO_LUONG" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150" />
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="150" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="150" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="150" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                            <ext:Column ColumnID="SO_LUONG" DataIndex="SO_LUONG" Header="Số lượng" Width="80"
                                Align="Right" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                            <Listeners>
                                <RowSelect Handler="" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                <ext:StringFilter DataIndex="SO_LUONG" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdXuatKho_YeuCauMoi" runat="server" Hidden="true" Title="Xuất kho thiết bị"
            Icon="Information" Width="810" Height="500">
            <Items>
                <ext:FormPanel ID="xkycmForm" runat="server" Height="470" Width="800" Border="false"
                    ButtonAlign="Center" Url="/Danhmuc/saveXuatkho">
                    <Items>
                        <ext:Hidden ID="id" runat="server" />
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:CompositeField ID="CompositeField2" runat="server" FieldLabel="Mã xuất kho"
                                    AnchorHorizontal="98%">
                                    <Items>
                                        <ext:TextField ID="MA_XK" DataIndex="MA_XK" Disabled="true" runat="server" AllowBlank="false"
                                            IndicatorCls="red" Flex="3" />
                                        <ext:DisplayField ID="DisplayField2" runat="server" Margins="0 0 0 10" Text="Ngày xuất kho"
                                            Flex="2" />
                                        <ext:DateField ID="NGAY_XK" DataIndex="NGAY_XK" runat="server" AllowBlank="false"
                                            IndicatorCls="red" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField3" runat="server" FieldLabel="Ghi chú" AnchorHorizontal="98%">
                                    <Items>
                                        <ext:TextArea ID="GHI_CHU" DataIndex="GHI_CHU" runat="server" Height="50" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                            </Items>
                        </ext:FieldSet>
                        <ext:Container ID="Container1" runat="server" Layout="HBoxLayout">
                            <Items>
                                <ext:FieldSet ID="FieldSet3" runat="server" Margins="0 0 0 0" Flex="1" Border="false"
                                    Layout="AnchorLayout" Width="800" Height="330">
                                    <Items>
                                        <ext:GridPanel ID="gpDanhSachThietBiDaChon" RowHeight="0.6" Title="Danh sách thiết bị đã chọn"
                                            runat="server" Border="true" Height="320" Selectable="true" Header="true" TrackMouseOver="true">
                                            <TopBar>
                                                <ext:Toolbar ID="Toolbar4" runat="server">
                                                    <Items>
                                                        <ext:Button ID="btnHuy" runat="server" Text="Hủy" Icon="Cancel" IconAlign="Left">
                                                            <Listeners>
                                                                <Click Fn="Huy" />
                                                            </Listeners>
                                                        </ext:Button>
                                                        <ext:ToolbarFill ID="ToolbarFill4" runat="server" />
                                                        <ext:Button ID="Button1" runat="server" Text="Chọn" Icon="ArrowRight" IconAlign="Right">
                                                            <Listeners>
                                                                <Click Handler="#{dstb_Form}.reload(); #{wd_DanhSachThietBi}.show();" />
                                                            </Listeners>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:Toolbar>
                                            </TopBar>
                                            <Store>
                                                <ext:Store ID="Store_TimKiemThietBiTheoMaNhapKho" runat="server" RemoteSort="false"
                                                    UseIdConfirmation="false" WarningOnDirty="false" GroupField="TEN_CL">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/TimKiemThietBiTheoMaXuatKho/" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="Check" />
                                                                <ext:RecordField Name="MA_TB" />
                                                                <ext:RecordField Name="TEN_TB" />
                                                                <ext:RecordField Name="SERIAL" />
                                                                <ext:RecordField Name="NGUYEN_GIA" />
                                                                <ext:RecordField Name="MA_CL" />
                                                                <ext:RecordField Name="TEN_CL" />
                                                                <ext:RecordField Name="MA_KIEU" />
                                                                <ext:RecordField Name="TEN_KIEU" />
                                                                <ext:RecordField Name="TRANG_THAI" />
                                                                <ext:RecordField Name="TEN_TT" />
                                                                <ext:RecordField Name="NGAY_BH" Type="Date" />
                                                                <ext:RecordField Name="THOI_GIAN_BH" />
                                                                <ext:RecordField Name="NGAY_KH" Type="Date" />
                                                                <ext:RecordField Name="THOI_GIAN_KH" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                        <ext:Parameter Name="maXK" Value="#{id}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="maPYC" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <ColumnModel ID="ColumnModel4" runat="server">
                                                <Columns>
                                                    <ext:CheckColumn ColumnID="Check" Hidden="true" DataIndex="Check" Header="Check">
                                                    </ext:CheckColumn>
                                                    <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100" />
                                                    <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200" />
                                                    <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100" />
                                                    <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100">
                                                        <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" />
                                                    </ext:Column>
                                                    <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                                                    <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200" />
                                                    <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200" />
                                                    <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" Header="Ngày bắt đầu bảo hành"
                                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                                    <ext:Column ColumnID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" Header="Thời gian bảo hành còn lại"
                                                        Width="150" />
                                                    <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" Header="Ngày bắt đầu khấu hao"
                                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                                    <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian khấu hao còn lại"
                                                        Width="150" />
                                                </Columns>
                                            </ColumnModel>
                                            <SelectionModel>
                                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" />
                                            </SelectionModel>
                                            <Plugins>
                                                <ext:GridFilters ID="GridFilters4" runat="server" Local="true">
                                                    <Filters>
                                                        <ext:StringFilter DataIndex="TEN_TB" />
                                                        <ext:StringFilter DataIndex="SERIAL" />
                                                        <ext:StringFilter DataIndex="NGUYEN_GIA" />
                                                        <ext:StringFilter DataIndex="TEN_CL" />
                                                        <ext:StringFilter DataIndex="TEN_KIEU" />
                                                        <ext:StringFilter DataIndex="NGAY_BH" />
                                                        <ext:StringFilter DataIndex="THOI_GIAN_BH" />
                                                        <ext:StringFilter DataIndex="NGAY_KH" />
                                                        <ext:StringFilter DataIndex="THOI_GIAN_KH" />
                                                    </Filters>
                                                </ext:GridFilters>
                                            </Plugins>
                                            <View>
                                                <ext:GroupingView ID="GroupingView2" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                                            </View>
                                            <LoadMask ShowMask="true" />
                                            <SaveMask ShowMask="true" />
                                        </ext:GridPanel>
                                    </Items>
                                </ext:FieldSet>
                            </Items>
                        </ext:Container>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{xkycmForm}.form.submit({waitMsg : 'Đang ghi...', params:{json: Ext.encode(#{gpDanhSachThietBiDaChon}.getRowsValues()),MA_PYC:#{ID_PYC}.getValue()},success : successHandler, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button5" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{xkycmForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
        </ext:Window>
        <ext:Window ID="wd_DanhSachThietBi" runat="server" Hidden="true" Title="Danh sách thiết bị"
            Icon="Information" Width="610" Height="400">
            <Items>
                <ext:FormPanel ID="dstb_Form" runat="server" Height="400" Width="600" Border="false"
                    ButtonAlign="Center" Url="">
                    <Items>
                        <ext:GridPanel ID="gpDanhSachThietBi" RowHeight="0.6"
                            runat="server" Border="true" AnchorHorizontal="100%" Height="365" Selectable="true" Header="true" TrackMouseOver="true">
                            <TopBar>
                                <ext:Toolbar ID="Toolbar3" runat="server">
                                    <Items>

                                     
                                        <ext:Button ID="btnChonLua" runat="server" Text="Chọn" Icon="ArrowLeft" IconAlign="Left">
                                            <Listeners>
                                                <Click Fn="ChonLua" />
                                            </Listeners>
                                        </ext:Button>
                                        <ext:ToolbarFill ID="ToolbarFill3" runat="server" />
                                        <ext:TriggerField ID="TriggerField1" runat="server" EmptyText="Nhập số serial hoặc tên thiết bị..."
                                            EnableKeyEvents="true" Width="300px">
                                            <Listeners>
                                                <KeyDown Buffer="300" Handler="#{TB_S_T}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                                <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                                <TriggerClick Handler="#{Store_dsThietBi}.reload();" />
                                            </Listeners>
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                <ext:FieldTrigger Icon="Search" />
                                            </Triggers>
                                        </ext:TriggerField>
                                        <ext:Hidden ID="TB_S_T" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                            <Listeners>
                                                <Change Handler="#{Store_dsThietBi}.reload();" Delay="100" />
                                            </Listeners>
                                        </ext:Hidden>
                                       
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="Store_dsThietBi" runat="server" RemoteSort="false" UseIdConfirmation="false"
                                    WarningOnDirty="false" GroupField="TEN_CL">
                                    <Proxy>
                                        <ext:HttpProxy Url="/Danhmuc/dsThietBi/" />
                                    </Proxy>
                                    <Reader>
                                        <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                            <Fields>
                                                <ext:RecordField Name="Check" />
                                                <ext:RecordField Name="MA_TB" />
                                                <ext:RecordField Name="TEN_TB" />
                                                <ext:RecordField Name="SERIAL" />
                                                <ext:RecordField Name="NGUYEN_GIA" />
                                                <ext:RecordField Name="MA_CL" />
                                                <ext:RecordField Name="TEN_CL" />
                                                <ext:RecordField Name="MA_KIEU" />
                                                <ext:RecordField Name="TEN_KIEU" />
                                                <ext:RecordField Name="TRANG_THAI" />
                                                <ext:RecordField Name="TEN_TT" />
                                                <ext:RecordField Name="NGAY_BH" Type="Date" />
                                                <ext:RecordField Name="THOI_GIAN_BH" />
                                                <ext:RecordField Name="NGAY_KH" Type="Date" />
                                                <ext:RecordField Name="THOI_GIAN_KH" />
                                            </Fields>
                                        </ext:JsonReader>
                                    </Reader>
                                    <BaseParams>
                                        <ext:Parameter Name="txtfilter" Value="#{TB_S_T}.getValue()" Mode="Raw" />
                                        <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                        <ext:Parameter Name="trangthai" Value="2" Mode="Raw" />
                                    </BaseParams>
                                </ext:Store>
                            </Store>
                            <ColumnModel ID="ColumnModel3" runat="server">
                                <Columns>
                                    <ext:CheckColumn ColumnID="Check" Hidden="true" DataIndex="Check" Header="Check">
                                    </ext:CheckColumn>
                                    <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100" />
                                    <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100" />
                                    <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200" />
                                    <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                                    <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200" />
                                    <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100">
                                        <Renderer Fn="Ext.util.Format.numberRenderer('0,000')" />
                                    </ext:Column>
                                    <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200" />
                                    <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" Header="Ngày bắt đầu bảo hành"
                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                    <ext:Column ColumnID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" Header="Thời gian bảo hành còn lại"
                                        Width="150" />
                                    <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" Header="Ngày bắt đầu khấu hao"
                                        Width="150" Format="dd/MM/yyyy" Align="Center" />
                                    <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian khấu hao còn lại"
                                        Width="150" />
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel2" runat="server" />
                            </SelectionModel>
                            <Plugins>
                                <ext:GridFilters ID="GridFilters3" runat="server" Local="true">
                                    <Filters>
                                        <ext:StringFilter DataIndex="TEN_TB" />
                                        <ext:StringFilter DataIndex="SERIAL" />
                                        <ext:StringFilter DataIndex="NGUYEN_GIA" />
                                        <ext:StringFilter DataIndex="TEN_CL" />
                                        <ext:StringFilter DataIndex="TEN_KIEU" />
                                        <ext:StringFilter DataIndex="NGAY_BH" />
                                        <ext:StringFilter DataIndex="THOI_GIAN_BH" />
                                        <ext:StringFilter DataIndex="NGAY_KH" />
                                        <ext:StringFilter DataIndex="THOI_GIAN_KH" />
                                    </Filters>
                                </ext:GridFilters>
                            </Plugins>
                            <View>
                                <ext:GroupingView ID="GroupingView1" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                            </View>
                            <LoadMask ShowMask="true" />
                            <SaveMask ShowMask="true" />
                        </ext:GridPanel>
                    </Items>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
        </ext:Window>
        <ext:Window ID="wdXuatKho_ThanhLy" runat="server" Hidden="true" Title="Xuất kho thanh lý thiết bị"
            Icon="Information" Width="810" Height="500">
            <Items>
                <ext:FormPanel ID="xktlForm" runat="server" Height="470" Width="800" Border="false"
                    ButtonAlign="Center" Url="/Danhmuc/saveXuatkho_ThanhLy">
                    <Items>
                        <ext:Hidden ID="Hidden1" runat="server" />
                        <ext:FieldSet ID="FieldSet4" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:CompositeField ID="CompositeField1" runat="server" FieldLabel="Mã xuất kho"
                                    AnchorHorizontal="98%">
                                    <Items>
                                        <ext:TextField ID="MA_XK_TL" Disabled="true" DataIndex="MA_XK_TL" runat="server"
                                            AllowBlank="false" IndicatorCls="red" Flex="3" />
                                        <ext:DisplayField ID="DisplayField1" runat="server" Margins="0 0 0 10" Text="Ngày xuất kho"
                                            Flex="2" />
                                        <ext:DateField ID="NGAY_XK_TL" DataIndex="NGAY_XK_TL" runat="server" AllowBlank="false"
                                            IndicatorCls="red" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField4" runat="server" FieldLabel="Ghi chú" AnchorHorizontal="98%">
                                    <Items>
                                        <ext:TextArea ID="GHI_CHU_TL" DataIndex="GHI_CHU_TL" runat="server" Height="50" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:GridPanel ID="gdXuaKhoThanhLy" RowHeight="0.6" Title="Danh sách thiết" runat="server"
                                    Border="true" Height="320" Selectable="true" Header="true" TrackMouseOver="true">
                                    <Store>
                                        <ext:Store ID="TimKiemTheoMaYeuCau_TL" runat="server" RemoteSort="false" UseIdConfirmation="false"
                                            WarningOnDirty="false" GroupField="TEN_CL">
                                            <Proxy>
                                                <ext:HttpProxy Url="/Danhmuc/TimKiemTheoMaYeuCau/" />
                                            </Proxy>
                                            <Reader>
                                                <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                                    <Fields>
                                                        <ext:RecordField Name="MA_TB" />
                                                        <ext:RecordField Name="TEN_TB" />
                                                        <ext:RecordField Name="SERIAL" />
                                                        <ext:RecordField Name="MA_CL" />
                                                        <ext:RecordField Name="TEN_CL" />
                                                        <ext:RecordField Name="MA_KIEU" />
                                                        <ext:RecordField Name="TEN_KIEU" />
                                                    </Fields>
                                                </ext:JsonReader>
                                            </Reader>
                                            <BaseParams>
                                                <ext:Parameter Name="maYC" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                            </BaseParams>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel ID="ColumnModel6" runat="server">
                                        <Columns>
                                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="100" />
                                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200" />
                                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100" />
                                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200" />
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server">
                                            <Listeners>
                                                <RowSelect Handler="" />
                                            </Listeners>
                                        </ext:RowSelectionModel>
                                    </SelectionModel>
                                    <Plugins>
                                        <ext:GridFilters ID="GridFilters6" runat="server" Local="true">
                                            <Filters>
                                                <ext:StringFilter DataIndex="TEN_TB" />
                                                <ext:StringFilter DataIndex="SERIAL" />
                                                <ext:StringFilter DataIndex="TEN_CL" />
                                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                            </Filters>
                                        </ext:GridFilters>
                                    </Plugins>
                                    <View>
                                        <ext:GroupingView ID="GroupingView4" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                                    </View>
                                    <LoadMask ShowMask="true" />
                                    <SaveMask ShowMask="true" />
                                </ext:GridPanel>
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar7" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill5" runat="server" />
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{xktlForm}.form.submit({waitMsg : 'Đang ghi...', params:{json: Ext.encode(#{gdXuaKhoThanhLy}.getRowsValues()),MA_PYC:#{ID_PYC}.getValue()},success : successHandler, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button4" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{xktlForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
        </ext:Window>
    </div>
</body>
</html>
