﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Thẩm định yêu cầu</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
        
        
        .x-form-group .x-form-group-header-text
        {
            background-color: #dfe8f6;
        }
        
        .x-label-text
        {
            font-weight: bold;
            font-size: 11px;
        }
        
        .red
        {
            background-color: Red;
            color: Black;
        }
        
        .green
        {
            background-color: Green;
            color: Black;
        }
        
        .yellow
        {
            background-color: Yellow;
            color: Black;
        }
        .silver
        {
            background-color: Silver;
            color: Black;
        }
    </style>
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsYeuCauTongQuat.reload();
            tdycForm.reset();
            tdttForm.reset();
            tdbhForm.reset();
            wdThamDinhYeuCau.hide();
            wdThamDinhThayThe.hide();
            wdThamDinhBaoHanh.hide();

        };




        var ThamDinhYeuCau_KD = function () {
            TRANG_THAI.setValue(3);
            var record = GridPanel1.getSelectionModel().getSelected();
            tdycForm.getForm().loadRecord(record);
            wdThamDinhYeuCau.show();
        };

        var ThamDinhYeuCau_PD = function () {
            TRANG_THAI.setValue(4);
            var record = GridPanel1.getSelectionModel().getSelected();
            tdycForm.getForm().loadRecord(record);
            wdThamDinhYeuCau.show();
        };

        var ThamDinhYeuCau_TT = function () {
            TRANG_THAI.setValue(15);
            var record = GridPanel1.getSelectionModel().getSelected();
            tdttForm.getForm().loadRecord(record);
            wdThamDinhThayThe.show();
        };


        var ThamDinhYeuCau_TS = function () {
            TRANG_THAI.setValue(15);
            var record = GridPanel1.getSelectionModel().getSelected();
            tdycForm.getForm().loadRecord(record);
            wdThamDinhYeuCau.show();
        };

        var ThamDinhYeuCau_BH = function () {
            TRANG_THAI.setValue(7);
            var record = GridPanel1.getSelectionModel().getSelected();
            tdbhForm.getForm().loadRecord(record);
            wdThamDinhBaoHanh.show();
        };


        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };



        $(document).ready(function () {
            AnHienChucNang(0, 0);
        });

        function AnHienChucNang(tt, l) {
            $.post("/Danhmuc/AnHienChucNang", { trangthai: tt, loai: l }, function (data) {
                if (data.CHO_DUYET == true) {
                    $('#btnChoDuyet').show();
                }
                else {
                    $('#btnChoDuyet').hide();
                }

                if (data.KHONG_DUYET == true) {
                    $('#btnKhongDuyet').show();
                }
                else {
                    $('#btnKhongDuyet').hide();
                }


                if (data.DA_DUYET == true) {
                    $('#btnDaDuyet').show();
                }
                else {
                    $('#btnDaDuyet').hide();
                }

                if (data.BAO_HANH == true) {
                    $('#btnDuyetBaoHanh').show();
                }
                else {
                    $('#btnDuyetBaoHanh').hide();
                }


                if (data.TU_SUA == true) {
                    $('#btnDuyetTuSua').show();
                }
                else {
                    $('#btnDuyetTuSua').hide();
                }

                if (data.THAY_THE == true) {
                    $('#btnDuyetThayThe').show();
                }
                else {
                    $('#btnDuyetThayThe').hide();
                }

            });
        }

        function getRowClass(record) {
            if (record.data.TRANG_THAI === 2) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 3) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 4) {
                return 'red';
            }

            if (record.data.TRANG_THAI === 5) {
                return 'yellow';
            }

        };



    </script>
    <script runat="server">
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }

            this.Store1.DataSource = new object[]
            {
                new object[] {1,"Yêu cầu cấp mới"},
                new object[] {2,"Yêu cầu sửa"},
                new object[] {3,"Yêu cầu thu hồi"},
                new object[] {4,"Yêu cầu thanh lý"}       
            };
            this.Store1.DataBind();

        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:Hidden ID="TRANG_THAI" runat="server" />
                <ext:Hidden ID="ID_PYC" runat="server" />
                <ext:GridPanel ID="GridPanel1" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="btnChoDuyet" runat="server" Text="Chờ duyệt" Icon="ApplicationGo">
                                    <DirectEvents>
                                        <Click Url="/Danhmuc/ChoDuyet" CleanRequest="true" Method="POST" Success="#{dsYeuCauTongQuat}.reload();">
                                            <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="Bạn có chắc chắn muốn thực hiện?" />
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC')"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnKhongDuyet" runat="server" Text="Không duyệt" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="ThamDinhYeuCau_KD" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnDaDuyet" runat="server" Text="Phê duyệt" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="ThamDinhYeuCau_PD" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnDuyetThayThe" runat="server" Text="Thay thế" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="ThamDinhYeuCau_TT" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnDuyetTuSua" runat="server" Text="Tự sửa" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="ThamDinhYeuCau_TS" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnDuyetBaoHanh" runat="server" Text="Bảo hành" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="ThamDinhYeuCau_BH" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:ComboBox ID="LOAI_YEU_CAU" AllowBlank="false" Flex="3" DataIndex="PHONG" runat="server"
                                    Mode="Local" ValueField="ID" DisplayField="NAME" FieldLabel="Điều kiện lọc" ItemSelector="div.search-item"
                                    EmptyText="Chọn loại yêu cầu..." Width="250">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                            <Reader>
                                                <ext:ArrayReader IDProperty="ID">
                                                    <Fields>
                                                        <ext:RecordField Name="ID" SortDir="ASC" />
                                                        <ext:RecordField Name="NAME" />
                                                    </Fields>
                                                </ext:ArrayReader>
                                            </Reader>
                                        </ext:Store>
                                    </Store>
                                    <Template ID="Template1" runat="server">
                                        <Html>
                                            <tpl for=".">
						                                          <div class="search-item">
							                                         <h3>{NAME}</h3>	                
						                                          </div>
					                                           </tpl>
                                        </Html>
                                    </Template>
                                    <Triggers>
                                        <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                    </Triggers>
                                    <Listeners>
                                        <TriggerClick Handler="this.setValue('');" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Nhập mã yêu cầu hoặc tên người đề nghị..."
                                    EnableKeyEvents="true" Width="300px">
                                    <Listeners>
                                        <KeyDown Buffer="300" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                        <TriggerClick Handler="#{dsYeuCauTongQuat}.reload();" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsYeuCauTongQuat}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsYeuCauTongQuat" runat="server" RemoteSort="true" UseIdConfirmation="true"
                            GroupField="LOAI_DP">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauTongQuat/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_PYC" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="LY_DO" />
                                        <ext:RecordField Name="NGAY_DE_NGHI" Type="Date" />
                                        <ext:RecordField Name="NGAY_CAP" Type="Date" />
                                        <ext:RecordField Name="LOAI" />
                                        <ext:RecordField Name="LOAI_DP" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                        <ext:RecordField Name="TRANG_THAI" />
                                        <ext:RecordField Name="TRANG_THAI_DP" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="loai" Value="#{LOAI_YEU_CAU}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="trangthai" Value="'1,2,3'" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_PYC" DataIndex="MA_PYC" Header="Mã số phiếu" Width="150" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người đề nghị" Width="150">
                            </ext:Column>
                            <ext:Column ColumnID="LY_DO" DataIndex="LY_DO" Header="Lý do yêu cầu" Width="200" />
                            <ext:DateColumn ColumnID="NGAY_DE_NGHI" DataIndex="NGAY_DE_NGHI" Header="Ngày đề nghị"
                                Width="100" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="LOAI_DP" DataIndex="LOAI_DP" Header="Loại" Width="200" />
                            <ext:Column ColumnID="TRANG_THAI_DP" DataIndex="TRANG_THAI_DP" Header="Trạng thái"
                                Width="200" />
                            <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" Width="200" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="
                             AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'),#{GridPanel1}.getSelectionModel().getSelected().get('LOAI'));
                             setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                             " />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="LY_DO" />
                                <ext:StringFilter DataIndex="NGAY_DE_NGHI" />
                                <ext:StringFilter DataIndex="MO_TA" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                            <GetRowClass Fn="getRowClass" />
                        </ext:GroupingView>
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'),#{GridPanel1}.getSelectionModel().getSelected().get('LOAI'));
                    setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));" />
                    </Listeners>
                </ext:GridPanel>
                <ext:GridPanel ID="GridPanel2" RowHeight="0.6" Title="Chi tiết yêu cầu" runat="server"
                    Border="false" TrackMouseOver="true">
                    <Store>
                        <ext:Store ID="dsYeuCauChiTiet" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauChiTiet/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="ID" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="SO_LUONG" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150" />
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="150" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="150" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="150" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                            <ext:Column ColumnID="SO_LUONG" DataIndex="SO_LUONG" Header="Số lượng" Width="80"
                                Align="Right" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                            <Listeners>
                                <RowSelect Handler="" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                <ext:StringFilter DataIndex="SO_LUONG" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdThamDinhYeuCau" runat="server" Hidden="true" Title="Thẩm định yêu cầu"
            Icon="Information" Width="530" Height="150" Resizable="false">
            <Items>
                <ext:FormPanel ID="tdycForm" runat="server" Height="120" Width="520" Border="false"
                    Url="/Danhmuc/ThamDinhYeuCau" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:TextArea ID="TD_GHI_CHU" DataIndex="TD_GHI_CHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="95%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{tdycForm}.form.submit({waitMsg : 'Đang ghi...',params:{MA_PYC:#{ID_PYC}.getValue(),TRANG_THAI:#{TRANG_THAI}.getValue()},success : successHandler, failure : failureHandler});" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{tdycForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
        </ext:Window>
        <ext:Window ID="wdThamDinhThayThe" runat="server" Hidden="true" Title="Thẩm định yêu cầu thay thế"
            Icon="Information" Width="530" Height="150" Resizable="false">
            <Items>
                <ext:FormPanel ID="tdttForm" runat="server" Height="120" Width="520" Border="false"
                    Url="/Danhmuc/ThamDinhThayThe" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet2" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:TextArea ID="TDTT_GHI_CHU" DataIndex="TDTT_GHI_CHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="95%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar3" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill3" runat="server" />
                                <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{tdttForm}.form.submit({waitMsg : 'Đang ghi...',params:{json: Ext.encode(#{GridPanel2}.getRowsValues()),MA_PYC:#{ID_PYC}.getValue(),TRANG_THAI:#{TRANG_THAI}.getValue()},success : successHandler, failure : failureHandler});" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button2" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{tdttForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
        </ext:Window>
        <ext:Window ID="wdThamDinhBaoHanh" runat="server" Hidden="true" Title="Thẩm định bảo hành"
            Icon="Information" Width="530" Height="200" Resizable="false">
            <Items>
                <ext:FormPanel ID="tdbhForm" runat="server" Height="170" Width="520" Border="false"
                    Url="/Danhmuc/ThamDinhBaoHanh" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet4" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:RadioGroup ID="grBaoHanh" runat="server" Flex="3" FieldLabel="Giới tính">
                                    <Items>
                                        <ext:Radio ID="rdBaoHanh" runat="server" BoxLabel="Đi bảo hành" Checked="true" InputValue="baohanh" />
                                        <ext:Radio ID="rdSuaNgoai" runat="server" BoxLabel="Sửa ngoài" InputValue="suangoai" />
                                    </Items>
                                </ext:RadioGroup>
                            </Items>
                        </ext:FieldSet>
                        <ext:FieldSet ID="FieldSet3" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:TextArea ID="TDBH_GHI_CHU" DataIndex="TDBH_GHI_CHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="95%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar4" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill4" runat="server" />
                                <ext:Button ID="Button4" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{tdbhForm}.form.submit({waitMsg : 'Đang ghi...',params:{json: Ext.encode(#{GridPanel2}.getRowsValues()),MA_PYC:#{ID_PYC}.getValue(),TRANG_THAI:#{TRANG_THAI}.getValue()},success : successHandler, failure : failureHandler});" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button5" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{tdbhForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer3" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
        </ext:Window>
    </div>
</body>
</html>
