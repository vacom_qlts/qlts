﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {    
        Ext.Net.Parameter pa1 = new Ext.Net.Parameter();
        pa1.Name = "ma_xe";
        pa1.Value = ViewData["ma_xe"].ToString();
        pa1.Mode = ParameterMode.Value;
        
        LoadConfig lc = new LoadConfig();
        lc.Params.Add(pa1);
        
        lc.Method = HttpMethod.POST;
        lc.Url = "/Danhmuc/getPhieu";
        this.panel1.LoadContent(lc);           
    }
    
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">   
    <title>In phi?u yêu c?u</title>  
    <style type="text/css">
        .container
        {
            margin-left:70px;
            margin-top:10px;                   
            }
        .label span
        {
            font-weight: bold;
            }
        
    </style>
    <script type="text/javascript">
        
        
    </script>
</head>
<body>
    <div>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />  
        <ext:Panel runat="server" ID="panel1" Width="570" Height="590">
            <AutoLoad Mode="IFrame">
                        
            </AutoLoad>      
        </ext:Panel>
    </div>
</body>
</html>
