﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Globalization;
using System.Threading;
using System.IO;
using QUANLYBANHANG.Models;

namespace QUANLYBANHANG
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public sealed class LanguageManager
    {
        /// <summary>
        /// Default CultureInfo
        /// </summary>
        public static readonly CultureInfo DefaultCulture = new CultureInfo("vi-VN");

        /// <summary>
        /// Available CultureInfo that according resources can be found
        /// </summary>
        public static readonly CultureInfo[] AvailableCultures;

        static LanguageManager()
        {
            //
            // Available Cultures
            //
            List<string> availableResources = new List<string>();
            string resourcespath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "App_GlobalResources");
            DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
            foreach (FileInfo fi in dirInfo.GetFiles("*.*.resx", SearchOption.AllDirectories))
            {
                //Take the cultureName from resx filename, will be smt like en-US
                string cultureName = Path.GetFileNameWithoutExtension(fi.Name); //get rid of .resx
                if (cultureName.LastIndexOf(".") == cultureName.Length - 1)
                    continue; //doesnt accept format FileName..resx
                cultureName = cultureName.Substring(cultureName.LastIndexOf(".") + 1);
                availableResources.Add(cultureName);
            }

            List<CultureInfo> result = new List<CultureInfo>();
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                //If language file can be found
                if (availableResources.Contains(culture.ToString()))
                {
                    result.Add(culture);
                }
            }

            AvailableCultures = result.ToArray();

            //
            // Current Culture
            //
            CurrentCulture = DefaultCulture;
            // If default culture is not available, take another available one to use
            if (!result.Contains(DefaultCulture) && result.Count > 0)
            {
                CurrentCulture = result[0];
            }
        }

        /// <summary>
        /// Current selected culture
        /// </summary>
        public static CultureInfo CurrentCulture
        {
            get { return Thread.CurrentThread.CurrentCulture; }
            set
            {
                //NOTE:                
                Thread.CurrentThread.CurrentUICulture = value;
                Thread.CurrentThread.CurrentCulture = value;
            }
        }
    }



    public class MvcApplication : System.Web.HttpApplication
    {
        private const string SESSION_KEY_LANGUAGE = "CURRENT_LANGUAGE";

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            QLBHDataContext qlbh = new QLBHDataContext();
            CultureInfo ci;
            var lang = (from p in qlbh.ACCOUNTs
                           where p.Username == User.Identity.Name
                           select new
                           {
                               p.Username,
                               p.Language,
                               p.Theme
                           });
            //if (lang.Count() >0 && !string.IsNullOrEmpty(lang.First().Theme))
            //    if(lang.First().Theme == "Slate")
            //    Session["theme"] = Ext.Net.Theme.Gray;
            if (lang.Count()==0 || string.IsNullOrEmpty(lang.First().Language))
            {
                ci = new CultureInfo("vi-VN");                
            }
            else
            {
                ci = new CultureInfo(lang.First().Language);                
            }
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);            
        }

        protected void Application_AuthenticateRequest(object sender, System.EventArgs e)
        {
            string url = HttpContext.Current.Request.RawUrl.ToLower();
            if (url.Contains("ext.axd"))
            {
                HttpContext.Current.SkipAuthorization = true;
            }
            else if (url.Contains("returnurl=/default.aspx") || url.Contains("returnurl=%2fdefault.aspx"))
            {
                Response.Redirect(url.Replace("returnurl=/default.aspx", "r=/").Replace("returnurl=%2fdefault.aspx", "r=/"));
            }
        } 

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{exclude}/{extnet}/ext.axd");

            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Home", action = "Index", id = "" }  // Parameter defaults
            );

            /**/

            routes.MapRoute(
              "Default2",
              "{controller}.mvc/{action}/{id}",
              new { action = "Index", id = "" }
            );

            routes.MapRoute(
              "Root",
              "",
              new { controller = "Home", action = "Index", id = "" }
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
        }
    }
}