﻿using System;
using System.Drawing;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using QUANLYBANHANG.Models;

namespace QUANLYBANHANG.Report
{
    public partial class rpt_Phieu_De_Xuat_Thanh_Ly_Thiet_Bi : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_Phieu_De_Xuat_Thanh_Ly_Thiet_Bi(string MA_PYC)
        {
            QLBHDataContext aQLBHDataContext = new QLBHDataContext();
            InitializeComponent();
            try
            {
                var ttc = (from pyc in aQLBHDataContext.PHIEU_YEU_CAUs
                           join nv in aQLBHDataContext.NHANVIENs on pyc.MA_NV equals nv.MA_NV
                           join cv in aQLBHDataContext.DM_CHUC_VUs on nv.MA_CV equals cv.MA_CV
                           join pb in aQLBHDataContext.DM_PHONG_BANs on nv.MA_PB equals pb.MA_PB
                           join dv in aQLBHDataContext.DM_DONVIs on nv.MA_DV equals dv.MA_DV
                           select new 
                           { 
                                nv.TEN_NV,
                                cv.TEN_CV,
                                pb.TEN_PB,
                                dv.TEN_DV,
                                pyc.LY_DO,
                                pyc.MA_PYC

                           }).Distinct().ToList();
                lblNgay.Text = "TP. Hồ Chí Minh, ngày "+DateTime.Now.Day.ToString()+" tháng " +DateTime.Now.Month.ToString()+" năm "+DateTime.Now.Year.ToString();
                if(ttc.Count > 0)
                {
                    lblMPYC.Text = ttc[0].MA_PYC;
                    lblNDN.Text = ttc[0].TEN_NV;
                    lblCV.Text = ttc[0].TEN_CV;
                    lblPB.Text = ttc[0].TEN_PB;
                    lblDV.Text = ttc[0].TEN_DV;
                    lblLD.Text = ttc[0].LY_DO;
                    lblNDN_KY.Text = ttc[0].TEN_NV;
                }


                var dsTB = (from yc in aQLBHDataContext.PHIEUYEUCAU_THIETBIs
                          join tb in aQLBHDataContext.DM_THIET_BIs on yc.SERIAL equals tb.SERIAL
                          join cl in aQLBHDataContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                          join k in aQLBHDataContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                          join nv in aQLBHDataContext.NHANVIENs on yc.MA_NV equals nv.MA_NV
                          where yc.MA_PYC.Equals(MA_PYC)
                          select new
                          {
                              tb.TEN_TB,
                              tb.MA_TB,
                              tb.SERIAL,
                              cl.TEN_CL,
                              k.TEN_KIEU,
                              nv.TEN_NV
                          }).ToList();


                this.DataSource = dsTB;
                colTenThietBi.DataBindings.Add("Text", this.DataSource, "TEN_TB");
                colMaThietBi.DataBindings.Add("Text", this.DataSource, "MA_TB");
                colSerial.DataBindings.Add("Text", this.DataSource, "SERIAL");
                colChungLoai.DataBindings.Add("Text", this.DataSource, "TEN_CL");
                colKieu.DataBindings.Add("Text", this.DataSource, "TEN_KIEU");
                colNSD.DataBindings.Add("Text", this.DataSource, "TEN_NV");

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

    }
}
