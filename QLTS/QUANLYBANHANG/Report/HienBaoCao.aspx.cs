﻿using QUANLYBANHANG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.UI;
using System.Web.UI.WebControls;

namespace QUANLYBANHANG.Report
{
    public partial class HienBaoCao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string MaNV = string.Empty;
                if (Request.Cookies["remember"] != null)
                {
                    MaNV = Request.Cookies["remember"].Values.Get(2);
                }
                else
                {
                    MaNV = string.Empty;
                }
                string code = Request.QueryString["bc"];
                string ts = Request.QueryString["ts"];
                switch (code)
                {
                    case "bktb":
                        List<THIET_BI_EN> dsThietBi = (List<THIET_BI_EN>)Session["BANG_KE_TB"];
                        rptView.Report = new QUANLYBANHANG.Report.rpt_BangKeThietBi(dsThietBi);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_BangKeThietBi";
                        break;
                    case "thsdtb":
                        List<THIET_BI_EN> dsTinhHinhSuDungThietBi = (List<THIET_BI_EN>)Session["TINH_HINH_SDTB"];
                        rptView.Report = new QUANLYBANHANG.Report.rpt_Tinh_Hinh_SDTB(dsTinhHinhSuDungThietBi);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_Tinh_Hinh_SDTB";
                        break;
                    case "xk_ycm":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_PhieuXuatKho_YCM(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_PhieuXuatKho_YCM";
                        break;
                    case "xk_tl":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_PhieuXuatKho_TL(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_PhieuXuatKho_TL";
                        break;
                    case "pycm":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_Phieu_De_Xuat_Cap_Thiet_Bi(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_Phieu_De_Xuat_Cap_Thiet_Bi";
                        break;
                    case "pycsc":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_Phieu_De_Xuat_Sua_Thiet_Bi(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_Phieu_De_Xuat_Sua_Thiet_Bi";
                        break;
                    case "pycth":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_Phieu_De_Xuat_Thu_Hoi_Thiet_Bi(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_Phieu_De_Xuat_Thu_Hoi_Thiet_Bi";
                        break;
                    case "pyctl":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_Phieu_De_Xuat_Thanh_Ly_Thiet_Bi(ts);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_Phieu_De_Xuat_Thanh_Ly_Thiet_Bi";
                        break;
                    case "nhakho_th":
                        rptView.Report = new QUANLYBANHANG.Report.rpt_PhieuNhapThuHoi(ts,MaNV);
                        rptView.ReportName = "QUAN_LY_CCID_250415.Report.rpt_PhieuNhapThuHoi";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}