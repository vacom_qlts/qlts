﻿using System;
using System.Web;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using QUANLYBANHANG.Models;


namespace QUANLYBANHANG.Report
{
    public partial class rpt_PhieuNhapThuHoi : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_PhieuNhapThuHoi(string MA_PYC,string NVNK)
        {
            InitializeComponent();
            try
            {
                QLBHDataContext aQLBHDataContext = new QLBHDataContext();
                lblNgayTaoPhieu.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                lblMYC.Text = MA_PYC;

                var query = (from y in aQLBHDataContext.PHIEU_YEU_CAUs
                             join ndn in aQLBHDataContext.NHANVIENs on y.MA_NV equals ndn.MA_NV
                             join yc in aQLBHDataContext.PHIEUYEUCAU_THIETBIs on y.MA_PYC equals yc.MA_PYC
                             join cl in aQLBHDataContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                             join k in  aQLBHDataContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                             join nsd in aQLBHDataContext.NHANVIENs on yc.MA_NV equals nsd.MA_NV
                             join tb in aQLBHDataContext.DM_THIET_BIs on yc.SERIAL equals tb.SERIAL
                             where y.MA_PYC.Equals(MA_PYC)
                             select new
                             {
                                 MA_TB=tb.MA_TB,
                                 TEN_TB=tb.TEN_TB,
                                 SERIAL=tb.SERIAL,
                                 TEN_CL=cl.TEN_CL,
                                 TEN_KIEU=k.TEN_KIEU,
                                 NSD=nsd.TEN_NV,
                                 NDN = ndn.TEN_NV

                             }).Distinct();


                this.DataSource = query;
                colMA_TB.DataBindings.Add("Text", this.DataSource, "MA_TB");
                colTEN_TB.DataBindings.Add("Text", this.DataSource, "TEN_TB");
                colSERIAL.DataBindings.Add("Text", this.DataSource, "SERIAL");
                colTEN_CL.DataBindings.Add("Text", this.DataSource, "TEN_CL");
                colTEN_KIEU.DataBindings.Add("Text", this.DataSource, "TEN_KIEU");
                colNSD.DataBindings.Add("Text", this.DataSource, "NSD");

                if (query.ToList().Count > 0)
                {
                    lblNguoiDeNghi.Text = query.ToList()[0].NDN;
                }

                List<NHANVIEN> ds=aQLBHDataContext.NHANVIENs.Where(n=>n.MA_NV.Equals(NVNK)).ToList();
                if(ds.Count >0)
                {
                    lblNguoiNhapKho.Text = ds[0].TEN_NV;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}
