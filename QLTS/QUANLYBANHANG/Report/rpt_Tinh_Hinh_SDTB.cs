﻿using System;
using System.Web;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using QUANLYBANHANG.Models;


namespace QUANLYBANHANG.Report
{
    public partial class rpt_Tinh_Hinh_SDTB : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_Tinh_Hinh_SDTB(List<THIET_BI_EN> aList)
        {
            InitializeComponent();
            try
            {

                lblNgay.Text = "Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString();

                this.DataSource = aList;

                // Create a group header band and add it to the report.
                GroupHeaderBand ghBand = new GroupHeaderBand();
                this.Bands.Add(ghBand);
                // Create a group field, 
                // and assign it to the group header band.
                GroupField groupField = new GroupField("TEN_DV");
                ghBand.GroupFields.Add(groupField);
                colTEN_DV.DataBindings.Add("Text", this.DataSource, "TEN_DV");

                colMA_TB.DataBindings.Add("Text", this.DataSource, "MA_TB");
                colTEN_TB.DataBindings.Add("Text", this.DataSource, "TEN_TB");
                colSERIAL.DataBindings.Add("Text", this.DataSource, "SERIAL");
                colTEN_CL.DataBindings.Add("Text", this.DataSource, "TEN_CL");
                colTEN_KIEU.DataBindings.Add("Text", this.DataSource, "TEN_KIEU");
                colTEN_PB.DataBindings.Add("Text", this.DataSource, "TEN_PB");
                colNSD.DataBindings.Add("Text", this.DataSource, "TEN_NV");
                


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}
