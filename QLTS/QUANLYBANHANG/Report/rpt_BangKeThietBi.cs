﻿using System;
using System.Web;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using QUANLYBANHANG.Models;


namespace QUANLYBANHANG.Report
{
    public partial class rpt_BangKeThietBi : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_BangKeThietBi(List<THIET_BI_EN> aList)
        {
            InitializeComponent();
            try
            {
                lblNgay.Text = "Ngày " +DateTime.Now.Day.ToString()+" Tháng " +DateTime.Now.Month.ToString()+ " Năm " + DateTime.Now.Year.ToString();

                this.DataSource = aList;

                // Create a group header band and add it to the report.
                GroupHeaderBand ghBand = new GroupHeaderBand();
                this.Bands.Add(ghBand);
                // Create a group field, 
                // and assign it to the group header band.
                GroupField groupField = new GroupField("TEN_TT");
                ghBand.GroupFields.Add(groupField);
                colTT.DataBindings.Add("Text", this.DataSource, "TEN_TT");

                colMA_TB.DataBindings.Add("Text", this.DataSource, "MA_TB");
                colTEN_TB.DataBindings.Add("Text", this.DataSource, "TEN_TB");
                colSERIAL.DataBindings.Add("Text", this.DataSource, "SERIAL");
                colTEN_CL.DataBindings.Add("Text", this.DataSource, "TEN_CL");
                colTEN_KIEU.DataBindings.Add("Text", this.DataSource, "TEN_KIEU");
                colNBH.DataBindings.Add("Text", this.DataSource, "NGAY_BH","{0:dd-MM-yyyy}");
                colTGBHCL.DataBindings.Add("Text", this.DataSource, "THOI_GIAN_BHCL");
                colNKH.DataBindings.Add("Text", this.DataSource, "NGAY_KH", "{0:dd-MM-yyyy}");
                colTGKHCL.DataBindings.Add("Text", this.DataSource, "THOI_GIAN_KHCL");

                


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}
