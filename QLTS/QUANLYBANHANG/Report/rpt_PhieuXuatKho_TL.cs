﻿using System;
using System.Web;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using QUANLYBANHANG.Models;


namespace QUANLYBANHANG.Report
{
    public partial class rpt_PhieuXuatKho_TL : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_PhieuXuatKho_TL(string MA_PYC)
        {
            InitializeComponent();
            try
            {
                QLBHDataContext aQLBHDataContext = new QLBHDataContext();

                var ads1 = (from pyc in aQLBHDataContext.PHIEU_YEU_CAUs
                            join nv in aQLBHDataContext.NHANVIENs on pyc.MA_NV equals nv.MA_NV
                            where pyc.MA_PYC.Equals(MA_PYC)
                            select new
                            {
                                pyc.MA_PYC,
                                nv.TEN_NV
                            });

                if (ads1.ToList().Count() > 0)
                {
                    lblMYC.Text = ads1.ToList()[0].MA_PYC;
                    lblNDN.Text = ads1.ToList()[0].TEN_NV;
                    lblNDN_KY.Text = ads1.ToList()[0].TEN_NV;
                }


                var ads2 = (from xk in aQLBHDataContext.XUATKHO_TBs
                            join nv in aQLBHDataContext.NHANVIENs on xk.NGUOI_XUAT_KHO equals nv.MA_NV
                            where xk.MA_PYC.Equals(MA_PYC)
                            select new
                            {
                                xk.MA_XK,
                                xk.NGAY_XK,
                                nv.TEN_NV
                            });


                if (ads2.ToList().Count() > 0)
                {
                    lblNgayXuatKho.Text = String.Format("{0:dd/MM/yyyy}", ads2.ToList()[0].NGAY_XK);
                    lblMPXK.Text = ads2.ToList()[0].MA_XK;
                    lblNXK.Text = ads2.ToList()[0].TEN_NV;
                }

                var Query = (from xk in aQLBHDataContext.XUATKHO_TBs
                             join yc in aQLBHDataContext.PHIEUYEUCAU_THIETBIs on xk.MA_PYC equals yc.MA_PYC
                             join tb in aQLBHDataContext.DM_THIET_BIs on xk.MA_TB equals tb.MA_TB
                             join cl in aQLBHDataContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                             join k in aQLBHDataContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                             join nv in aQLBHDataContext.NHANVIENs on yc.MA_NV equals nv.MA_NV
                             where yc.MA_PYC.Equals(MA_PYC)
                             select new
                             {
                                 tb.MA_TB,
                                 tb.TEN_TB,
                                 tb.SERIAL,
                                 cl.TEN_CL,
                                 k.TEN_KIEU,
                                 nv.TEN_NV
                             }).Distinct();
                this.DataSource = Query;
                colMA_TB.DataBindings.Add("Text", this.DataSource, "MA_TB");
                colTEN_TB.DataBindings.Add("Text", this.DataSource, "TEN_TB");
                colSERIAL.DataBindings.Add("Text", this.DataSource, "SERIAL");
                colTEN_CL.DataBindings.Add("Text", this.DataSource, "TEN_CL");
                colTEN_KIEU.DataBindings.Add("Text", this.DataSource, "TEN_KIEU");
                colNSD.DataBindings.Add("Text", this.DataSource, "TEN_NV");

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}
