﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Thống kê thiết bị</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
    </style> 
    <script type="text/javascript">
        
    </script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>

                              <ext:Button ID="Button2" runat="server" Text="In" Icon="Printer" Width="80">
                                 <Listeners>
                                     
                                 </Listeners>   
                             </ext:Button>  
                             <ext:ToolbarSeparator ID="ToolbarSpacer1" runat="server" />
                             <ext:Button ID="Button1" runat="server" Text="Xuất Excel" Icon="PageExcel" Width="80">
                                 <Listeners>
                                     
                                 </Listeners>   
                             </ext:Button>  

                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              
                              <ext:ComboBox 
                                                        ID="TRANG_THAI" 
                                                        runat="server"
                                                        DataIndex="TRANG_THAI"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local"  
                                                        FieldLabel="Điều kiện lọc"
                                                        EmptyText="Chọn điều kiện..."
                                                        Width="300"
                                                        EnableKeyEvents="true"
                                                        DisplayField="TEN_TT"     
                                                        ValueField="MA_TT" >
                                                        <Store>
                                                            <ext:Store 
                                                                ID="Store3" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsTrangthai" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_TT" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_TT" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_TT"/>                                                                                                    
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{TRANG_THAI}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                                                                   
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store> 
                                                       
                                                        <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>   
                                                            
                                                        <Listeners> 
                                                        <TriggerClick Handler="this.setValue('');" />
                                                        </Listeners>                                                                                                                          
                                         </ext:ComboBox>

                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Nhập số serial hoặc tên thiết bị" 
                                EnableKeyEvents="true" Width="300px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                             </ext:TriggerField>

                              <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["MA_TB"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{TimKiemThietBiTheoTrangThai}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>    
                <Store>
                    <ext:Store 
                        ID="TimKiemThietBiTheoTrangThai" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true"
                        GroupField="TEN_TT" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/TimKiemThietBiTheoTrangThai/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_TB" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_TB" />
                                    <ext:RecordField Name="TEN_TB" /> 
                                    <ext:RecordField Name="SERIAL" /> 
                                    <ext:RecordField Name="NGUYEN_GIA" /> 
                                    <ext:RecordField Name="MA_CL" />
                                    <ext:RecordField Name="TEN_CL" /> 
                                    <ext:RecordField Name="MA_KIEU" />    
                                    <ext:RecordField Name="TEN_KIEU" />   
                                    <ext:RecordField Name="TRANG_THAI" /> 
                                    <ext:RecordField Name="TEN_TT" />     
                                    <ext:RecordField Name="NGAY_BH" Type="Date" /> 
                                    <ext:RecordField Name="THOI_GIAN_BH" />   
                                    <ext:RecordField Name="NGAY_KH" Type="Date" /> 
                                    <ext:RecordField Name="THOI_GIAN_KH" />                                                                                                                                
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            <ext:Parameter Name="trangthai" Value="#{TRANG_THAI}.getValue()" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="220" Align="Center">
                        </ext:Column>
                        <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="200"  /> 
                        <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Số Serial" Width="100"  /> 
                        <ext:Column ColumnID="NGUYEN_GIA" DataIndex="NGUYEN_GIA" Header="Nguyên giá" Width="100"  />
                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="200" />
                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="200"/>
                        <ext:DateColumn ColumnID="NGAY_BH" DataIndex="NGAY_BH" 
                        Header="Ngày bắt đầu bảo hành" Width="150" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_BH" DataIndex="THOI_GIAN_BH" Header="Thời gian bảo hành còn lại" Width="150"/>
                        <ext:DateColumn ColumnID="NGAY_KH" DataIndex="NGAY_KH" 
                        Header="Ngày bắt đầu khấu hao" Width="150" Format="dd/MM/yyyy" Align="Center"/>
                        <ext:Column ColumnID="THOI_GIAN_KH" DataIndex="THOI_GIAN_KH" Header="Thời gian khấu hao còn lại" Width="150" /> 
                        <ext:Column ColumnID="TEN_TT" DataIndex="TEN_TT" Header="Trạng thái" Width="200"/>                                  
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>


                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="TEN_TB"/>
                            <ext:StringFilter DataIndex="SERIAL"/>     
                            <ext:StringFilter DataIndex="NGUYEN_GIA"/>
                            <ext:StringFilter DataIndex="TEN_CL"/>       
                            <ext:StringFilter DataIndex="TEN_KIEU"/>
                            <ext:StringFilter DataIndex="NGAY_BH"/>
                            <ext:StringFilter DataIndex="THOI_GIAN_BH"/>
                            <ext:StringFilter DataIndex="NGAY_KH"/>  
                            <ext:StringFilter DataIndex="THOI_GIAN_KH"/>                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>

                <View>
                       <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport> 
    </div>
</body>
</html>
