﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Danh mục chủng loại</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
    </style>
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };
        var failureHandler1 = function (form, action) {
            if (action.response.responseText == '<pre>{success:true}</pre>') {
                dsChungloai.reload();
                importForm.reset();
                wdImport.hide();
            }
            else {
                Ext.Msg.show({
                    title: "Thông báo",
                    msg: "Định dạng file không đúng",
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
        }
        var successHandler = function (form, action) {
            dsChungloai.reload();
            clForm.reset();
            wdChungLoai.hide();


            importForm.reset();
            wdImport.hide();

            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

     

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_CL');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            clForm.getForm().loadRecord(record);
            wdChungLoai.show();
            MA_CL.disable();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };

    </script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:GridPanel ID="GridPanel1" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true"
                    AutoExpandColumn="GHICHU">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">
                                    <Listeners>
                                        <Click Handler="#{clForm}.reset();#{wdChungLoai}.show();#{MA_CL}.enable();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>"
                                    Icon="BookEdit">
                                    <Listeners>
                                        <Click Fn="sua" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>"
                                    Icon="Delete">
                                    <DirectEvents>
                                        <Click Url="/Danhmuc/xoaChungloai" CleanRequest="true" Method="POST" Success="#{dsChungloai}.reload();">
                                            <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                            <ExtraParams>
                                                <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_CL')"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>

                                <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server" />
                              <ext:Button ID="Button8" runat="server" Text="Import File Excel" Icon="PageExcel">                                                   
                                  <Listeners>
                                    <Click Handler="#{importForm}.reset();#{wdImport}.show();" />
                                  </Listeners>                                                                         
                            </ext:Button> 


                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Tìm kiếm theo mã hoặc tên chủng loại..."
                                    EnableKeyEvents="true" Width="400px">
                                    <Listeners>
                                        <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["id"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsChungloai}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsChungloai" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsChungloai/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_CL" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="GHICHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_CL" DataIndex="MA_CL" Header="Mã chủng loại" Width="80" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Tên chủng loại" Width="200" />
                            <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="MA_CL" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="GHICHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView1" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                    </Listeners>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>

        <ext:Window ID="wdChungLoai" runat="server" Hidden="true" Title="Thông tin Chủng loại thiết bị"
            Icon="Information" Width="530" Height="210" Resizable="false">
            <Items>
                <ext:FormPanel ID="clForm" runat="server" Height="180" Width="520" Border="false"
                    Url="/Danhmuc/saveChungloai" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:Hidden ID="id" runat="server" />
                                <ext:TextField ID="MA_CL" IndicatorText="*" AllowBlank="false" DataIndex="MA_CL"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="45%" FieldLabel="Mã chủng loại" />
                                <ext:TextField ID="TEN_CL" IndicatorText="*" AllowBlank="false" DataIndex="TEN_CL"
                                    IndicatorCls="red" runat="server" AnchorHorizontal="90%" FieldLabel="Tên chủng loại" />
                                <ext:TextArea ID="GHICHU" DataIndex="GHICHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="90%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{clForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{clForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
            </Listeners>
        </ext:Window>

        <ext:Window ID="wdImport" runat="server" Hidden="true" Title="Import File Excel" Icon="PageExcel" Width="400" Height="110" Resizable="false">
            <Items>
                 <ext:FormPanel ID="importForm" runat="server" Height="80" Width="390" Border="false"
                 Url="/Danhmuc/save_ChungLoai_Ex" ButtonAlign="Center"  FileUpload="true" Enctype="multipart/form-data" >                
                    <Items> 
                      <ext:FieldSet ID="FieldSet4" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                               <Items>
                                    <ext:FileUploadField ID="TEN_FILE" DataIndex="TEN_FILE" Name="TEN_FILE" runat="server" Icon="Attach"  FieldLabel="File chủng loại"
                                    AnchorHorizontal="100%"/>
                               </Items>
                       </ext:FieldSet> 
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar3" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill3" runat="server" />
                                <ext:Button ID="Button2" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{importForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler1 });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer2" runat="server" />
                            </Items>
                        </ext:Toolbar>
           </BottomBar>
                </ext:FormPanel>                  
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
              
        </ext:Window>

    </div>
</body>
</html>
