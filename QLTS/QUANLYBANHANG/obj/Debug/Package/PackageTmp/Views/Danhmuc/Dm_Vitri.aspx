﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
    </style> 
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhanvien.reload();
            nvForm.reset();
            wdNv.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_VT');
            id.setValue(s);
            MA_VT.disable();
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            wdNv.show();
            setTimeout(TimKiemPhongBanTheoMaDonVi.reload(), 900);
            var s1 = GridPanel1.getSelectionModel().getSelected().get('MA_PB');
            setTimeout(function () { MA_PB.setValue(s1); }, 1000);
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };     
</script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="GHICHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();#{wdNv}.show();#{MA_DV}.setValue('');#{MA_PB}.setValue('');#{MA_VT}.enable();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Danhmuc/xoaVitri" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsNhanvien}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_VT')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   
                              
                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              
                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tìm kiếm nhanh" 
                                EnableKeyEvents="true" Width="400px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                             <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["id"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsNhanvien}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsNhanvien" 
                        runat="server" 
                        RemoteSort="true" GroupField="TEN_DV"
                        UseIdConfirmation="true" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsVitri/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_VT" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_VT" />
                                    <ext:RecordField Name="TEN_VT" /> 
                                    <ext:RecordField Name="SDT" /> 
                                    <ext:RecordField Name="GHICHU" /> 
                                    <ext:RecordField Name="MA_DV" />
                                    <ext:RecordField Name="TEN_DV" />
                                    <ext:RecordField Name="MA_PB" />
                                    <ext:RecordField Name="TEN_PB" />                                                                                                                                        
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_VT" DataIndex="MA_VT" Header="Mã vị trí" Width="150" /> 
                        <ext:Column ColumnID="TEN_VT" DataIndex="TEN_VT" Header="Tên vị trí" Width="200" /> 
                        <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Đơn vị" Width="200" /> 
                        <ext:Column ColumnID="TEN_PB" DataIndex="TEN_PB" Header="Phòng ban" Width="200" /> 
                        <ext:Column ColumnID="SDT" DataIndex="SDT" Header="Điện thoại" Width="80" Align="Center" />
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú" />                                       
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="TEN_VT" />   
                            <ext:StringFilter DataIndex="TEN_DV" />
                            <ext:StringFilter DataIndex="TEN_PB" />
                            <ext:StringFilter DataIndex="SDT" />  
                            <ext:StringFilter DataIndex="GHICHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                       <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
         
        <ext:Window ID="wdNv" runat="server" Hidden="true" Title="Thông tin vị trí lắp đặt" 
                Icon="Information" Width="530" Height="285" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="nvForm" runat="server" Height="255" Width="520"
                        Border="false" Url="/Danhmuc/saveVitri" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />


                                        <ext:NumberField 
                                         ID="MA_VT" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="MA_VT" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="55%"  EmptyText="Chỉ nhập số..."                       
                                         FieldLabel="Mã vị trí"/>


                                         <ext:TextField 
                                         ID="TEN_VT" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_VT" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên vị trí"/>

                                         <ext:ComboBox 
                                                                ID="MA_DV" 
                                                                runat="server"
                                                                DataIndex="MA_DV"
                                                                TriggerAction="All"                                                 
                                                                Mode="Local" ItemSelector="div.search-item"                  
                                                                IndicatorText="*" AllowBlank="false"  IndicatorCls="red"
                                                                EnableKeyEvents="true"
                                                                DisplayField="TEN_DV"     
                                                                ValueField="MA_DV" 
                                                                FieldLabel="Đơn vị"
                                                                AnchorHorizontal="90%" 
                                                                EmptyText="Chọn đơn vị..."
                                                                >
                                                            
                                                                <Store>
                                                                <ext:Store 
                                                                ID="dsDonvi" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsDonvi"/>
                                                                </Proxy>        
                                                                                                                     
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_DV" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_DV" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_DV"/>                                                                                                
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>  
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                                                                </BaseParams>                                                                                                             
                                                            </ext:Store>
                                                        </Store> 
                                                                <Template ID="Template4" runat="server">
                                                           <Html>
                                                <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_DV}</h3>                 
                                                </div>
                                                </tpl>
                                               </Html>
                                                        </Template>    
                                                                <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                <Listeners> 
                                                                     <TriggerClick Handler="this.setValue('');#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');" />
                                                                     <Select  Handler="#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');"/>
                                                                </Listeners>                                                                                                                          
                                                                </ext:ComboBox>

                                         <ext:ComboBox 
                                                                ID="MA_PB" 
                                                                runat="server"
                                                                DataIndex="MA_PB"
                                                                TriggerAction="All"                                                 
                                                                Mode="Local" ItemSelector="div.search-item"                  
                                                                IndicatorText="*" AllowBlank="false"  IndicatorCls="red"
                                                                EnableKeyEvents="true"
                                                                DisplayField="TEN_PB"     
                                                                ValueField="MA_PB"
                                                                FieldLabel="Phòng ban" 
                                                                AnchorHorizontal="90%" 
                                                                EmptyText="Chọn phòng ban..."
                                                                >
                                                            
                                                                <Store>
                                                                <ext:Store 
                                                                ID="TimKiemPhongBanTheoMaDonVi" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/TimKiemPhongBanTheoMaDonVi"/>
                                                                </Proxy>        
                                                                                                                     
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_PB" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_PB" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_PB"/>                                                                                                
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>   
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                                                                </BaseParams>                                                                                                             
                                                            </ext:Store>
                                                        </Store> 
                                                                <Template ID="Template1" runat="server">
                                                           <Html>
                                                <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_PB}</h3>                 
                                                </div>
                                                </tpl>
                                               </Html>
                                                        </Template>    
                                                                <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                                <Listeners> 
                                                                     <TriggerClick Handler="this.setValue('');" />
                                                                </Listeners>                                                                                                                          
                                                                </ext:ComboBox>

                                         <ext:TextField 
                                         ID="SDT"   
                                         DataIndex="SDT"  
                                         runat="server" AnchorHorizontal="45%"                          
                                         FieldLabel="Điện thoại"/>

                                         <ext:TextArea 
                                         ID="GHICHU" 
                                         DataIndex="GHICHU"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="90%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{nvForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{nvForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>
    </div>
</body>
</html>
