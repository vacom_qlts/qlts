﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục phòng/đội</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
    </style> 
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsPhongBan.reload();
            pbForm.reset();
            wdPhongBan.hide();
            MA_PB.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {

            var s = GridPanel1.getSelectionModel().getSelected().get('MA_PB');
            MA_PB.setValue(s);

            var record = GridPanel1.getSelectionModel().getSelected();
            pbForm.getForm().loadRecord(record);
            wdPhongBan.show();
            KY_HIEU.setValue(s);
            KY_HIEU.disable();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };     
</script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="GHI_CHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{pbForm}.reset();#{wdPhongBan}.show();#{KY_HIEU}.enable();" />
                                </Listeners>                                                                             
                            </ext:Button>                                                
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Danhmuc/xoaPhongBan" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsPhongBan}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="MA_PB" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_PB')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   
                              
                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              
                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tìm kiếm theo mã hoặc tên phòng/đội..." 
                                EnableKeyEvents="true" Width="400px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                             <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["id"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsPhongBan}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>                                                
                <Store>
                    <ext:Store 
                        ID="dsPhongBan" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true" GroupField="TEN_DV" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsPhongBan/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_PB" Root="data" TotalProperty="total">
                                <Fields>
                                   <ext:RecordField Name="MA_PB" />
                                    <ext:RecordField Name="TEN_PB" /> 
                                    <ext:RecordField Name="MO_TA" />
                                    <ext:RecordField Name="MA_DV" /> 
                                    <ext:RecordField Name="TEN_DV" /> 
                                    <ext:RecordField Name="GHI_CHU" />                                                                                                                                         
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_PB" DataIndex="MA_PB" Header="Mã phòng/đội" Width="180" /> 
                        <ext:Column ColumnID="TEN_PB" DataIndex="TEN_PB" Header="Tên phòng/đội" Width="180" /> 
                        <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Tên đơn vị" Width="180" /> 
                        <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" Width="120" /> 
                        <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" />                                       
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters> 
                            <ext:StringFilter DataIndex="TEN_PB" />                           
                            <ext:StringFilter DataIndex="MO_TA" />
                            <ext:StringFilter DataIndex="GHI_CHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <View>
                       <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true" />
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
         
        <ext:Window ID="wdPhongBan" runat="server" Hidden="true" Title="Thông tin phòng/đội" 
                Icon="Information" Width="530" Height="300" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="pbForm" runat="server" Height="270" Width="520"
                        Border="false" Url="/Danhmuc/savePhongBan" ButtonAlign="Center">                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        
                                        <ext:Hidden ID="MA_PB" runat="server" />

                                        <ext:ComboBox 
                                                        ID="MA_DV" 
                                                        runat="server"
                                                        DataIndex="MA_DV"
                                                        TriggerAction="All"                                                        
                                                        Mode="Local" FieldLabel="Đơn vị"                                            
                                                        AnchorHorizontal="90%" EnableKeyEvents="true" EmptyText="Chọn đơn vị..."
                                                        DisplayField="TEN_DV" ItemSelector="div.search-item"    
                                                        PageSize="10"  IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                                        ValueField="MA_DV" >
                                                        <Store>
                                                            <ext:Store 
                                                                ID="Store8" 
                                                                runat="server" 
                                                                RemoteSort="true" 
                                                                UseIdConfirmation="true">
                                                                <Proxy>
                                                                    <ext:HttpProxy Url="/Danhmuc/dsDonvi" />
                                                                </Proxy>                                                            
                                                                <Reader>
                                                                    <ext:JsonReader IDProperty="MA_DV" Root="data" TotalProperty="total">
                                                                        <Fields>
                                                                            <ext:RecordField Name="MA_DV" SortDir="ASC"/>  
                                                                            <ext:RecordField Name="TEN_DV"/>                                                                                                    
                                                                        </Fields>
                                                                    </ext:JsonReader>
                                                                </Reader>
                                                                <BaseParams>
                                                                    <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                                    <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />                                                                   
                                                                </BaseParams>                                                                                                                        
                                                            </ext:Store>
                                                        </Store> 
                                                        <Template ID="Template1" runat="server">
                                                           <Html>
					                                           <tpl for=".">
						                                          <div class="search-item">
							                                         <h3><span>{MA_DV}</span>{TEN_DV}</h3>	                
						                                          </div>
					                                           </tpl>
				                                           </Html>
                                                        </Template>    
                                                        <Triggers>
                                                            <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                        </Triggers>       
                                                        <Listeners>
                                                            <TriggerClick Handler="this.setValue('');" />
                                                        </Listeners>                                                                                                                                    
                                                      </ext:ComboBox>
                                        <ext:TextField
                                          
                                         ID="KY_HIEU" IndicatorText="*" AllowBlank="false" IndicatorCls="red"   
                                         DataIndex="KY_HIEU"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Mã phòng/đội"/>
                                        <ext:TextField 
                                         ID="TEN_PB" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_PB" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="90%"                          
                                         FieldLabel="Tên phòng/đội"/>

                                         <ext:TextArea 
                                         ID="MO_TA" 
                                         DataIndex="MO_TA"
                                         runat="server"        
                                         FieldLabel="Mô tả"
                                         AnchorHorizontal="90%"/>   

                                         <ext:TextArea 
                                         ID="GHI_CHU" 
                                         DataIndex="GHI_CHU"
                                         runat="server"        
                                         FieldLabel="Ghi chú"
                                         AnchorHorizontal="90%"/>                                                   
                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{pbForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{pbForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>
    </div>
</body>
</html>
