﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Nhập kho thu hồi</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
        
        
        .x-form-group .x-form-group-header-text
        {
            background-color: #dfe8f6;
        }
        
        .x-label-text
        {
            font-weight: bold;
            font-size: 11px;
        }
        
        .orange
        {
            background-color: Orange;
            color: Black;
        }
        
        .green
        {
            background-color: Green;
            color: Black;
        }
        
        .yellow
        {
            background-color: Yellow;
            color: Black;
        }
        .silver
        {
            background-color: Silver;
            color: Black;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            AnHienChucNang(0);
        });

        function AnHienChucNang(tt) {
            if (tt == 4) {
                btnTh.enable();
                btnInPhieu.disable();
            }
            else if (tt == 9) {
                btnInPhieu.enable();
                btnTh.disable();
            }
            else {
                btnTh.disable();
                btnInPhieu.disable();
            }
        }

        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };
        var successHandler = function (form, action) {
            dsYeuCauTongQuat.reload();
            nltbForm.reset();
            wdNhanLaiThietBi.hide();
        };




        function getRowClass(record) {
            if (record.data.TRANG_THAI === 2) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 3) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 4) {
                return 'orange';
            }

            if (record.data.TRANG_THAI === 5) {
                return 'yellow';
            }

        };

        var thuhoi = function () {
            nltbForm.reset();
            wdNhanLaiThietBi.show();
        };

        function InPhieu() {
            var myc = GridPanel1.getSelectionModel().getSelected().get('MA_PYC');
            window.location.href = "/Report/HienBaoCao.aspx?bc=nhakho_th&ts=" + myc;
        }


    </script>
    <script runat="server">
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:Hidden ID="ID_PYC" runat="server" />
                <ext:GridPanel ID="GridPanel1" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="btnTh" Disabled="true" runat="server" Text="Thu hồi" Icon="Accept">
                                    <Listeners>
                                        <Click Fn="thuhoi" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnInPhieu" Disabled="true" runat="server" Text="In phiếu" Icon="PrinterAdd">
                                    <Listeners>
                                        <Click Handler="InPhieu()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Nhập mã yêu cầu hoặc tên người đề nghị..."
                                    EnableKeyEvents="true" Width="300px">
                                    <Listeners>
                                        <KeyDown Buffer="300" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                        <TriggerClick Handler="#{dsYeuCauTongQuat}.reload();" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsYeuCauTongQuat}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsYeuCauTongQuat" runat="server" RemoteSort="true" UseIdConfirmation="true"
                            GroupField="TRANG_THAI_DP">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauTongQuat/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_PYC" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="LY_DO" />
                                        <ext:RecordField Name="NGAY_DE_NGHI" Type="Date" />
                                        <ext:RecordField Name="NGAY_CAP" Type="Date" />
                                        <ext:RecordField Name="LOAI" />
                                        <ext:RecordField Name="LOAI_DP" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                        <ext:RecordField Name="TRANG_THAI" />
                                        <ext:RecordField Name="TRANG_THAI_DP" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="loai" Value="3" Mode="Raw" />
                                <ext:Parameter Name="trangthai" Value="'4,9'" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_PYC" DataIndex="MA_PYC" Header="Mã số phiếu" Width="150" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người đề nghị" Width="150">
                            </ext:Column>
                            <ext:Column ColumnID="LY_DO" DataIndex="LY_DO" Header="Lý do yêu cầu" Width="200" />
                            <ext:DateColumn ColumnID="NGAY_DE_NGHI" DataIndex="NGAY_DE_NGHI" Header="Ngày đề nghị"
                                Width="100" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="LOAI_DP" DataIndex="LOAI_DP" Header="Loại" Width="200" />
                            <ext:Column ColumnID="TRANG_THAI_DP" DataIndex="TRANG_THAI_DP" Header="Trạng thái"
                                Width="200" />
                            <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" Width="200" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="
                             AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'));
                             setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                             " />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="LY_DO" />
                                <ext:StringFilter DataIndex="NGAY_DE_NGHI" />
                                <ext:StringFilter DataIndex="MO_TA" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                            <GetRowClass Fn="getRowClass" />
                        </ext:GroupingView>
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="
                              AnHienChucNang(#{GridPanel1}.getSelectionModel().getSelected().get('TRANG_THAI'));
                              setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));" />
                    </Listeners>
                </ext:GridPanel>
                <ext:GridPanel ID="gpYeuCauChiTiet" RowHeight="0.6" Title="Chi tiết yêu cầu" runat="server"
                    Border="false" TrackMouseOver="true">
                    <Store>
                        <ext:Store ID="dsYeuCauChiTiet" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauChiTiet/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="ID" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="SO_LUONG" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150" />
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="150" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="150" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="150" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                            <ext:Column ColumnID="SO_LUONG" DataIndex="SO_LUONG" Header="Số lượng" Width="80"
                                Align="Right" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                            <Listeners>
                                <RowSelect Handler="" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                <ext:StringFilter DataIndex="SO_LUONG" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdNhanLaiThietBi" runat="server" Hidden="true" Title="Ghi chú" Icon="Information"
            Width="530" Height="200" Resizable="false">
            <Items>
                <ext:FormPanel ID="nltbForm" runat="server" Height="170" Width="520" Border="false"
                    Url="/Danhmuc/KhoNhanLaiThietBi" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:TextArea ID="TD_GHI_CHU" DataIndex="TD_GHI_CHU" runat="server" FieldLabel="Ghi chú"
                                    Height="110" AnchorHorizontal="95%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{nltbForm}.form.submit({waitMsg : 'Đang ghi...',params:{MA_PYC:#{ID_PYC}.getValue(),json:Ext.encode(#{gpYeuCauChiTiet}.getRowsValues())},success : successHandler, failure : failureHandler});" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{nltbForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
        </ext:Window>
    </div>
</body>
</html>
