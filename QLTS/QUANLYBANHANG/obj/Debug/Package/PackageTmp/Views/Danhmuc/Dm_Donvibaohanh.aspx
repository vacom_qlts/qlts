﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục đơn vị bảo hành</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />   
    <style type="text/css">
        .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
    </style> 
    <script type="text/javascript">
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsDonViBaoHanh.reload();
            dmdvbhForm.reset();
            wdDMDVBH.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_DVBH');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            dmdvbhForm.getForm().loadRecord(record);
            wdDMDVBH.show();
            MA_DVBH.disable();
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };     
</script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items>                               
                <ext:GridPanel 
                ID="GridPanel1" RowHeight="0.6" 
                runat="server" Border="false"              
                TrackMouseOver="true" AutoExpandColumn="GHI_CHU">    
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>

                            <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">                                                   
                                <Listeners>
                                    <Click Handler="#{dmdvbhForm}.reset();#{wdDMDVBH}.show();#{MA_DVBH}.enable();" />
                                </Listeners>                                                                             
                            </ext:Button> 
                                         
                            <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>" Icon="BookEdit">
                                <Listeners>
                                    <Click Fn="sua" />
                                </Listeners>                     
                            </ext:Button>                                                
                            <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>" Icon="Delete">
                                 <DirectEvents>
                                     <Click 
                                        Url="/Danhmuc/xoaDonViBaoHanh" 
                                        CleanRequest="true"
                                        Method="POST" Success="#{dsDonViBaoHanh}.reload();">
                                        <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                        <ExtraParams>
                                            <ext:Parameter Name="MA_DVBH" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_DVBH')" Mode="Raw" />
                                        </ExtraParams>
                                     </Click>
                                  </DirectEvents>
                              </ext:Button>   
                              
                              <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                              
                              <ext:TriggerField 
                                ID="txtLichLV" 
                                runat="server" 
                                EmptyText="Tìm kiếm theo mã hoặc tên đơn vị..." 
                                EnableKeyEvents="true" Width="400px" >
                                <Listeners>
                                    <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                    <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                </Listeners>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                    <ext:FieldTrigger Icon="Search" />
                                </Triggers>
                            </ext:TriggerField>
                             <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["MA_DVBH"] %>' AutoDataBind="true">
                                <Listeners>
                                    <Change Handler="#{dsDonViBaoHanh}.reload();" Delay="100" />
                                </Listeners>
                            </ext:Hidden>                    
                        </Items>
                    </ext:Toolbar>
                </TopBar>    
                <Store>
                    <ext:Store
                        ID="dsDonViBaoHanh" 
                        runat="server" 
                        RemoteSort="true" >
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsDonViBaoHanh/"  />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_DVBH" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_DVBH" />
                                    <ext:RecordField Name="TEN_DVBH" /> 
                                    <ext:RecordField Name="DIA_CHI" /> 
                                    <ext:RecordField Name="SO_DIEN_THOAI" /> 
                                    <ext:RecordField Name="EMAIL" />        
                                    <ext:RecordField Name="GHI_CHU" />                                                                                                                               
                                </Fields>
                            </ext:JsonReader>                            
                        </Reader>
                        <BaseParams>
                            <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                            <ext:Parameter Name="start" Value="0" Mode="Raw" />                            
                        </BaseParams>                                                  
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="MA_DVBH" DataIndex="MA_DVBH" Header="Mã vị bảo hành" Width="100"/>
                        <ext:Column ColumnID="TEN_DVBH" DataIndex="TEN_DVBH" Header="Đơn vị bảo hành" Width="200"/>
                        <ext:Column ColumnID="DIA_CHI" DataIndex="DIA_CHI" Header="Địa chỉ"  Width="200"/> 
                        <ext:Column ColumnID="SO_DIEN_THOAI" DataIndex="SO_DIEN_THOAI" Header="Điện thoại" Width="100" /> 
                        <ext:Column ColumnID="EMAIL" DataIndex="EMAIL" Header="Email" Width="150" />
                        <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="150"/>                                    
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >
                        <Listeners>                             
                             <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>


                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>                            
                            <ext:StringFilter DataIndex="TEN_DVBH" />
                            <ext:StringFilter DataIndex="DIA_CHI" />     
                            <ext:StringFilter DataIndex="SO_DIEN_THOAI" />       
                            <ext:StringFilter DataIndex="EMAIL" />
                            <ext:StringFilter DataIndex="GHI_CHU" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>

                <View>
                       <ext:BufferView ID="BufferView1" runat="server" ScrollDelay="0" />
                </View>
                                            
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <Listeners>                               
                    <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                </Listeners>              
                </ext:GridPanel>
            </Items>            
        </ext:Viewport>   
         
        <ext:Window ID="wdDMDVBH" runat="server" Hidden="true" Title="Thông tin đơn vị bảo hành" 
                Icon="Information" Width="530" Height="265" Resizable="false">
                    <Items>
                        <ext:FormPanel ID="dmdvbhForm" runat="server" Height="235" Width="520"
                        Border="false" Url="/Danhmuc/saveDonViBaoHanh" ButtonAlign="Center" >                
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield" >
                                    <Items>
                                        <ext:Hidden ID="id" runat="server" />
                                       
                                       <ext:TextField 
                                         ID="MA_DVBH" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="MA_DVBH" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Mã đơn vị bảo hành"/>

                                        <ext:TextField 
                                         ID="TEN_DVBH" IndicatorText="*" AllowBlank="false"   
                                         DataIndex="TEN_DVBH" IndicatorCls="red"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Tên đơn vị bảo hành"/>

                                         <ext:TextField 
                                         ID="DIA_CHI"
                                         DataIndex="DIA_CHI"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Địa chỉ"/>

                                         <ext:TextField 
                                         ID="SO_DIEN_THOAI"
                                         DataIndex="SO_DIEN_THOAI"   
                                         runat="server" AnchorHorizontal="60%"                          
                                         FieldLabel="Số diện thoại"/>

                                         <ext:TextField 
                                         ID="EMAIL"
                                         DataIndex="EMAIL"   
                                         runat="server" AnchorHorizontal="60%"                          
                                         FieldLabel="Email"/>

                                         <ext:TextField 
                                         ID="GHI_CHU"
                                         DataIndex="GHI_CHU"   
                                         runat="server" AnchorHorizontal="95%"                          
                                         FieldLabel="Ghi chú"/>

                           </Items>
                    </ext:FieldSet>                    
                </Items>
                <BottomBar>
                    <ext:Toolbar ID="Toolbar2" runat="server">  
                        <Items>     
                            <ext:ToolbarFill ID="ToolbarFill2" runat="server" />                                                                         
                            <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                <Listeners>
                                    <Click Handler="#{dmdvbhForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });"/>
                                </Listeners>
                            </ext:Button>                                 
                            <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                <Listeners>
                                    <Click Handler="#{dmdvbhForm}.reset();" />
                                </Listeners>
                            </ext:Button>
                            <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                        </Items>                 
                    </ext:Toolbar>
                </BottomBar>                                
            </ext:FormPanel>
                    </Items>
                    <Listeners>
                        <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
                    </Listeners>
                </ext:Window>
    </div>
</body>
</html>
