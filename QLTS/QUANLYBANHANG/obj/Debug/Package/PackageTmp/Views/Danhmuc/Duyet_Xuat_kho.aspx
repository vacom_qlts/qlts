﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Danh mục khu vực</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />    
    <script type="text/javascript">

        var commandHandler = function (cmd, record) {
            switch (cmd) {
                case "duyet":
                    Ext.Msg.confirm('Thông báo', 'Bạn chắc chắn muốn duyệt xuất kho?!', function (btn) {
                        if (btn == "yes") {
                            Ext.Ajax.request({
                                url: '/Danhmuc/duyetXK',
                                params: { id: record.id },
                                method: 'POST',
                                success: function (response, request) {
                                    dsPYC.reload();
                                    Store2.reload();
                                },
                                failure: function (result, request) {
                                    alert(response.responseText);
                                }
                            });
                        }
                    });
                    break;
                case "huy":
                    Ext.Msg.confirm('Thông báo', 'Bạn chắc chắn muốn hủy xuất kho?!', function (btn) {
                        if (btn == "yes") {
                            Ext.Ajax.request({
                                url: '/Danhmuc/huyXK',
                                params: { id: record.id },
                                method: 'POST',
                                success: function (response, request) {
                                    dsPYC.reload();
                                    Store2.reload();
                                },
                                failure: function (result, request) {
                                    alert(response.responseText);
                                }
                            });
                        }
                    });
                    break;
            }
        };

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        };
            
</script>
<script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
</script>
</head>
<body>
    <div>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />   
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">            
            <Items> 
                <ext:GridPanel 
                ID="GridPanel1" 
                runat="server" Border="false" Icon="BookOpen"             
                TrackMouseOver="true" Title="Danh sách công việc chờ duyệt"
                AutoExpandColumn="GHICHU" RowHeight="0.4" >                                                      
                <Store>
                    <ext:Store 
                        ID="dsPYC" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsXK_Choduyet/" />
                        </Proxy>                        
                        <Reader>
                            <ext:JsonReader IDProperty="MA_XK" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_XK" />
                                    <ext:RecordField Name="SO_PHIEU" /> 
                                    <ext:RecordField Name="NGAY_XK" Type="Date" />
                                    <ext:RecordField Name="MA_DV" />
                                    <ext:RecordField Name="TEN_DV" />
                                    <ext:RecordField Name="MA_NSD" />
                                    <ext:RecordField Name="TEN_NSD" />
                                    <ext:RecordField Name="GHICHU" />
                                    <ext:RecordField Name="TRANGTHAI" />
                                    <ext:RecordField Name="MA_TB" />
                                    <ext:RecordField Name="TEN_TB" />  
                                    <ext:RecordField Name="TEN_CL" />  
                                    <ext:RecordField Name="TEN_KIEU" />                                                                                                                                   
                                </Fields>
                            </ext:JsonReader>                   
                        </Reader>
                        <SortInfo Field="NGAY_XK" Direction="DESC" />                                            
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="SO_PHIEU" DataIndex="SO_PHIEU" Header="Số phiếu" Width="80" />
                        <ext:DateColumn ColumnID="NGAY_XK" DataIndex="NGAY_XK" Header="Ngày xuất kho" Width="100" />
                        <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Thiết bị" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Phòng ban" Width="120" /> 
                        <ext:Column ColumnID="TEN_NSD" DataIndex="TEN_NSD" Header="Người sử dụng" Width="120" /> 
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú">
                            <Renderer Fn="myRenderer" />
                        </ext:Column>   
                        <ext:CommandColumn Hideable="false" Width="60">
                            <Commands>
                                <ext:GridCommand CommandName="duyet" Icon="Tick">                                                                    
                                      <ToolTip Text="Duyệt" />
                                </ext:GridCommand>
                                <ext:CommandSeparator />
                                <ext:GridCommand CommandName="huy" Icon="Delete">                                                                    
                                      <ToolTip Text="Hủy" />
                                </ext:GridCommand>
                            </Commands>                                                      
                       </ext:CommandColumn>           
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" >                        
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                        <Filters>
                            <ext:DateFilter DataIndex="TU_NGAY" />
                            <ext:DateFilter DataIndex="DEN_NGAY" />
                            <ext:StringFilter DataIndex="TEN_NV" />
                            <ext:StringFilter DataIndex="GHICHU" />
                            <ext:StringFilter DataIndex="NOIDUNG" />                            
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="3" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" />  
                <View>
                    <ext:BufferView ID="GroupingView2" RowHeight="50" runat="server" />
                </View>
                <Listeners>
                      <Command Fn="commandHandler" />                      
                </Listeners>               
                </ext:GridPanel>    
                
                <ext:GridPanel 
                ID="GridPanel2" Icon="BookOpenMark"    
                runat="server" Border="false"              
                TrackMouseOver="true" Title="Danh sách Xuất kho đã duyệt" 
                AutoExpandColumn="GHICHU" RowHeight="0.6">  
                <Store>
                    <ext:Store 
                        ID="Store2" 
                        runat="server" 
                        RemoteSort="true" 
                        UseIdConfirmation="true">
                        <Proxy>
                            <ext:HttpProxy Url="/Danhmuc/dsXK_Daduyet/" />
                        </Proxy>                        
                        <Reader>
                             <ext:JsonReader IDProperty="MA_XK" Root="data" TotalProperty="total">
                                <Fields>
                                    <ext:RecordField Name="MA_XK" />
                                    <ext:RecordField Name="SO_PHIEU" /> 
                                    <ext:RecordField Name="NGAY_XK" Type="Date" />
                                    <ext:RecordField Name="MA_DV" />
                                    <ext:RecordField Name="TEN_DV" />
                                    <ext:RecordField Name="MA_NSD" />
                                    <ext:RecordField Name="TEN_NV" />
                                    <ext:RecordField Name="NGAY_DUYET" Type="Date" />
                                    <ext:RecordField Name="TEN_NSD" />
                                    <ext:RecordField Name="GHICHU" />
                                    <ext:RecordField Name="TRANGTHAI" />
                                    <ext:RecordField Name="MA_TB" />
                                    <ext:RecordField Name="TEN_TB" />  
                                    <ext:RecordField Name="TEN_CL" />  
                                    <ext:RecordField Name="TEN_KIEU" />                                                                                                                                   
                                </Fields>
                            </ext:JsonReader> 
                        </Reader>
                                                                    
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn />
                        <ext:Column ColumnID="SO_PHIEU" DataIndex="SO_PHIEU" Header="Số phiếu" Width="80" />
                        <ext:DateColumn ColumnID="NGAY_XK" DataIndex="NGAY_XK" Header="Ngày xuất kho" Width="100" />
                        <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Thiết bị" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="120" >
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Phòng ban" Width="120" /> 
                        <ext:Column ColumnID="TEN_NSD" DataIndex="TEN_NSD" Header="Người sử dụng" Width="120" /> 
                        <ext:DateColumn ColumnID="NGAY_DUYET" DataIndex="NGAY_DUYET" Header="Ngày duyệt" Width="80" />
                        <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người duyệt" Width="120" /> 
                        <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú">
                            <Renderer Fn="myRenderer" />
                        </ext:Column> 
                        <ext:Column ColumnID="TRANGTHAI" DataIndex="TRANGTHAI" Header="Trạng thái"/>                      
                    </Columns>
                </ColumnModel>
                            
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" >                        
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Plugins>
                    <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                        <Filters>
                            <ext:DateFilter DataIndex="TU_NGAY" />
                            <ext:DateFilter DataIndex="DEN_NGAY" />
                            <ext:StringFilter DataIndex="TEN_NV" />
                            <ext:StringFilter DataIndex="GHICHU" />
                            <ext:StringFilter DataIndex="NOIDUNG" />                           
                        </Filters>
                    </ext:GridFilters>
                </Plugins>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="5" />
                </BottomBar>                              
                <LoadMask ShowMask="true" />
                <SaveMask ShowMask="true" /> 
                <View>
                    <ext:BufferView ID="BufferView1" RowHeight="50" runat="server" />
                </View>
                </ext:GridPanel>     
            </Items>
        </ext:Viewport>   
        
         
                 
    </div>
</body>
</html>
