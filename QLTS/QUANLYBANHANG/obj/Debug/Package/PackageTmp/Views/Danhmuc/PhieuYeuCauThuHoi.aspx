﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Phiếu yêu cầu thu hồi thiết bị</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
        
        
        .x-form-group .x-form-group-header-text
        {
            background-color: #dfe8f6;
        }
        
        .x-label-text
        {
            font-weight: bold;
            font-size: 11px;
        }
        
        .orange
        {
            background-color:Orange;
            color: Black;
        }
        
        .green
        {
            background-color: Green;
            color: Black;
        }
        
        .yellow
        {
            background-color: Yellow;
            color: Black;
        }
        .silver
        {
            background-color: Silver;
            color: Black;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#LY_DO').hide();
        });

        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        }

        var successHandler = function (form, action) {
            dsYeuCauTongQuat.reload();
            dsYeuCauChiTiet.reload();
            ycstbForm.reset();
            wdYCTBM.hide();
            ID.setValue('');
            ID_LD.setValue('');
            lydo();
            btnSua.disable();
            btnXoa.disable();
            resetTTC();
            resetYC();

        }

        var sua = function () {
            var myc = GridPanel1.getSelectionModel().getSelected().get('MA_PYC');
            ID.setValue(myc);
            MA_PYC.setValue(myc);
            MA_PYC.disable();

            var mnv = GridPanel1.getSelectionModel().getSelected().get('MA_NV');
            MA_NV.setValue(mnv);

            var mnv = GridPanel1.getSelectionModel().getSelected().get('MA_NV');
            MA_NV.setValue(mnv);

            var mt = GridPanel1.getSelectionModel().getSelected().get('MO_TA');
            GHI_CHU_PYC.setValue(mt);

            var ndn = GridPanel1.getSelectionModel().getSelected().get('NGAY_DE_NGHI');
            NGAY_DE_NGHI.setValue(ndn);

            var ld = GridPanel1.getSelectionModel().getSelected().get('LY_DO');
            trangthai_lydo(ld);

            wdYCTBM.show();



            TimKiemTheoMaPhieuYeuCau.reload();

            SERIAL.setValue('');
            MA_CL.setValue('');
            MA_NSD.setValue('');
            MA_KIEU.setValue('');
            TEN_CL.setValue('');
            TEN_KIEU.setValue('');
            GHI_CHU.setValue('');
        }

        function trangthai_lydo(ld) {
            var tt = 6;
            if (ld == 'Lý do 1') {
                tt = 1;
            }
            else if (ld == 'Lý do 2') {
                tt = 2;
            }
            else if (ld == 'Lý do 3') {
                tt = 3;
            }
            else if (ld == 'Lý do 4') {
                tt = 4;
            }
            else if (ld == 'Lý do 5') {
                tt = 5;
            }
            else {
                tt = 6;
            }

            setTimeout(function () {
                ID_LD.setValue(tt);
                LY_DO.setValue(ld);
                if (tt == 6) {
                    $('#LY_DO').show();
                }
                else {
                    $('#LY_DO').hide();
                }
            }, 500);

        }

        var myRenderer = function (value) {
            if (value == '' || value == null)
                return "";
            else
                return "<div style='overflow-x:auto; overflow-y:auto;white-space:pre-wrap;vertical-align:middle; '>" + value + "</div>";
        }

        function luulai() {
            if (MA_PYC.getValue() == '') {
                alert("Vui lòng nhập mã phiếu yêu cầu.");
                MA_PYC.focus();
            }
            else if (MA_NV.getValue() == '') {
                alert("Vui lòng chọn người đề nghị.");
                MA_NV.focus();
            }
            else if (NGAY_DE_NGHI.getValue() == '') {
                alert("Nhập ngày đề nghị.");
                NGAY_DE_NGHI.focus();
            }
            else if (ID_LD.getValue() == '') {
                alert("Vui lòng chọn lý do.");
                ID_LD.focus();
            }
            else if (gpDSYCTB.getRowsValues() == '') {
                alert("Vui lòng chọn thiết bị đề nghị sửa.");
            }
            else {
                ycstbForm.form.submit({ waitMsg: 'Đang ghi...', success: successHandler, failure: failureHandler });
            }

        }

        function updateRecord() {

            if (id_update.getValue() == '') {
                alert("Vui lòng chọn bản ghi cần sửa.");
            }
            else if (SERIAL.getValue() == '') {
                alert("Vui lòng chọn thiết bị.");
                SERIAL.focus();
            }
            else if (MA_NSD.getValue() == '') {
                alert("Vui lòng chọn người sử dụng.");
                SERIAL.focus();
            }
            else {
                gpDSYCTB.deleteSelected();
                gpDSYCTB.insertRecord(0, { MA_CL: MA_CL.getValue(), TEN_CL: TEN_CL.getValue(), MA_KIEU: MA_KIEU.getValue(),
                    TEN_KIEU: TEN_KIEU.getValue(), GHI_CHU: GHI_CHU.getValue(), SERIAL: SERIAL.getValue(), MA_NV: MA_NSD.getValue(), TEN_NV: MA_NSD.getText()
                });
                setTimeout(resetYC(), 500);
            }

        }

        function addRecord() {
            if (SERIAL.getValue() == '') {
                alert("Vui lòng chọn thiết bị.");
                SERIAL.focus();
            }
            else if (MA_NSD.getValue() == '') {
                alert("Vui lòng chọn người sử dụng.");
                SERIAL.focus();
            }
            else {
                gpDSYCTB.insertRecord(0, { MA_CL: MA_CL.getValue(), TEN_CL: TEN_CL.getValue(), MA_KIEU: MA_KIEU.getValue(),
                    TEN_KIEU: TEN_KIEU.getValue(), GHI_CHU: GHI_CHU.getValue(), SERIAL: SERIAL.getValue(), MA_NV: MA_NSD.getValue(), TEN_NV: MA_NSD.getText()
                });
                setTimeout(resetYC(), 500);
            }
        }

        function resetTTC() {
            MA_NV.setValue('');
            MA_PYC.setValue('');
            var now = new Date();
            NGAY_DE_NGHI.setValue(now);

            LY_DO.setValue('');
            GHI_CHU_PYC.setValue('');
            ID_LD.setValue('');
            lydo();
            TimKiemTheoMaPhieuYeuCau.removeAll();
        }

        function resetYC() {
            SERIAL.setValue('');
            MA_CL.setValue('');
            MA_NSD.setValue('');
            MA_KIEU.setValue('');
            TEN_CL.setValue('');
            TEN_KIEU.setValue('');
            GHI_CHU.setValue('');
            id_update.setValue('');
            dsTatCaThietBi.reload();
        }

        function setValueForText() {

            $.post("/Danhmuc/TimKiemThietBiTheoSerial", { serial: SERIAL.getValue() }, function (data) {
                $.each(data, function (i, r) {
                    MA_CL.setValue(r.MA_CL);
                    MA_KIEU.setValue(r.MA_KIEU);
                    TEN_CL.setValue(r.TEN_CL);
                    TEN_KIEU.setValue(r.TEN_KIEU);
                });
            });

        }

        function getComboDisplay(combo) {
            var rl = '';
            combo.getStore().each(function (r) {
                if (r.data[combo.valueField].toString().length > 0) {
                    rl = r.data[combo.valueField];
                }

            });
            return rl;
        }

        function lydo() {
            if (ID_LD.getValue() == 6) {
                $('#LY_DO').show();
            }
            else {
                $('#LY_DO').hide();
            }

        }

        function getRowClass(record) {
            if (record.data.TRANG_THAI === 2) {
                return 'green';
            }

            if (record.data.TRANG_THAI === 3) {
                return 'silver';
            }


            if (record.data.TRANG_THAI === 4) {
                return 'orange';
            }

            if (record.data.TRANG_THAI === 5) {
                return 'yellow';
            }

        };

        function kiemtra() {
            if (GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') >= 1 && GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') <= 3) {
                btnSua.enable(); btnXoa.enable();
            }
            else {
                btnSua.disable(); btnXoa.disable();
            }

            if (GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') == 4 || GridPanel1.getSelectionModel().getSelected().get('TRANG_THAI') == 5
            ) {
                btnInPhieu.enable();
            }
            else {
                btnInPhieu.disable();
            }
        }


        function MaPhieu() {
            $.post("/Danhmuc/TaoMaTuDong",{Ma:"PYCTH"}, function (data) {
                MA_PYC.setValue(data);
            }, "json");

        }

        function InPhieu() {
            var myc = GridPanel1.getSelectionModel().getSelected().get('MA_PYC');
            window.location.href = "/Report/HienBaoCao.aspx?bc=pycth&ts=" + myc;
        }
    </script>
    <script runat="server">
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
            NGAY_DE_NGHI.SetValue(DateTime.Now);
            this.Store3.DataSource = new object[]
            {
                new object[] {1,"Lý do 1"},
                new object[] {2,"Lý do 2"},
                new object[] {3,"Lý do 3"},
                new object[] {4,"Lý do 4"},
                new object[] {5,"Lý do 5"},
                new object[] {6,"Lý do khác"}     
            };
            this.Store3.DataBind();
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="RowLayout" Border="false">
            <Items>
                <ext:Hidden ID="ID_PYC" runat="server" />
                <ext:GridPanel ID="GridPanel1" RowHeight="0.6" runat="server" Border="false" TrackMouseOver="true">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">
                                    <Listeners>
                                        <Click Handler="#{ycstbForm}.reset();#{wdYCTBM}.show();resetTTC();resetYC();#{MA_PYC}.disable(); MaPhieu();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>"
                                    Icon="BookEdit">
                                    <Listeners>
                                        <Click Fn="sua" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>"
                                    Icon="Delete">
                                    <DirectEvents>
                                        <Click Url="/Danhmuc/xoaYeuCauThietBi" CleanRequest="true" Method="POST" Success="#{dsYeuCauTongQuat}.reload();#{dsYeuCauChiTiet}.reload();">
                                            <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                            <ExtraParams>
                                                <ext:Parameter Name="MA_PYC" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC')"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnInPhieu" Disabled="true" runat="server" Text="In phiếu" Icon="PrinterAdd">
                                    <Listeners>
                                        <Click Handler="InPhieu()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Nhập mã yêu cầu hoặc tên người đề nghị..."
                                    EnableKeyEvents="true" Width="400px">
                                    <Listeners>
                                        <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                        <TriggerClick Handler="#{dsYeuCauTongQuat}.reload();" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["ID"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsYeuCauTongQuat}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsYeuCauTongQuat" runat="server" RemoteSort="true" UseIdConfirmation="true"
                            GroupField="LOAI_DP">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauTongQuat/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_PYC" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="LY_DO" />
                                        <ext:RecordField Name="NGAY_DE_NGHI" Type="Date" />
                                        <ext:RecordField Name="NGAY_CAP" Type="Date" />
                                        <ext:RecordField Name="LOAI" />
                                        <ext:RecordField Name="LOAI_DP" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                        <ext:RecordField Name="TRANG_THAI" />
                                        <ext:RecordField Name="TRANG_THAI_DP" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="loai" Value="3" Mode="Raw" />
                                <ext:Parameter Name="trangthai" Value="'1,2,3,4'" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_PYC" DataIndex="MA_PYC" Header="Mã số phiếu" Width="150" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người đề nghị" Width="150">
                            </ext:Column>
                            <ext:Column ColumnID="LY_DO" DataIndex="LY_DO" Header="Lý do yêu cầu" Width="200" />
                            <ext:DateColumn ColumnID="NGAY_DE_NGHI" DataIndex="NGAY_DE_NGHI" Header="Ngày đề nghị"
                                Width="100" Format="dd/MM/yyyy" Align="Center" />
                            <ext:Column ColumnID="LOAI_DP" DataIndex="LOAI_DP" Header="Loại" Width="200" />
                            <ext:Column ColumnID="TRANG_THAI_DP" DataIndex="TRANG_THAI_DP" Header="Trạng thái"
                                Width="200" />
                            <ext:Column ColumnID="MO_TA" DataIndex="MO_TA" Header="Mô tả" Width="200" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="
                             setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                             #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                             kiemtra();
                             " />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="LY_DO" />
                                <ext:StringFilter DataIndex="NGAY_DE_NGHI" />
                                <ext:StringFilter DataIndex="MO_TA" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:GroupingView ID="BufferView1" runat="server" ScrollDelay="0" HideGroupedColumn="true">
                            <GetRowClass Fn="getRowClass" />
                        </ext:GroupingView>
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="
                              setTimeout(function(){dsYeuCauChiTiet.reload();},500);
                              #{ID_PYC}.setValue(#{GridPanel1}.getSelectionModel().getSelected().get('MA_PYC'));
                              kiemtra();
                             " />
                    </Listeners>
                </ext:GridPanel>
                <ext:GridPanel ID="GridPanel2" RowHeight="0.6" Title="Chi tiết yêu cầu" runat="server"
                    Border="false" TrackMouseOver="true">
                    <Store>
                        <ext:Store ID="dsYeuCauChiTiet" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Danhmuc/dsYeuCauChiTiet/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="ID" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_PYC" />
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="MA_TB" />
                                        <ext:RecordField Name="SERIAL" />
                                        <ext:RecordField Name="TEN_TB" />
                                        <ext:RecordField Name="MA_KIEU" />
                                        <ext:RecordField Name="TEN_KIEU" />
                                        <ext:RecordField Name="MA_CL" />
                                        <ext:RecordField Name="TEN_CL" />
                                        <ext:RecordField Name="SO_LUONG" />
                                        <ext:RecordField Name="MO_TA" />
                                        <ext:RecordField Name="GHI_CHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{ID_PYC}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150" />
                            <ext:Column ColumnID="MA_TB" DataIndex="MA_TB" Header="Mã thiết bị" Width="150" />
                            <ext:Column ColumnID="TEN_TB" DataIndex="TEN_TB" Header="Tên thiết bị" Width="150" />
                            <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="100" />
                            <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                            <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                            <ext:Column ColumnID="SO_LUONG" DataIndex="SO_LUONG" Header="Số lượng" Width="80"
                                Align="Right" />
                            <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="200" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server">
                            <Listeners>
                                <RowSelect Handler="" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters3" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="TEN_CL" />
                                <ext:StringFilter DataIndex="TEN_KIEU" />
                                <ext:StringFilter DataIndex="SO_LUONG" />
                                <ext:StringFilter DataIndex="GHI_CHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <View>
                        <ext:BufferView ID="BufferView3" runat="server" ScrollDelay="0" />
                    </View>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdYCTBM" runat="server" Hidden="true" Title="Yêu cầu thu hồi thiết bị"
            Icon="Information" Width="1010" Height="500" Resizable="false">
            <Items>
                <ext:FormPanel ID="ycstbForm" runat="server" Height="470" Width="1000" Border="false"
                    Url="/Danhmuc/saveYeuCauThuHoi" ButtonAlign="Center">
                    <Items>
                        <ext:Hidden ID="ID" runat="server" />
                        <ext:Container ID="Container1" runat="server" Layout="HBoxLayout">
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Margins="10 5 10 10" Flex="1" Title="Thông tin chung"
                                    Layout="AnchorLayout" DefaultAnchor="100%" Width="250" Height="425">
                                    <Items>
                                        <ext:Panel ID="Panel3" runat="server" Title="" AutoHeight="true" FormGroup="true">
                                            <Content>
                                                <table border="0" cellpadding="5" cellspacing="5">
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Mã phiếu:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:TextField ID="MA_PYC" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                                                DataIndex="MA_PYC" runat="server" Width="220" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Người đề nghị:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:ComboBox ID="MA_NV" runat="server" DataIndex="MA_NV" TriggerAction="All" EmptyText="Chọn người đề nghị..."
                                                                Mode="Local" AllowBlank="false" IndicatorCls="red" Width="220" EnableKeyEvents="true"
                                                                DisplayField="TEN_NV" ItemSelector="div.search-item" ValueField="MA_NV">
                                                                <Store>
                                                                    <ext:Store ID="Store1" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                                        <Proxy>
                                                                            <ext:HttpProxy Url="/Hethong/dsNhanvien" />
                                                                        </Proxy>
                                                                        <Reader>
                                                                            <ext:JsonReader IDProperty="MA_NV" Root="data" TotalProperty="total">
                                                                                <Fields>
                                                                                    <ext:RecordField Name="MA_NV" SortDir="ASC" />
                                                                                    <ext:RecordField Name="TEN_NV" />
                                                                                </Fields>
                                                                            </ext:JsonReader>
                                                                        </Reader>
                                                                        <BaseParams>
                                                                            <ext:Parameter Name="txtfilter" Value="#{MA_NV}.getValue()" Mode="Raw" />
                                                                            <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                                        </BaseParams>
                                                                    </ext:Store>
                                                                </Store>
                                                                <Template ID="Template1" runat="server">
                                                                    <Html>
                                                                        <tpl for=".">
						                                          <div class="search-item">
							                                         <h3><span>{MA_NV}</span>{TEN_NV}</h3>	                
						                                          </div>
					                                           </tpl>
                                                                    </Html>
                                                                </Template>
                                                                <Triggers>
                                                                    <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                                </Triggers>
                                                                <Listeners>
                                                                    <TriggerClick Handler="this.setValue('');" />
                                                                </Listeners>
                                                            </ext:ComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Ngày đề nghị:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:DateField ID="NGAY_DE_NGHI" DataIndex="NGAY_DE_NGHI" IndicatorText="*" AllowBlank="false"
                                                                IndicatorCls="red" runat="server" Width="220" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Lý do:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:ComboBox ID="ID_LD" runat="server" DataIndex="ID_LD" TriggerAction="All" Mode="Local"
                                                                ItemSelector="div.search-item" Width="220" IndicatorText="*" AllowBlank="false"
                                                                IndicatorCls="red" EnableKeyEvents="true" DisplayField="VALUE_LD" ValueField="ID_LD"
                                                                EmptyText="Chọn lý do...">
                                                                <Store>
                                                                    <ext:Store ID="Store3" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                                        <Reader>
                                                                            <ext:ArrayReader>
                                                                                <Fields>
                                                                                    <ext:RecordField Name="ID_LD" />
                                                                                    <ext:RecordField Name="VALUE_LD" />
                                                                                </Fields>
                                                                            </ext:ArrayReader>
                                                                        </Reader>
                                                                        <SortInfo Field="ID_LD" Direction="ASC" />
                                                                    </ext:Store>
                                                                </Store>
                                                                <Template ID="Template3" runat="server">
                                                                    <Html>
                                                                        <tpl for=".">
                                                <div class="search-item">
                                                <h3><span>{ID_LD}</span>{VALUE_LD}</h3>                 
                                                </div>
                                                </tpl>
                                                                    </Html>
                                                                </Template>
                                                                <Triggers>
                                                                    <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                                </Triggers>
                                                                <Listeners>
                                                                    <TriggerClick Handler="this.setValue('');" />
                                                                    <Select Handler="lydo()" />
                                                                </Listeners>
                                                            </ext:ComboBox>
                                                            <br />
                                                            <ext:TextArea ID="LY_DO" DataIndex="LY_DO" runat="server" EmptyText="Nhập lý do..."
                                                                Width="220" Height="80" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Ghi chú:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:TextArea ID="GHI_CHU_PYC" DataIndex="GHI_CHU_PYC" runat="server" Width="220"
                                                                Height="80" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ext:Panel>
                                    </Items>
                                </ext:FieldSet>
                                <ext:FieldSet ID="FieldSet2" runat="server" Margins="10 10 10 5" Flex="1" Title="Yêu cầu"
                                    Layout="AnchorLayout" DefaultAnchor="100%" Width="715" Height="425">
                                    <Items>
                                        <ext:Panel ID="Panel2" runat="server" Title="" AutoHeight="true" FormGroup="true">
                                            <Content>
                                                <table border="0" cellpadding="5" cellspacing="5">
                                                    <tr>
                                                        <td colspan="3">
                                                            <span class="x-label-text">Người sử dụng:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <ext:ComboBox ID="MA_NSD" runat="server" DataIndex="MA_NV" TriggerAction="All" EmptyText="Chọn người sử dụng..."
                                                                Mode="Local" IndicatorText="*" AllowBlank="false" IndicatorCls="red" Width="255"
                                                                PageSize="100" EnableKeyEvents="true" DisplayField="TEN_NV" ItemSelector="div.search-item"
                                                                ValueField="MA_NV">
                                                                <Store>
                                                                    <ext:Store ID="dsNhanvien" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                                        <Proxy>
                                                                            <ext:HttpProxy Url="/Hethong/dsNhanvien" />
                                                                        </Proxy>
                                                                        <Reader>
                                                                            <ext:JsonReader IDProperty="MA_NV" Root="data" TotalProperty="total">
                                                                                <Fields>
                                                                                    <ext:RecordField Name="MA_NV" SortDir="ASC" />
                                                                                    <ext:RecordField Name="TEN_NV" />
                                                                                </Fields>
                                                                            </ext:JsonReader>
                                                                        </Reader>
                                                                        <BaseParams>
                                                                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                                        </BaseParams>
                                                                    </ext:Store>
                                                                </Store>
                                                                <Template ID="Template2" runat="server">
                                                                    <Html>
                                                                        <tpl for=".">
						                                          <div class="search-item">
							                                         <h3><span>{MA_NV}</span>{TEN_NV}</h3>	                
						                                          </div>
					                                           </tpl>
                                                                    </Html>
                                                                </Template>
                                                                <Triggers>
                                                                    <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                                </Triggers>
                                                                <Listeners>
                                                                    <TriggerClick Handler="this.setValue('');" />
                                                                    <Select Handler="#{SERIAL}.setValue('');#{TEN_CL}.setValue('');#{TEN_KIEU}.setValue('');#{MA_CL}.setValue('');#{MA_KIEU}.setValue('');
                                                                    setTimeout(function(){dsTatCaThietBi.reload()},500);" />
                                                                </Listeners>
                                                            </ext:ComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="x-label-text">Serial thiết bị:</span>
                                                        </td>
                                                        <td>
                                                            <span class="x-label-text">Chủng loại:</span>
                                                        </td>
                                                        <td>
                                                            <span class="x-label-text">Kiểu:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ext:ComboBox ID="SERIAL" runat="server" DataIndex="SERIAL" TriggerAction="All" Mode="Local"
                                                                ItemSelector="div.search-item" Width="255" IndicatorText="*" AllowBlank="false"
                                                                IndicatorCls="red" EnableKeyEvents="true" DisplayField="SERIAL" ValueField="SERIAL"
                                                                PageSize="10" EmptyText="Chọn số serial...">
                                                                <Store>
                                                                    <ext:Store ID="dsTatCaThietBi" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                                        <Proxy>
                                                                            <ext:HttpProxy Url="/Danhmuc/TimKiemThietBi_NSD" />
                                                                        </Proxy>
                                                                        <Reader>
                                                                            <ext:JsonReader IDProperty="SERIAL" Root="data" TotalProperty="total">
                                                                                <Fields>
                                                                                    <ext:RecordField Name="SERIAL" SortDir="ASC" />
                                                                                    <ext:RecordField Name="TEN_TB" />
                                                                                </Fields>
                                                                            </ext:JsonReader>
                                                                        </Reader>
                                                                        <BaseParams>
                                                                            <ext:Parameter Name="MA_NSD" Value="#{MA_NSD}.getValue()" Mode="Raw" />
                                                                        </BaseParams>
                                                                    </ext:Store>
                                                                </Store>
                                                                <Template ID="Template4" runat="server">
                                                                    <Html>
                                                                        <tpl for=".">
                                                <div class="search-item">
                                                <h3><span>{TEN_TB}</span>{SERIAL}</h3>                 
                                                </div>
                                                </tpl>
                                                                    </Html>
                                                                </Template>
                                                                <Triggers>
                                                                    <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                                                </Triggers>
                                                                <Listeners>
                                                                    <TriggerClick Handler="this.setValue('');" />
                                                                    <Select Handler="setValueForText();" />
                                                                </Listeners>
                                                            </ext:ComboBox>
                                                        </td>
                                                        <td>
                                                            <ext:Hidden ID="MA_CL" runat="server" />
                                                            <ext:TextField ID="TEN_CL" DataIndex="MA_CL" runat="server" Width="200" Disabled="true" />
                                                        </td>
                                                        <td>
                                                            <ext:Hidden ID="MA_KIEU" runat="server" />
                                                            <ext:TextField ID="TEN_KIEU" DataIndex="MA_KIEU" runat="server" Width="200" Disabled="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <span class="x-label-text">Ghi chú:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <ext:TextArea ID="GHI_CHU" DataIndex="GHI_CHU" runat="server" Width="685" Height="60" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <ext:CompositeField ID="CompositeField3" runat="server">
                                                                <Items>
                                                                    <ext:Button ID="Button2" runat="server" Text="Thêm mới" Width="90" Icon="Add">
                                                                        <Listeners>
                                                                            <Click Handler="addRecord();" />
                                                                        </Listeners>
                                                                    </ext:Button>
                                                                    <ext:Button ID="Button4" runat="server" Text="Lưu lại" Width="90" Icon="ApplicationEdit">
                                                                        <Listeners>
                                                                            <Click Handler="updateRecord();" />
                                                                        </Listeners>
                                                                    </ext:Button>
                                                                    <ext:Button ID="Button6" runat="server" Text="Làm lại" Width="90" Icon="Reload">
                                                                        <Listeners>
                                                                            <Click Handler="resetYC();" />
                                                                        </Listeners>
                                                                    </ext:Button>
                                                                    <ext:Button ID="Button5" runat="server" Text="Xóa" Width="90" Icon="Delete">
                                                                        <Listeners>
                                                                            <Click Handler="#{gpDSYCTB}.deleteSelected();resetYC();" />
                                                                        </Listeners>
                                                                    </ext:Button>
                                                                </Items>
                                                            </ext:CompositeField>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <ext:Hidden ID="id_update" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <ext:GridPanel ID="gpDSYCTB" RowHeight="0.6" runat="server" Border="true" Width="685"
                                                                Height="180" TrackMouseOver="true" AutoExpandColumn="GHI_CHU">
                                                                <Store>
                                                                    <ext:Store ID="TimKiemTheoMaPhieuYeuCau" runat="server">
                                                                        <Proxy>
                                                                            <ext:HttpProxy Url="/Danhmuc/dsYeuCauChiTiet/" />
                                                                        </Proxy>
                                                                        <Reader>
                                                                            <ext:JsonReader IDProperty="ID" Root="data" TotalProperty="total">
                                                                                <Fields>
                                                                                    <ext:RecordField Name="MA_CL" />
                                                                                    <ext:RecordField Name="MA_KIEU" />
                                                                                    <ext:RecordField Name="SERIAL" />
                                                                                    <ext:RecordField Name="MA_TB" />
                                                                                    <ext:RecordField Name="TEN_TB" />
                                                                                    <ext:RecordField Name="GHI_CHU" />
                                                                                    <ext:RecordField Name="TEN_CL" />
                                                                                    <ext:RecordField Name="TEN_KIEU" />
                                                                                    <ext:RecordField Name="MA_NV" />
                                                                                    <ext:RecordField Name="TEN_NV" />
                                                                                </Fields>
                                                                            </ext:JsonReader>
                                                                        </Reader>
                                                                        <BaseParams>
                                                                            <ext:Parameter Name="txtfilter" Value="#{MA_PYC}.getValue()" Mode="Raw" />
                                                                            <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                                                            <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                                        </BaseParams>
                                                                    </ext:Store>
                                                                </Store>
                                                                <ColumnModel ID="ColumnModel2" runat="server">
                                                                    <Columns>
                                                                        <ext:RowNumbererColumn />
                                                                        <ext:Column ColumnID="SERIAL" DataIndex="SERIAL" Header="Serial" Width="100" />
                                                                        <ext:Column ColumnID="TEN_CL" DataIndex="TEN_CL" Header="Chủng loại" Width="150" />
                                                                        <ext:Column ColumnID="TEN_KIEU" DataIndex="TEN_KIEU" Header="Kiểu" Width="150" />
                                                                        <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Người sử dụng" Width="150" />
                                                                        <ext:Column ColumnID="GHI_CHU" DataIndex="GHI_CHU" Header="Ghi chú" Width="150" />
                                                                    </Columns>
                                                                </ColumnModel>
                                                                <SelectionModel>
                                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server">
                                                                        <Listeners>
                                                                            <RowSelect Handler="
                            
                             #{MA_CL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_CL'));
                             #{TEN_CL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('TEN_CL'));
                             #{MA_KIEU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_KIEU'));
                             #{TEN_KIEU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('TEN_KIEU'));
                             #{GHI_CHU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('GHI_CHU'));
                             

                             #{MA_NSD}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_NV'));
                             #{dsTatCaThietBi}.reload();
                             setTimeout(function(){ #{SERIAL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('SERIAL'));},800);


                             #{id_update}.setValue('1');
                             " />
                                                                        </Listeners>
                                                                    </ext:RowSelectionModel>
                                                                </SelectionModel>
                                                                <Plugins>
                                                                    <ext:GridFilters ID="GridFilters2" runat="server" Local="true">
                                                                        <Filters>
                                                                            <ext:StringFilter DataIndex="TEN_CL" />
                                                                            <ext:StringFilter DataIndex="TEN_KIEU" />
                                                                            <ext:StringFilter DataIndex="SO_LUONG" />
                                                                            <ext:StringFilter DataIndex="GHI_CHU" />
                                                                        </Filters>
                                                                    </ext:GridFilters>
                                                                </Plugins>
                                                                <View>
                                                                    <ext:BufferView ID="BufferView2" runat="server" ScrollDelay="0" />
                                                                </View>
                                                                <LoadMask ShowMask="true" />
                                                                <SaveMask ShowMask="true" />
                                                                <Listeners>
                                                                    <RowClick Handler="
                             #{MA_CL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_CL'));
                             #{TEN_CL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('TEN_CL'));
                             #{MA_KIEU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_KIEU'));
                             #{TEN_KIEU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('TEN_KIEU'));
                             #{GHI_CHU}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('GHI_CHU'));
                             #{MA_NSD}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('MA_NV'));
                             #{dsTatCaThietBi}.reload();
                             setTimeout(function(){ #{SERIAL}.setValue(#{gpDSYCTB}.getSelectionModel().getSelected().get('SERIAL'));},800);
                             #{id_update}.setValue('1');" />
                                                                </Listeners>
                                                            </ext:GridPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ext:Panel>
                                    </Items>
                                </ext:FieldSet>
                            </Items>
                        </ext:Container>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="luulai();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="resetTTC();resetYC();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                    <BaseParams>
                        <ext:Parameter Name="gpAllData" Value="Ext.encode(#{gpDSYCTB}.getRowsValues())" Mode="Raw" />
                    </BaseParams>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="" />
            </Listeners>
        </ext:Window>
    </div>
</body>
</html>
