﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Danh sách nhân viên</title>
    <link rel="stylesheet" type="text/css" href="/Resources/css/main.css" />
    <script src="../../Scripts/jquery-1.11.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .search-item
        {
            font: normal 11px tahoma, arial, helvetica, sans-serif;
            padding: 3px 10px 3px 10px;
            border: 1px solid #fff;
            border-bottom: 1px solid #eeeeee;
            white-space: normal;
            color: #555;
        }
        
        .search-item h3
        {
            display: block;
            font: inherit;
            font-weight: bold;
            color: #222;
        }
        
        .search-item h3 span
        {
            float: right;
            font-weight: normal;
            margin: 0 0 5px 5px;
            width: 100px;
            display: block;
            clear: none;
        }
    </style>
    <script type="text/javascript">
        
        var failureHandler = function (form, action) {
            var msg = "";
            if (action.failureType == "client" && (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Kiểm tra thông tin khu vực!";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }
            else {

                if (MA_NV.getValue() == '') {
                    MA_NV.focus();
                    msg = "Vui lòng nhập mã nhân viên.";
                }
                else if (MA_DV.getValue() == '') {
                    MA_DV.focus();
                    msg = "Vui lòng chọn đơn vị.";
                }
                else if (MA_PB.getValue() == '') {
                    MA_PB.focus();
                    msg = "Vui lòng chọn phòng ban.";
                }
                else if (MA_CV.getValue() == '') {
                    MA_CV.focus();
                    msg = "Vui lòng chọn chức vụ.";
                }
                else {
                    msg = "Kiểm tra thông tin khi nhập.";
                }
            }

            Ext.Msg.show({
                title: "Thông báo",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        };

        var successHandler = function (form, action) {
            dsNhanvien.reload();
            nvForm.reset();
            wdNv.hide();
            id.setValue('');
            btnSua.disable();
            btnXoa.disable();
        };

        var sua = function () {
            var s = GridPanel1.getSelectionModel().getSelected().get('MA_NV');
            id.setValue(s);
            var record = GridPanel1.getSelectionModel().getSelected();
            nvForm.getForm().loadRecord(record);
            MA_NV.disable();
            wdNv.show();

            var gt = GridPanel1.getSelectionModel().getSelected().get('GIOI_TINH');
            if (gt == true) {
                $("#rdNam").prop("checked", true);
                $("#rdNu").prop("checked", false);
            }
            else {
                $("#rdNam").prop("checked", false);
                $("#rdNu").prop("checked", true);
            }

            setTimeout(TimKiemPhongBanTheoMaDonVi.reload(), 900);
            var s1 = GridPanel1.getSelectionModel().getSelected().get('MA_PB');
            setTimeout(function () { MA_PB.setValue(s1); }, 1000);
        };

        var kiemtra = function () {
            $.post("/Hethong/KiemTraMaNV", { MA_NV: MA_NV.getValue() }, function (data) {
                if (data == 'NO') {
                    alert("Mã nhân viên đã tồn tại vui lòng nhập mã khác.");
                    MA_NV.focus();
                }
            });
        }
          
    </script>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["themes"] != null)
            {
                string themes = Session["themes"].ToString();
                if (!string.IsNullOrEmpty(themes))
                    ResourceManager1.Theme = (Theme)Enum.Parse(typeof(Theme), themes);
            }
        }
    </script>
</head>
<body>
    <div>
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Viewport ID="Panel1" runat="server" Layout="FitLayout" Border="false">
            <Items>
                <ext:GridPanel ID="GridPanel1" runat="server" Border="false" TrackMouseOver="true"
                    AutoExpandColumn="GHICHU">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="Button1" runat="server" Text="<%$Resources:lang, Add %>" Icon="Add">
                                    <Listeners>
                                        <Click Handler="#{nvForm}.reset();#{wdNv}.show();#{MA_NV}.enable();#{MA_DV}.setValue('');#{MA_PB}.setValue('');#{MA_CV}.setValue('');" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnSua" Disabled="true" runat="server" Text="<%$Resources:lang, Edit %>"
                                    Icon="BookEdit">
                                    <Listeners>
                                        <Click Fn="sua" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnXoa" Disabled="true" runat="server" Text="<%$Resources:lang, Delete %>"
                                    Icon="Delete">
                                    <DirectEvents>
                                        <Click Url="/Hethong/xoaNhanvien" CleanRequest="true" Method="POST" Success="#{dsNhanvien}.reload();">
                                            <Confirmation ConfirmRequest="true" Title="<%$Resources:lang, Notice %>" Message="<%$Resources:lang, THONGBAO_XOA %>" />
                                            <ExtraParams>
                                                <ext:Parameter Name="id" Value="#{GridPanel1}.getSelectionModel().getSelected().get('MA_NV')"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:ToolbarFill ID="ToolbarFill2" runat="server" />
                                <ext:TriggerField ID="txtLichLV" runat="server" EmptyText="Tìm kiếm theo mã hoặc tên nhân viên..."
                                    EnableKeyEvents="true" Width="400px">
                                    <Listeners>
                                        <KeyDown Buffer="400" Handler="#{txtFilterLichLV}.setValue(this.getValue()); if(!Ext.isEmpty(this.getValue())) { this.triggers[0].show(); }" />
                                        <Blur Handler="if (Ext.isEmpty(this.getValue())) { this.triggers[0].hide(); }" />
                                    </Listeners>
                                    <Triggers>
                                        <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        <ext:FieldTrigger Icon="Search" />
                                    </Triggers>
                                </ext:TriggerField>
                                <ext:Hidden ID="txtFilterLichLV" runat="server" Text='<%# ViewData["MA_NV"] %>' AutoDataBind="true">
                                    <Listeners>
                                        <Change Handler="#{dsNhanvien}.reload();" Delay="100" />
                                    </Listeners>
                                </ext:Hidden>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="dsNhanvien" runat="server" RemoteSort="true" UseIdConfirmation="true">
                            <Proxy>
                                <ext:HttpProxy Url="/Hethong/dsNhanvien/" />
                            </Proxy>
                            <Reader>
                                <ext:JsonReader IDProperty="MA_NV" Root="data" TotalProperty="total">
                                    <Fields>
                                        <ext:RecordField Name="MA_NV" />
                                        <ext:RecordField Name="TEN_NV" />
                                        <ext:RecordField Name="MA_DV" />
                                        <ext:RecordField Name="TEN_DV" />
                                        <ext:RecordField Name="MA_PB" />
                                        <ext:RecordField Name="TEN_PB" />
                                        <ext:RecordField Name="MA_CV" />
                                        <ext:RecordField Name="TEN_CV" />
                                        <ext:RecordField Name="NGAY_SINH" Type="Date" />
                                        <ext:RecordField Name="GIOI_TINH" />
                                        <ext:RecordField Name="CMND" />
                                        <ext:RecordField Name="NGAY_CAP" Type="Date" />
                                        <ext:RecordField Name="NOI_CAP" />
                                        <ext:RecordField Name="EMAIL" />
                                        <ext:RecordField Name="TEN_TAT" />
                                        <ext:RecordField Name="NICK_CHAT" />
                                        <ext:RecordField Name="HOKHAU" />
                                        <ext:RecordField Name="DIACHI" />
                                        <ext:RecordField Name="TAIKHOAN" />
                                        <ext:RecordField Name="SDT" />
                                        <ext:RecordField Name="SDD" />
                                        <ext:RecordField Name="SDT_FAMILY" />
                                        <ext:RecordField Name="GHICHU" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <BaseParams>
                                <ext:Parameter Name="txtfilter" Value="#{txtFilterLichLV}.getValue()" Mode="Raw" />
                                <ext:Parameter Name="limit" Value="100" Mode="Raw" />
                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                            </BaseParams>
                            <SortInfo Field="MA_PB" Direction="ASC" />
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column ColumnID="MA_NV" DataIndex="MA_NV" Header="Mã nhân viên" />
                            <ext:Column ColumnID="TEN_NV" DataIndex="TEN_NV" Header="Tên nhân viên" Width="130" />
                            <ext:Column ColumnID="TEN_DV" DataIndex="TEN_DV" Header="Đơn vị" />
                            <ext:Column ColumnID="TEN_PB" DataIndex="TEN_PB" Header="Phòng ban" />
                            <ext:Column ColumnID="TEN_CV" DataIndex="TEN_CV" Header="Chức vụ" />
                            <ext:Column ColumnID="SDD" DataIndex="SDD" Header="Điện thoại" Align="Center" Width="80" />
                            <ext:DateColumn ColumnID="NGAY_SINH" DataIndex="NGAY_SINH" Header="Ngày sinh" Align="Center"
                                Width="80" Format="dd/MM/yyyy" />
                            <ext:Column ColumnID="DIACHI" DataIndex="DIACHI" Header="Địa chỉ" Width="280" />
                            <ext:Column ColumnID="GHICHU" DataIndex="GHICHU" Header="Ghi chú" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            <Listeners>
                                <RowSelect Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <Plugins>
                        <ext:GridFilters ID="GridFilters1" runat="server" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="MA_NV" />
                                <ext:StringFilter DataIndex="TEN_NV" />
                                <ext:StringFilter DataIndex="TEN_DV" />
                                <ext:StringFilter DataIndex="TEN_PB" />
                                <ext:StringFilter DataIndex="TEN_CV" />
                                <ext:StringFilter DataIndex="GHICHU" />
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="30" />
                    </BottomBar>
                    <LoadMask ShowMask="true" />
                    <SaveMask ShowMask="true" />
                    <Listeners>
                        <RowClick Handler="#{btnSua}.enable(); #{btnXoa}.enable();" />
                    </Listeners>
                </ext:GridPanel>
            </Items>
        </ext:Viewport>
        <ext:Window ID="wdNv" runat="server" Hidden="true" Title="Thông tin nhân viên" Icon="Information"
            Width="730" Height="390" Resizable="false">
            <Items>
                <ext:FormPanel ID="nvForm" runat="server" Height="360" Width="720" Border="false"
                    Url="/Hethong/saveNhanvien" ButtonAlign="Center">
                    <Items>
                        <ext:FieldSet ID="FieldSet1" runat="server" Layout="FormLayout" Border="false" Cls="textfield">
                            <Items>
                                <ext:Hidden ID="id" runat="server" />
                                <ext:TextField ID="MA_NV" DataIndex="MA_NV" runat="server" IndicatorText="*" AnchorHorizontal="65%"
                                    IndicatorCls="red" FieldLabel="Mã nhân viên" AllowBlank="false">
                                    <Listeners>
                                    <Change Fn="kiemtra" />
                                    </Listeners>
                                    </ext:TextField>
                                <ext:CompositeField ID="CompositeField3" runat="server" FieldLabel="Họ tên" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:TextField ID="TEN_NV" IndicatorText="*" DataIndex="TEN_NV" IndicatorCls="red"
                                            runat="server" Flex="3" />
                                        <ext:DisplayField ID="DisplayField2" runat="server" Margins="0 0 0 10" Text="Tên viết tắt:"
                                            Flex="2" />
                                        <ext:TextField ID="TEN_TAT" DataIndex="TEN_TAT" runat="server" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField6" runat="server" FieldLabel="Giới tính" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:RadioGroup ID="grGioitinh" runat="server" Flex="3" FieldLabel="Giới tính">
                                            <Items>
                                                <ext:Radio ID="rdNam" runat="server" BoxLabel="Nam" Checked="true" InputValue="nam" />
                                                <ext:Radio ID="rdNu" runat="server" BoxLabel="Nữ" InputValue="nu" />
                                            </Items>
                                        </ext:RadioGroup>
                                        <ext:DisplayField ID="DisplayField6" runat="server" Margins="0 0 0 10" Flex="5" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField2" runat="server" FieldLabel="Ngày sinh" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:DateField ID="NGAY_SINH" DataIndex="NGAY_SINH" runat="server" Flex="3" />
                                        <ext:DisplayField ID="DisplayField1" runat="server" Margins="0 0 0 10" Text="SĐT:"
                                            Flex="2" />
                                        <ext:TextField ID="SDT" DataIndex="SDT" runat="server" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField5" runat="server" FieldLabel="SDD" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:TextField ID="SDD" DataIndex="SDD" runat="server" Flex="3" />
                                        <ext:DisplayField ID="DisplayField5" runat="server" Margins="0 0 0 10" Text="ĐT người thân:"
                                            Flex="2" />
                                        <ext:TextField ID="SDT_FAMILY" DataIndex="SDT_FAMILY" runat="server" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:CompositeField ID="CompositeField1" runat="server" FieldLabel="Đơn vị:" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:ComboBox ID="MA_DV" runat="server" DataIndex="MA_DV" TriggerAction="All" Mode="Local"
                                            ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                            EnableKeyEvents="true" DisplayField="TEN_DV" ValueField="MA_DV" Flex="3" EmptyText="Chọn đơn vị...">
                                            <Store>
                                                <ext:Store ID="dsDonvi" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/dsDonvi" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_DV" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_DV" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_DV" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template4" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_DV}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');" />
                                                <Select Handler="#{TimKiemPhongBanTheoMaDonVi}.reload();#{MA_PB}.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                        <ext:DisplayField ID="DisplayField4" runat="server" Margins="0 0 0 10" Text="Phòng ban:"
                                            Flex="2" />
                                        <ext:ComboBox ID="MA_PB" runat="server" DataIndex="MA_PB" TriggerAction="All" Mode="Local"
                                            ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                            EnableKeyEvents="true" DisplayField="TEN_PB" ValueField="MA_PB" Flex="3" EmptyText="Chọn phòng ban...">
                                            <Store>
                                                <ext:Store ID="TimKiemPhongBanTheoMaDonVi" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                                    <Proxy>
                                                        <ext:HttpProxy Url="/Danhmuc/TimKiemPhongBanTheoMaDonVi" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="MA_PB" Root="data" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="MA_PB" SortDir="ASC" />
                                                                <ext:RecordField Name="TEN_PB" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>
                                                    <BaseParams>
                                                        <ext:Parameter Name="txtfilter" Value="#{MA_DV}.getValue()" Mode="Raw" />
                                                        <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                        <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                                    </BaseParams>
                                                </ext:Store>
                                            </Store>
                                            <Template ID="Template1" runat="server">
                                                <Html>
                                                    <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_PB}</h3>                 
                                                </div>
                                                </tpl>
                                                </Html>
                                            </Template>
                                            <Triggers>
                                                <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="this.setValue('');" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:CompositeField>
                                <ext:ComboBox ID="MA_CV" runat="server" DataIndex="MA_CV" TriggerAction="All" Mode="Local"
                                    ItemSelector="div.search-item" IndicatorText="*" AllowBlank="false" IndicatorCls="red"
                                    EnableKeyEvents="true" DisplayField="TEN_CV" ValueField="MA_CV" FieldLabel="Chức vụ"
                                    AnchorHorizontal="65%" EmptyText="Chọn chức vụ...">
                                    <Store>
                                        <ext:Store ID="dsChucVu" runat="server" RemoteSort="true" UseIdConfirmation="true">
                                            <Proxy>
                                                <ext:HttpProxy Url="/Danhmuc/dsChucVu" />
                                            </Proxy>
                                            <Reader>
                                                <ext:JsonReader IDProperty="MA_CV" Root="data" TotalProperty="total">
                                                    <Fields>
                                                        <ext:RecordField Name="MA_CV" SortDir="ASC" />
                                                        <ext:RecordField Name="TEN_CV" />
                                                    </Fields>
                                                </ext:JsonReader>
                                            </Reader>
                                            <BaseParams>
                                                <ext:Parameter Name="txtfilter" Value="#{MA_CV}.getValue()" Mode="Raw" />
                                                <ext:Parameter Name="limit" Value="1000" Mode="Raw" />
                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                            </BaseParams>
                                        </ext:Store>
                                    </Store>
                                    <Template ID="Template2" runat="server">
                                        <Html>
                                            <tpl for=".">
                                                <div class="search-item">
                                                <h3>{TEN_CV}</h3>                 
                                                </div>
                                                </tpl>
                                        </Html>
                                    </Template>
                                    <Triggers>
                                        <ext:FieldTrigger AutoDataBind="true" Icon="Clear" />
                                    </Triggers>
                                    <Listeners>
                                        <TriggerClick Handler="this.setValue('');" />
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:CompositeField ID="CompositeField4" runat="server" FieldLabel="Email" AnchorHorizontal="95%">
                                    <Items>
                                        <ext:TextField ID="EMAIL" DataIndex="EMAIL" runat="server" Flex="3" />
                                        <ext:DisplayField ID="DisplayField3" runat="server" Margins="0 0 0 10" Text="Tài khoản:"
                                            Flex="2" />
                                        <ext:TextField ID="TAIKHOAN" DataIndex="TAIKHOAN" runat="server" Flex="3" />
                                    </Items>
                                </ext:CompositeField>
                                <ext:TextField ID="DIACHI" DataIndex="DIACHI" runat="server" AnchorHorizontal="95%"
                                    FieldLabel="Địa chỉ" />
                                <ext:TextArea ID="GHICHU" DataIndex="GHICHU" runat="server" FieldLabel="Ghi chú"
                                    AnchorHorizontal="95%" />
                            </Items>
                        </ext:FieldSet>
                    </Items>
                    <BottomBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <ext:Button ID="btnSave" runat="server" Text="<%$Resources:lang, Save %>" Icon="Disk">
                                    <Listeners>
                                        <Click Handler="#{nvForm}.form.submit({waitMsg : 'Đang ghi...', success : successHandler, failure : failureHandler });" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="Button3" runat="server" Text="<%$Resources:lang, Reset %>" Icon="Reload">
                                    <Listeners>
                                        <Click Handler="#{nvForm}.reset();" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSpacer ID="ToolbarSpacer1" runat="server" />
                            </Items>
                        </ext:Toolbar>
                    </BottomBar>
                </ext:FormPanel>
            </Items>
            <Listeners>
                <Hide Handler="#{btnSua}.disable(); #{btnXoa}.disable();" />
            </Listeners>
        </ext:Window>
    </div>
</body>
</html>
