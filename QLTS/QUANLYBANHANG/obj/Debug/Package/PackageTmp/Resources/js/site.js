﻿Ext.ns("Northwind");

var message_box = function () {
    alert('Hello World!');
};

//var filterTree = function (el, e) {
//    var tree = panel,
//                text = this.getRawValue();

//    tree.clearFilter();

//    if (Ext.isEmpty(text, false)) {
//        return;
//    }

//    if (e.getKey() === Ext.EventObject.ESC) {
//        clearFilter();
//    } else {
//        var re = new RegExp(".*" + text + ".*", "i");

//        tree.filterBy(function (node) {
//            return re.test(node.text);
//        });
//    }
//};

var clearFilter = function () {
    var field = TriggerField1;
    //tree = panel;

    field.setValue("");
    //tree.clearFilter();
    //tree.getRootNode().collapseChildNodes(true);
    //tree.getRootNode().ensureVisible();
};




Northwind = {
    hashCode: function (str) {
        var hash = 1315423911;

        for (var i = 0; i < str.length; i++) {
            hash ^= ((hash << 5) + str.charCodeAt(i) + (hash >> 2));
        }

        return (hash & 0x7FFFFFFF);
    },

    addTab: function (config) {        
        if (Ext.isEmpty(config.url, false)) {
            return;
        }        
        var tp = Ext.getCmp('tpMain'); //window.parent.tpMain;
        var id = this.hashCode(config.url); // tab id
        var tab = tp.getComponent(id);

        if (!tab) {
            tab = tp.addTab({
                id: id.toString(),
                title: config.title,
                //iconCls: config.icon || 'icon-applicationdouble',
                closable: true,
                autoLoad: {
                    showMask: true,
                    url: config.url,
                    mode: 'iframe',
                    noCache: true,
                    maskMsg: "Loading '" + config.title + "'...",
                    scripts: true,
                    passParentSize: config.passParentSize
                }
            });
        } else {
            tp.setActiveTab(tab);
            Ext.get(tab.tabEl).frame();
        }
    }
};

