﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using QUANLYBANHANG.Models;
using Ext.Net;
using System.Data.Linq.SqlClient;
using VACOMLIB;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using QUANLYBANHANG.Resources;
using Newtonsoft.Json.Linq;

namespace QUANLYBANHANG.Controllers
{
    public class DanhmucController : BaseDataController
    {
        //
        // GET: /Danhmuc/

        public ContentResult GetMenuPanel()
        {
            ContentResult cr = new ContentResult();
            Button btn;
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Resources.lang",
                         System.Reflection.Assembly.Load("App_GlobalResources"));
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;

            Panel gr = new Panel();
            gr.ID = "menu";
            gr.Border = false;
            gr.AutoScroll = true;

            var menu = (from p in this.DbContext.DANHMUCs
                        select new
                        {
                            p.MA_DANHMUC,
                            p.TEN_DANHMUC_VN,
                            p.TEN_DANHMUC_US,
                            p.TEN_DANHMUC_CN,
                            p.TEN_DANHMUC_JP,
                            p.TEN_ICON
                        }).ToList().Take(5);
            foreach (var dm in menu)
            {
                btn = new Button();
                btn.ID = dm.MA_DANHMUC;
                if (ci.Name == "ja-JP")
                    btn.Text = dm.TEN_DANHMUC_JP;
                else if (ci.Name == "en-US")
                    btn.Text = dm.TEN_DANHMUC_US;
                else if (ci.Name == "zh-CN")
                    btn.Text = dm.TEN_DANHMUC_CN;
                else
                    btn.Text = dm.TEN_DANHMUC_VN;
                btn.Width = 230;
                btn.IconCls = dm.TEN_ICON;
                btn.Scale = ButtonScale.Large;
                //float d = (float)(1 / menu.Count);
                //btn.RowHeight = 0.2;
                btn.Listeners.Click.Fn = "loadChildMenu";
                gr.Add(btn);
            }

            string script = @"<script>{0}</script>";
            cr.Content = string.Format(script, gr.ToScript(RenderMode.AddTo, "MenuPanel"));

            return cr;
        }

        #region "Danh mục đơn vị"

        public ActionResult Dm_Donvi()
        {
            return View();
        }

        public AjaxStoreResult dsDonvi(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from c in this.DbContext.DM_DONVIs
                         where SqlMethods.Like(c.TEN_DV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || c.MA_DV.Equals(txtfilter)
                         orderby c.MA_DV ascending
                         select new
                         {
                             c.MA_DV,
                             c.TEN_DV,
                             c.SDT,
                             c.FAX,
                             c.DIACHI,
                             c.TEN_TAT,
                             c.MST,
                             c.EMAIL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveDonvi(FormCollection values)
        {
            DM_DONVI dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_DONVI> aList = this.DbContext.DM_DONVIs.Where(dv => dv.MA_DV.Equals(values["MA_DV"])).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã đơn vị đã tồn tại, Vui lòng nhập mã đơn vị khác !";
                    }
                    else
                    {
                        dm = new DM_DONVI();
                        dm.MA_DV = values["MA_DV"];
                        dm.TEN_DV = values["TEN_DV"];
                        dm.DIACHI = values["DIACHI"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.MST = values["MST"];
                        dm.SDT = values["SDT"];
                        dm.FAX = values["FAX"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.DM_DONVIs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_DV = values["TEN_DV"];
                        dm.DIACHI = values["DIACHI"];
                        dm.SDT = values["SDT"];
                        dm.TEN_TAT = values["TEN_TAT"];
                        dm.MST = values["MST"];
                        dm.FAX = values["FAX"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDonvi(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_DONVIs
                          where c.MA_DV == id
                          select c).First();
                this.DbContext.DM_DONVIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục phòng ban"

        public ActionResult Dm_PhongBan()
        {
            return View();
        }

        public AjaxStoreResult dsPhongBan(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from pb in this.DbContext.DM_PHONG_BANs
                         join dv in this.DbContext.DM_DONVIs on pb.MA_DV equals dv.MA_DV
                         where SqlMethods.Like(pb.TEN_PB, "%" + LIB.ProcessStrVal(txtfilter) + "%") || pb.MA_PB.Equals(txtfilter)
                         orderby dv.MA_DV
                         select new
                         {
                             pb.MA_PB,
                             pb.TEN_PB,
                             pb.MO_TA,
                             pb.GHI_CHU,
                             pb.MA_DV,
                             dv.TEN_DV
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult TimKiemPhongBanTheoMaDonVi(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from pb in this.DbContext.DM_PHONG_BANs
                         join dv in this.DbContext.DM_DONVIs on pb.MA_DV equals dv.MA_DV
                         where pb.MA_DV.Equals(txtfilter)
                         orderby dv.MA_DV
                         select new
                         {
                             pb.MA_PB,
                             pb.TEN_PB,
                             pb.MO_TA,
                             pb.GHI_CHU,
                             pb.MA_DV,
                             dv.TEN_DV
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult savePhongBan(FormCollection values)
        {
            DM_PHONG_BAN dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                string maPB = String.IsNullOrEmpty(values["MA_PB"]) ? string.Empty : values["MA_PB"];
                List<DM_PHONG_BAN> aList = this.DbContext.DM_PHONG_BANs.Where(pb => pb.MA_PB.Equals(values["KY_HIEU"])).ToList();
                if (String.IsNullOrEmpty(maPB))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã phòng ban đã tồn tại, vui lòng nhập mã phòng ban khác";
                    }
                    else
                    {
                        dm = new DM_PHONG_BAN();
                        dm.MA_PB = values["KY_HIEU"];
                        dm.TEN_PB = values["TEN_PB"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        dm.MA_DV = values["MA_DV_Value"];
                        this.DbContext.DM_PHONG_BANs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_PB = values["TEN_PB"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        dm.MA_DV = values["MA_DV_Value"];
                        this.DbContext.SubmitChanges();
                    }
                }

            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaPhongBan(string MA_PB)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var pb = (from c in this.DbContext.DM_PHONG_BANs
                          where c.MA_PB == MA_PB
                          select c).First();
                this.DbContext.DM_PHONG_BANs.DeleteOnSubmit(pb);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục chức vụ"

        public ActionResult Dm_ChucVu()
        {
            return View();
        }

        public AjaxStoreResult dsChucVu(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from cv in this.DbContext.DM_CHUC_VUs
                         where SqlMethods.Like(cv.TEN_CV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || cv.MA_CV.Equals(txtfilter)
                         select cv);
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveChucVu(FormCollection values)
        {
            DM_CHUC_VU dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                string maCV = String.IsNullOrEmpty(values["MA_CV"]) ? string.Empty : values["MA_CV"];
                List<DM_CHUC_VU> aList = this.DbContext.DM_CHUC_VUs.Where(cv => cv.MA_CV.Equals(maCV)).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã chức vụ đã tồn tại, vui lòng nhập mã khác.";
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(maCV))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Đã có lỗi xẩy ra vui lòng kiểm tra thông tin nhập hoặc báo lại cho người quản lý.";
                        }
                        else
                        {
                            dm = new DM_CHUC_VU();
                            dm.MA_CV = values["MA_CV"];
                            dm.TEN_CV = values["TEN_CV"];
                            dm.GHI_CHU = values["GHI_CHU"];
                            this.DbContext.DM_CHUC_VUs.InsertOnSubmit(dm);
                            this.DbContext.SubmitChanges();
                        }

                    }
                }
                else
                {
                    dm = aList[0];
                    dm.TEN_CV = values["TEN_CV"];
                    dm.GHI_CHU = values["GHI_CHU"];
                    this.DbContext.SubmitChanges();
                }

            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaChucVu(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var cv = (from c in this.DbContext.DM_CHUC_VUs
                          where c.MA_CV == id
                          select c).First();
                this.DbContext.DM_CHUC_VUs.DeleteOnSubmit(cv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục vị trí lắp đặt"

        public ActionResult Dm_Vitri()
        {
            return View();
        }

        public AjaxStoreResult dsVitri(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from vt in this.DbContext.DM_VITRI_LAPDATs
                         join dv in this.DbContext.DM_DONVIs on vt.MA_DV equals dv.MA_DV
                         join pb in this.DbContext.DM_PHONG_BANs on vt.MA_PB equals pb.MA_PB
                         where SqlMethods.Like(vt.TEN_VT, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby vt.MA_VT ascending
                         select new
                         {
                             vt.MA_VT,
                             vt.TEN_VT,
                             vt.MA_DV,
                             vt.MA_PB,
                             vt.SDT,
                             vt.GHICHU,
                             dv.TEN_DV,
                             pb.TEN_PB
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveVitri(FormCollection values)
        {
            DM_VITRI_LAPDAT dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                int mavt = String.IsNullOrEmpty(values["MA_VT"]) ? 0 : Convert.ToInt32(values["MA_VT"]);
                List<DM_VITRI_LAPDAT> aList = this.DbContext.DM_VITRI_LAPDATs.Where(vt => vt.MA_VT == mavt).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã vị trí đã tồn tại, Vui lòng nhập mã khác.";
                    }
                    else
                    {
                        if (mavt > 0)
                        {
                            dm = new DM_VITRI_LAPDAT();
                            dm.MA_VT = mavt;
                            dm.TEN_VT = values["TEN_VT"];
                            dm.SDT = values["SDT"];
                            dm.GHICHU = values["GHICHU"];
                            dm.MA_DV = values["MA_DV_Value"];
                            dm.MA_PB = values["MA_PB_Value"];
                            this.DbContext.DM_VITRI_LAPDATs.InsertOnSubmit(dm);
                            this.DbContext.SubmitChanges();
                        }
                        else
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Đã có lỗi xẩy ra vui lòng kiểm tra thông tin nhập hoặc báo lại cho người quản lý !";
                        }
                    }
                }
                else
                {
                    dm = aList[0];
                    dm.TEN_VT = values["TEN_VT"];
                    dm.SDT = values["SDT"];
                    dm.GHICHU = values["GHICHU"];
                    dm.MA_DV = values["MA_DV_Value"];
                    dm.MA_PB = values["MA_PB_Value"];
                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaVitri(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_VITRI_LAPDATs
                          where c.MA_VT == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_VITRI_LAPDATs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục chủng loại"

        public ActionResult Dm_Chungloai()
        {
            return View();
        }

        public AjaxStoreResult dsChungloai(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            var query = (from c in this.DbContext.DM_CHUNGLOAIs
                         where SqlMethods.Like(c.TEN_CL, "%" + LIB.ProcessStrVal(txtfilter) + "%") || c.MA_CL.Equals(txtfilter)
                         orderby c.MA_CL ascending
                         select new
                         {
                             c.MA_CL,
                             c.TEN_CL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }



        public AjaxFormResult saveChungloai(FormCollection values)
        {
            DM_CHUNGLOAI dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DM_CHUNGLOAIs
                        select c;
            try
            {
                List<DM_CHUNGLOAI> aList = this.DbContext.DM_CHUNGLOAIs.Where(cl => cl.MA_CL.Equals(values["MA_CL"])).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã chủng loại đã tồn tại, Vui lòng nhập mã chủng loại khác !";
                    }
                    else
                    {
                        dm = new DM_CHUNGLOAI();
                        dm.MA_CL = values["MA_CL"];
                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];

                        this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }

                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxFormResult save_ChungLoai_Ex(FormCollection values)
        {
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_CHUNGLOAI> aList = this.DbContext.DM_CHUNGLOAIs.ToList();
                HttpPostedFileBase postedFile = Request.Files["TEN_FILE"];
                string filename = postedFile.FileName;

                if (postedFile.ContentLength != 0 && (Path.GetExtension(filename) == ".xlsx" || Path.GetExtension(filename) == ".xsl" || Path.GetExtension(filename) == ".xls"))
                {
                    using (var excel = new ExcelPackage(postedFile.InputStream))
                    {
                        DM_CHUNGLOAI aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                        var worksheet = excel.Workbook.Worksheets.First();
                        for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                        {
                            string mcl = Convert.ToString(worksheet.Cells[row, 1].Value);
                            string tcl = Convert.ToString(worksheet.Cells[row, 2].Value);
                            string gc = Convert.ToString(worksheet.Cells[row, 3].Value);

                            List<DM_CHUNGLOAI> aTemp = aList.Where(cl => cl.MA_CL.Equals(mcl)).ToList();
                            if (aTemp.Count > 0)
                            {
                                aDM_CHUNGLOAI = aTemp[0];
                                aDM_CHUNGLOAI.TEN_CL = tcl;
                                aDM_CHUNGLOAI.GHICHU = gc;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(mcl))
                                {
                                    aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                                    aDM_CHUNGLOAI.MA_CL = mcl;
                                    aDM_CHUNGLOAI.TEN_CL = tcl;
                                    aDM_CHUNGLOAI.GHICHU = gc;
                                    this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(aDM_CHUNGLOAI);
                                }
                            }
                        }
                        this.DbContext.SubmitChanges();
                    }
                    return response;
                }
                else
                {
                    response.Success = false;

                    response.ExtraParams["msg"] = "Vui lòng chỉ chọn file có đuôi mở rộng là ( .xlsx, .xsl, .xls ) !";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
                return response;
            }

        }

        //Hiennv     -  Khi saveChungloai thay doi thi cung can phai sua luon ham nay
        public AjaxFormResult saveChungloai_Thieti(FormCollection values)
        {
            DM_CHUNGLOAI dm;
            AjaxFormResult response = new AjaxFormResult();
            var query = from c in this.DbContext.DM_CHUNGLOAIs
                        select c;
            try
            {
                if (string.IsNullOrEmpty(values["ID_MA_CL"]))
                {
                    foreach (DM_CHUNGLOAI p in query)
                    {
                        if (p.MA_CL.Equals(values["MA_CL_TB"]))
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Mã chủng loại đã tồn tại!";
                            return response;
                        }
                    }
                    dm = new DM_CHUNGLOAI();
                    dm.MA_CL = values["MA_CL_TB"];
                    dm.TEN_CL = values["TEN_CL"];
                    dm.GHICHU = values["GHICHU"];

                    this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(dm);
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    if (values["MA_CL_TB"].Equals(values["ID_MA_CL"]))
                    {
                        dm = (from p in this.DbContext.DM_CHUNGLOAIs
                              where p.MA_CL == values["id"]
                              select p).First();

                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];

                        this.DbContext.SubmitChanges();
                    }
                    else
                    {
                        foreach (DM_CHUNGLOAI p in query)
                        {
                            if (p.MA_CL.Equals(values["MA_CL_TB"]))
                            {
                                response.Success = false;
                                response.ExtraParams["msg"] = "Mã chủng loại đã tồn tại!";
                                return response;
                            }
                        }
                        this.DbContext.ExecuteCommand("UPDATE DM_CHUNGLOAI SET MA_CL = {0} WHERE MA_CL = {1}", values["MA_CL_TB"], values["ID_MA_CL"]);
                        dm = (from p in this.DbContext.DM_CHUNGLOAIs
                              where p.MA_CL == values["MA_CL_TB"]
                              select p).First();

                        dm.TEN_CL = values["TEN_CL"];
                        dm.GHICHU = values["GHICHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaChungloai(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_CHUNGLOAIs
                          where c.MA_CL == id
                          select c).First();
                this.DbContext.DM_CHUNGLOAIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục kiểu"

        public ActionResult Dm_Kieu()
        {
            return View();
        }

        public AjaxStoreResult dsKieu(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            var query = (from c in this.DbContext.DM_KIEUs
                         join d in this.DbContext.DM_CHUNGLOAIs on c.MA_CL equals d.MA_CL into ps
                         from d in ps.DefaultIfEmpty()
                         where SqlMethods.Like(c.TEN_KIEU, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby d.TEN_CL ascending
                         select new
                         {
                             c.MA_CL,
                             c.MA_KIEU,
                             c.TEN_KIEU,
                             d.TEN_CL,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult TimKiemKieuThemMaChungLoai(string macl)
        {
            var query = (from k in this.DbContext.DM_KIEUs
                         where k.MA_CL.Equals(macl)
                         select k);
            int total = query.ToList().Count;
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsKieu_CL(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            if (string.IsNullOrEmpty(txtfilter))
            {
                var query = (from c in this.DbContext.DM_KIEUs
                             join d in this.DbContext.DM_CHUNGLOAIs on c.MA_CL equals d.MA_CL into ps
                             from d in ps.DefaultIfEmpty()
                             orderby d.TEN_CL ascending
                             select new
                             {
                                 c.MA_CL,
                                 c.MA_KIEU,
                                 c.TEN_KIEU,
                                 d.TEN_CL,
                                 c.GHICHU
                             });
                int total = query.ToList().Count;

                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
            else
            {
                var query = (from c in this.DbContext.DM_KIEUs
                             join d in this.DbContext.DM_CHUNGLOAIs on c.MA_CL equals d.MA_CL into ps
                             from d in ps.DefaultIfEmpty()
                             where c.MA_CL == txtfilter
                             orderby d.TEN_CL ascending
                             select new
                             {
                                 c.MA_CL,
                                 c.MA_KIEU,
                                 c.TEN_KIEU,
                                 d.TEN_CL,
                                 c.GHICHU
                             });
                int total = query.ToList().Count;

                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
        }

        public AjaxFormResult saveKieu(FormCollection values)
        {
            DM_KIEU dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                int makieu = String.IsNullOrEmpty(values["MA_KIEU"]) ? 0 : Convert.ToInt32(values["MA_KIEU"]);

                List<DM_KIEU> aList = this.DbContext.DM_KIEUs.Where(k => k.MA_KIEU == makieu).ToList();

                if (string.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã kiểu đã tồn tại vui lòng nhập mã khác.";
                    }
                    else
                    {
                        if (makieu > 0)
                        {
                            dm = new DM_KIEU();
                            dm.MA_KIEU = makieu;
                            dm.TEN_KIEU = values["TEN_KIEU"];
                            dm.MA_CL = values["MA_CL_Value"];
                            dm.GHICHU = values["GHICHU"];

                            this.DbContext.DM_KIEUs.InsertOnSubmit(dm);
                            this.DbContext.SubmitChanges();
                        }
                        else
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Đã có lỗi xẩy ra vui lòng kiểm tra thông tin nhập hoặc báo lại cho người quản lý.";
                        }
                    }
                }
                else
                {
                    dm = aList[0];
                    dm.TEN_KIEU = values["TEN_KIEU"];
                    dm.MA_CL = values["MA_CL_Value"];
                    dm.GHICHU = values["GHICHU"];
                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxFormResult save_Kieu_Ex(FormCollection values)
        {
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_CHUNGLOAI> aList_cl = this.DbContext.DM_CHUNGLOAIs.ToList();
                List<DM_KIEU> aList_k = this.DbContext.DM_KIEUs.ToList();
                HttpPostedFileBase postedFile = Request.Files["TEN_FILE"];
                string filename = postedFile.FileName;

                if (postedFile.ContentLength != 0 && (Path.GetExtension(filename) == ".xlsx" || Path.GetExtension(filename) == ".xsl" || Path.GetExtension(filename) == ".xls"))
                {
                    using (var excel = new ExcelPackage(postedFile.InputStream))
                    {
                        DM_CHUNGLOAI aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                        DM_KIEU aDM_KIEU = new DM_KIEU();
                        var worksheet = excel.Workbook.Worksheets.First();
                        for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                        {
                            int mkieu = Convert.ToInt32(worksheet.Cells[row, 1].Value);
                            string mcl = Convert.ToString(worksheet.Cells[row, 2].Value);
                            string tkieu = Convert.ToString(worksheet.Cells[row, 3].Value);
                            string gc = Convert.ToString(worksheet.Cells[row, 4].Value);

                            //chung loai
                            List<DM_CHUNGLOAI> aTemp = aList_cl.Where(cl => cl.MA_CL.Equals(mcl)).ToList();
                            if (aTemp.Count > 0)
                            {
                                aDM_CHUNGLOAI = aTemp[0];
                                this.DbContext.SubmitChanges();
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(mcl))
                                {
                                    aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                                    aDM_CHUNGLOAI.MA_CL = mcl;
                                    this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(aDM_CHUNGLOAI);
                                    this.DbContext.SubmitChanges();
                                }
                            }

                            if (!String.IsNullOrEmpty(mcl))
                            {
                                //kieu 
                                List<DM_KIEU> aTemp_k = aList_k.Where(k => k.MA_KIEU == mkieu).ToList();
                                if (aTemp_k.Count > 0)
                                {
                                    aDM_KIEU = aTemp_k[0];
                                    aDM_KIEU.MA_CL = mcl;
                                    aDM_KIEU.TEN_KIEU = tkieu;
                                    aDM_KIEU.GHICHU = gc;
                                    this.DbContext.SubmitChanges();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(mcl) && mkieu > 0)
                                    {
                                        aDM_KIEU = new DM_KIEU();
                                        aDM_KIEU.MA_CL = mcl;
                                        aDM_KIEU.MA_KIEU = mkieu;
                                        aDM_KIEU.TEN_KIEU = tkieu;
                                        aDM_KIEU.GHICHU = gc;
                                        this.DbContext.DM_KIEUs.InsertOnSubmit(aDM_KIEU);
                                        this.DbContext.SubmitChanges();
                                    }
                                }
                            }
                        }
                        this.DbContext.SubmitChanges();
                    }
                    return response;
                }
                else
                {
                    response.Success = false;

                    response.ExtraParams["msg"] = "Vui lòng chỉ chọn file có đuôi mở rộng là ( .xlsx, .xsl, .xls ) !";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
                return response;
            }

        }

        public AjaxFormResult saveKieu_ThietBi(FormCollection values)
        {
            DM_KIEU dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                int mkieu = String.IsNullOrEmpty(values["MA_KIEU_KIEU"]) ? 0 : Convert.ToInt32(values["MA_KIEU_KIEU"]);
                List<DM_KIEU> aList = this.DbContext.DM_KIEUs.Where(k => k.MA_KIEU == mkieu).ToList();
                if (string.IsNullOrEmpty(values["ID_MA_KIEU"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã kiểu đã tồn tại vui lòng nhập mã kiểu khác.";
                    }
                    else
                    {
                        if (mkieu <= 0)
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Đã có lỗi xẩy ra vui lòng kiểm tra thông tin nhập hoặc báo lại cho người quản lý.";
                        }
                        else
                        {
                            dm = new DM_KIEU();
                            dm.MA_KIEU = mkieu;
                            dm.TEN_KIEU = values["TEN_KIEU"];
                            dm.MA_CL = values["MA_CL_KIEU"];
                            dm.GHICHU = values["GHI_CHU_TB"];
                            this.DbContext.DM_KIEUs.InsertOnSubmit(dm);
                            this.DbContext.SubmitChanges();
                            response.ExtraParams["MA_KIEU"] = values["MA_KIEU_KIEU"];
                        }
                    }
                }
                else
                {
                    dm = aList[0];
                    dm.TEN_KIEU = values["TEN_KIEU"];
                    dm.MA_CL = values["MA_CL_KIEU"];
                    dm.GHICHU = values["GHI_CHU_TB"];
                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaKieu(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_KIEUs
                          where c.MA_KIEU == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_KIEUs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Danh mục Trạng Thái"

        public ActionResult Dm_Trangthai()
        {
            return View();
        }

        public AjaxStoreResult dsTrangthai(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            var query = (from c in this.DbContext.DM_TRANGTHAIs
                         where SqlMethods.Like(c.TEN_TT, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                         orderby c.MA_TT ascending
                         select new
                         {
                             c.MA_TT,
                             c.TEN_TT,
                             c.GHICHU
                         });
            int total = query.ToList().Count;

            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveTrangthai(FormCollection values)
        {
            DM_TRANGTHAI dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                int matt = String.IsNullOrEmpty(values["MA_TT"]) ? 0 : Convert.ToInt32(values["MA_TT"]);
                List<DM_TRANGTHAI> aList = this.DbContext.DM_TRANGTHAIs.Where(tt => tt.MA_TT == matt).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã trạng thái đã tồn tại, vui lòng nhập mã khác";
                    }
                    else
                    {
                        if (matt > 0)
                        {
                            dm = new DM_TRANGTHAI();
                            dm.MA_TT = matt;
                            dm.TEN_TT = values["TEN_TT"];
                            dm.GHICHU = values["GHICHU"];
                            this.DbContext.DM_TRANGTHAIs.InsertOnSubmit(dm);
                            this.DbContext.SubmitChanges();
                        }
                        else
                        {
                            response.Success = false;
                            response.ExtraParams["msg"] = "Đã có lỗi xẩy ra vui lòng kiểm tra thông tin nhập hoặc báo lại cho người quản lý.";
                        }
                    }

                }
                else
                {
                    dm = aList[0];
                    dm.TEN_TT = values["TEN_TT"];
                    dm.GHICHU = values["GHICHU"];
                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaTrangthai(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DM_TRANGTHAIs
                          where c.MA_TT == Convert.ToInt32(id)
                          select c).First();
                this.DbContext.DM_TRANGTHAIs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Nhập kho thiết bị"

        public ActionResult Nhap_kho()
        {
            return View();
        }

        public ActionResult NK_ThuHoi()
        {
            return View();
        }
        public AjaxStoreResult dsNhapKho(string txtfilter, int start, int limit, int loai)
        {
            /*
             * Ghu chu: Loai =1 : Nhap kho them moi thiet bi
             * Ghi chu: Loai =2 : Nhap kho thu hoi thiet bi
             */

            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            loai = String.IsNullOrEmpty(Convert.ToString(loai)) ? 0 : loai;

            var query = (from nk in this.DbContext.DP_NHAPKHOs
                         join hd in this.DbContext.DM_HOPDONGs on nk.MA_HD equals hd.MA_HD
                         join nkp in this.DbContext.DM_NGUON_KINH_PHIs on nk.MA_NKP equals nkp.MA_NKP
                         join dvbh in this.DbContext.DM_DON_VI_BAO_HANHs on nk.MA_DVBH equals dvbh.MA_DVBH
                         join nv in this.DbContext.NHANVIENs on nk.MA_NV equals nv.MA_NV
                         where (nk.MA_NK.Contains(txtfilter) || nv.MA_NV.Contains(txtfilter)) && nk.LOAI == loai

                         select new
                         {
                             nk.MA_NK,
                             nk.MA_HD,
                             nk.MA_NKP,
                             nk.MA_DVBH,
                             nk.MA_NV,
                             nk.NGAY_NHAP,
                             nk.NGAY_SUA,
                             nk.CHAT_LUONG,
                             nk.MO_TA,
                             nk.GHI_CHU,
                             hd.TEN_HD,
                             hd.NGAY_BD,
                             hd.NGAY_KT,
                             nkp.TEN_NKP,
                             dvbh.TEN_DVBH,
                             nv.TEN_NV
                         });

            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsCT_Nhapkho(string txtfilter)
        {
            var query = (from c in this.DbContext.CT_NHAPKHOs
                         join d in this.DbContext.DP_NHAPKHOs on c.MA_NK equals d.MA_NK
                         join e in this.DbContext.DM_THIET_BIs on c.MA_TB equals e.MA_TB
                         join f in this.DbContext.DM_CHUNGLOAIs on e.MA_CL equals f.MA_CL
                         join g in this.DbContext.DM_KIEUs on e.MA_KIEU equals g.MA_KIEU
                         orderby c.MA_NK descending
                         where c.MA_NK == txtfilter
                         select new
                         {
                             c.MA_NK,
                             c.MA_TB,
                             e.TEN_TB,
                             f.TEN_CL,
                             g.TEN_KIEU,
                             e.NGAY_BH,
                             e.THOI_GIAN_BH,

                             e.SERIAL,
                             e.NGAY_KH,
                             e.THOI_GIAN_KH,
                             e.NGUYEN_GIA
                         });
            int total = query.ToList().Count;

            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsCT_Xuatkho(string txtfilter)
        {
            var query = (from e in this.DbContext.DM_THIET_BIs
                         join f in this.DbContext.DM_CHUNGLOAIs on e.MA_CL equals f.MA_CL
                         join g in this.DbContext.DM_KIEUs on e.MA_KIEU equals g.MA_KIEU
                         where e.MA_TB == txtfilter
                         select new
                         {
                             e.MA_TB,
                             e.TEN_TB,
                             f.TEN_CL,
                             g.TEN_KIEU,
                             e.NGAY_BH,
                             e.THOI_GIAN_BH,
                             e.SERIAL,
                             e.NGAY_KH,
                             e.THOI_GIAN_KH,
                             e.NGUYEN_GIA
                         });
            int total = query.ToList().Count;

            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveNhapkho(FormCollection values, string json)
        {
            DP_NHAPKHO aDP_NHAPKHO;
            CT_NHAPKHO aCT_NHAPKHO;
            AjaxFormResult response = new AjaxFormResult();
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);


            try
            {

                List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                List<DP_NHAPKHO> aList = new List<DP_NHAPKHO>();
                aList = this.DbContext.DP_NHAPKHOs.Where(nk => nk.MA_NK.Equals(values["MA_NK"])).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    string maNK = values["MA_NK"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maNK = DungChung.TaoMaTuDong("NK");
                            aList = this.DbContext.DP_NHAPKHOs.Where(nk => nk.MA_NK.Equals(maNK)).ToList();
                        }
                    }
                    aDP_NHAPKHO = new DP_NHAPKHO();
                    aDP_NHAPKHO.MA_NK = maNK;
                    aDP_NHAPKHO.MA_HD = values["MA_HD_Value"];
                    aDP_NHAPKHO.MA_NKP = values["MA_NKP_Value"];
                    aDP_NHAPKHO.MA_DVBH = values["MA_DVBH_Value"];
                    string MaNV = string.Empty;
                    if (Request.Cookies["remember"] != null)
                    {
                        MaNV = Request.Cookies["remember"].Values.Get(2);
                    }
                    else
                    {
                        MaNV = string.Empty;
                    }
                    aDP_NHAPKHO.MA_NV = MaNV;

                    aDP_NHAPKHO.NGAY_NHAP = DateTime.Now;
                    //aDP_NHAPKHO.NGAY_XUAT = values[""];

                    aDP_NHAPKHO.CHAT_LUONG = String.IsNullOrEmpty(values["CHAT_LUONG_Value"]) ? 0 : Convert.ToInt32(values["CHAT_LUONG_Value"]);

                    aDP_NHAPKHO.LOAI = 1;
                    aDP_NHAPKHO.MO_TA = string.Empty;
                    aDP_NHAPKHO.GHI_CHU = string.Empty;
                    aDP_NHAPKHO.NGAY_TAO = DateTime.Now;
                    aDP_NHAPKHO.NGAY_SUA = DateTime.Now;
                    this.DbContext.DP_NHAPKHOs.InsertOnSubmit(aDP_NHAPKHO);
                    this.DbContext.SubmitChanges();


                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {

                        string MA_TB = row["MA_TB"];
                        List<DM_THIET_BI> aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();

                        if (aListTemp.Count > 0)
                        {
                            aDM_THIET_BI = aListTemp[0];
                            aDM_THIET_BI.TRANG_THAI = 2;
                            this.DbContext.SubmitChanges();
                        }
                        aCT_NHAPKHO = new CT_NHAPKHO();
                        aCT_NHAPKHO.MA_NK = maNK;
                        aCT_NHAPKHO.MA_TB = MA_TB;
                        aCT_NHAPKHO.MO_TA = string.Empty;
                        aCT_NHAPKHO.GHI_CHU = string.Empty;
                        aCT_NHAPKHO.NGAY_TAO = DateTime.Now;
                        aCT_NHAPKHO.NGAY_SUA = DateTime.Now;
                        this.DbContext.CT_NHAPKHOs.InsertOnSubmit(aCT_NHAPKHO);
                        this.DbContext.SubmitChanges();
                    }




                }
                else
                {
                    if (aList.Count > 0)
                    {
                        aDP_NHAPKHO = aList[0];
                        aDP_NHAPKHO.MA_HD = values["MA_HD_Value"];
                        aDP_NHAPKHO.MA_NKP = values["MA_NKP_Value"];
                        aDP_NHAPKHO.MA_DVBH = values["MA_DVBH_Value"];
                        string MaNV = string.Empty;
                        if (Request.Cookies["remember"] != null)
                        {
                            MaNV = Request.Cookies["remember"].Values.Get(2);
                        }
                        else
                        {
                            MaNV = string.Empty;
                        }
                        aDP_NHAPKHO.MA_NV = MaNV;

                        aDP_NHAPKHO.NGAY_NHAP = DateTime.Now;
                        //aDP_NHAPKHO.NGAY_XUAT = values[""];

                        aDP_NHAPKHO.CHAT_LUONG = String.IsNullOrEmpty(values["CHAT_LUONG_Value"]) ? 0 : Convert.ToInt32(values["CHAT_LUONG_Value"]);
                        aDP_NHAPKHO.LOAI = 1;
                        aDP_NHAPKHO.MO_TA = string.Empty;
                        aDP_NHAPKHO.GHI_CHU = string.Empty;
                        aDP_NHAPKHO.NGAY_SUA = DateTime.Now;
                        this.DbContext.SubmitChanges();

                        List<DM_THIET_BI> aListTemp = new List<DM_THIET_BI>();
                        List<CT_NHAPKHO> dsChiTietNhapKho = this.DbContext.CT_NHAPKHOs.Where(ct => ct.MA_NK.Equals(values["MA_NK"])).ToList();
                        foreach (CT_NHAPKHO ctnk in dsChiTietNhapKho)
                        {
                            string MA_TB = ctnk.MA_TB;
                            aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();
                            if (aListTemp.Count > 0)
                            {
                                aDM_THIET_BI = aListTemp[0];
                                aDM_THIET_BI.TRANG_THAI = 1;
                            }

                            this.DbContext.CT_NHAPKHOs.DeleteOnSubmit(ctnk);
                            this.DbContext.SubmitChanges();
                        }

                        foreach (Dictionary<string, string> row in chitiet_ts)
                        {

                            string MA_TB = row["MA_TB"];
                            aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();

                            if (aListTemp.Count > 0)
                            {
                                aDM_THIET_BI = aListTemp[0];
                                aDM_THIET_BI.TRANG_THAI = 2;
                                this.DbContext.SubmitChanges();
                            }
                            aCT_NHAPKHO = new CT_NHAPKHO();
                            aCT_NHAPKHO.MA_NK = values["MA_NK"];
                            aCT_NHAPKHO.MA_TB = MA_TB;
                            aCT_NHAPKHO.MO_TA = string.Empty;
                            aCT_NHAPKHO.GHI_CHU = string.Empty;
                            aCT_NHAPKHO.NGAY_TAO = DateTime.Now;
                            aCT_NHAPKHO.NGAY_SUA = DateTime.Now;
                            this.DbContext.CT_NHAPKHOs.InsertOnSubmit(aCT_NHAPKHO);
                            this.DbContext.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaNhapkho(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var nv = (from c in this.DbContext.DP_NHAPKHOs
                          where c.MA_NK == id
                          select c).First();
                this.DbContext.DP_NHAPKHOs.DeleteOnSubmit(nv);
                this.DbContext.SubmitChanges();

                var query = from p in this.DbContext.CT_NHAPKHOs
                            where p.MA_NK == id
                            select p;
                foreach (var s in query)
                {
                    this.DbContext.CT_NHAPKHOs.DeleteOnSubmit(s);
                }
                this.DbContext.SubmitChanges();


            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }



        #endregion

        #region "Xuất kho thiết bị"

        public ActionResult Xuat_kho()
        {
            return View();
        }


        public AjaxFormResult saveXuatkho(FormCollection values, string json, string MA_PYC)
        {
            XUATKHO_TB aXUATKHO_TB;
            AjaxFormResult response = new AjaxFormResult();
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);


            try
            {

                List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                List<XUATKHO_TB> aList = new List<XUATKHO_TB>();
                aList = this.DbContext.XUATKHO_TBs.Where(xk => xk.MA_XK.Equals(values["MA_XK"])).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    string maXK = values["MA_XK"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maXK = DungChung.TaoMaTuDong("XKM");
                            aList = this.DbContext.XUATKHO_TBs.Where(xk => xk.MA_XK.Equals(maXK)).ToList();
                        }
                    }

                    List<PHIEU_YEU_CAU> apyc = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(MA_PYC)).ToList();
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU;
                    if (apyc.Count > 0)
                    {
                        aPHIEU_YEU_CAU = apyc[0];
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.KET_THUC; //Ket thuc yeu cau
                        this.DbContext.SubmitChanges();
                    }

                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {

                        string MA_TB = row["MA_TB"];
                        List<DM_THIET_BI> aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();

                        if (aListTemp.Count > 0)
                        {
                            aDM_THIET_BI = aListTemp[0];
                            aDM_THIET_BI.TRANG_THAI = 4; // Thiết bị đang được sử dụng
                            this.DbContext.SubmitChanges();
                        }
                        aXUATKHO_TB = new XUATKHO_TB();
                        aXUATKHO_TB.ID = Guid.NewGuid().ToString();
                        aXUATKHO_TB.MA_XK = maXK;
                        aXUATKHO_TB.MA_TB = row["MA_TB"];
                        aXUATKHO_TB.MA_PYC = MA_PYC;
                        if (!String.IsNullOrEmpty(values["NGAY_XK"]))
                        {
                            aXUATKHO_TB.NGAY_XK = Convert.ToDateTime(values["NGAY_XK"]);
                        }
                        aXUATKHO_TB.GHI_CHU = values["GHI_CHU"];
                        aXUATKHO_TB.TRANG_THAI = 1;
                        string MaNV = string.Empty;
                        if (Request.Cookies["remember"] != null)
                        {
                            MaNV = Request.Cookies["remember"].Values.Get(2);
                        }
                        else
                        {
                            MaNV = string.Empty;
                        }
                        aXUATKHO_TB.NGUOI_XUAT_KHO = MaNV;
                        this.DbContext.XUATKHO_TBs.InsertOnSubmit(aXUATKHO_TB);
                        this.DbContext.SubmitChanges();
                    }



                }
                else
                {
                    if (aList.Count > 0)
                    {


                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }


        public AjaxFormResult saveXuatkho_ThanhLy(FormCollection values, string json, string MA_PYC)
        {
            XUATKHO_TB aXUATKHO_TB;
            AjaxFormResult response = new AjaxFormResult();
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);


            try
            {
                List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                List<XUATKHO_TB> aList = new List<XUATKHO_TB>();
                aList = this.DbContext.XUATKHO_TBs.Where(xk => xk.MA_XK.Equals(values["MA_XK"])).ToList();
                if (string.IsNullOrEmpty(values["id"]))
                {
                    string maXK = values["MA_XK"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maXK = DungChung.TaoMaTuDong("XKTL");
                            aList = this.DbContext.XUATKHO_TBs.Where(xk => xk.MA_XK.Equals(maXK)).ToList();
                        }
                    }

                    List<PHIEU_YEU_CAU> apyc = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(MA_PYC)).ToList();
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU;
                    if (apyc.Count > 0)
                    {
                        aPHIEU_YEU_CAU = apyc[0];
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.KET_THUC; //Hoan thanh yeu cau
                        this.DbContext.SubmitChanges();
                    }

                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {

                        string MA_TB = row["MA_TB"];
                        List<DM_THIET_BI> aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();

                        if (aListTemp.Count > 0)
                        {
                            aDM_THIET_BI = aListTemp[0];
                            aDM_THIET_BI.TRANG_THAI = 8; // Thiết bị đã thanh lý
                            this.DbContext.SubmitChanges();
                        }
                        aXUATKHO_TB = new XUATKHO_TB();
                        aXUATKHO_TB.ID = Guid.NewGuid().ToString();
                        aXUATKHO_TB.MA_XK = values["MA_XK_TL"];
                        aXUATKHO_TB.MA_TB = row["MA_TB"];
                        aXUATKHO_TB.MA_PYC = MA_PYC;
                        if (!String.IsNullOrEmpty(values["NGAY_XK_TL"]))
                        {
                            aXUATKHO_TB.NGAY_XK = Convert.ToDateTime(values["NGAY_XK_TL"]);
                        }
                        aXUATKHO_TB.GHI_CHU = values["GHI_CHU_TL"];
                        aXUATKHO_TB.TRANG_THAI = 1;
                        string MaNV = string.Empty;
                        if (Request.Cookies["remember"] != null)
                        {
                            MaNV = Request.Cookies["remember"].Values.Get(2);
                        }
                        else
                        {
                            MaNV = string.Empty;
                        }
                        aXUATKHO_TB.NGUOI_XUAT_KHO = MaNV;
                        this.DbContext.XUATKHO_TBs.InsertOnSubmit(aXUATKHO_TB);
                        this.DbContext.SubmitChanges();
                    }



                }
                else
                {
                    if (aList.Count > 0)
                    {


                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        #endregion

        #region "Thanh lý thiết bị"

        public ActionResult Thanh_ly()
        {
            return View();
        }

        #endregion

        #region  danh mục thiết bị
        public ActionResult Dm_Thietbi()
        {
            return View();
        }

        public AjaxStoreResult TimKiemThietBi_TrangThai(string trangthai)
        {

            string[] ast = trangthai.Split(',');
            List<int> li = new List<int>();
            for (int i = 0; i < ast.Count(); i++)
            {
                li.Add(Convert.ToInt32(ast[i]));
            }
            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         where li.Contains(Convert.ToInt32(tb.TRANG_THAI))
                         select new
                         {
                             tb.MA_TB,
                             tb.SERIAL,
                             tb.TEN_TB,
                             tb.MA_CL,
                             tb.MA_KIEU,
                             tb.NGAY_BH,
                             tb.THOI_GIAN_BH,
                             tb.NGAY_KH,
                             tb.THOI_GIAN_KH,
                             tb.TRANG_THAI,
                             tb.NGAY_TAO,
                             tb.NGAY_SUA,
                             tb.NGUYEN_GIA,
                             cl.TEN_CL,
                             k.TEN_KIEU
                         });
            int total = query.ToList().Count;
            query = query.Skip(0).Take(total);
            return new AjaxStoreResult(query, total);
        }


        public AjaxStoreResult TimKiemThietBi_NSD(string MA_NSD)
        {
            MA_NSD = String.IsNullOrEmpty(MA_NSD) ? string.Empty : MA_NSD;
            var query = (from xk in this.DbContext.XUATKHO_TBs
                         join yc in this.DbContext.PHIEUYEUCAU_THIETBIs on xk.MA_PYC equals yc.MA_PYC
                         join tb in this.DbContext.DM_THIET_BIs on xk.MA_TB equals tb.MA_TB
                         where yc.MA_NV.Equals(MA_NSD) && tb.TRANG_THAI == 4
                         select new
                         {
                             tb.MA_TB,
                             tb.TEN_TB,
                             tb.SERIAL,
                             tb.MA_CL,
                             tb.MA_KIEU
                         });
            int total = query.ToList().Count;
            query = query.Skip(0).Take(total);
            return new AjaxStoreResult(query, total);
        }

        [HttpPost]
        public JsonResult TimKiemThietBiTheoSerial(string serial)
        {
            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         where tb.SERIAL.Equals(serial)
                         select new
                         {
                             tb.MA_CL,
                             tb.MA_KIEU,
                             cl.TEN_CL,
                             k.TEN_KIEU
                         });
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public AjaxStoreResult dsThietBi(string txtfilter, int start, int limit, string trangthai)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            int trang_thai = String.IsNullOrEmpty(trangthai) ? 0 : Convert.ToInt32(trangthai);

            if (trang_thai == 0)
            {
                var query = (from tb in this.DbContext.DM_THIET_BIs
                             join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                             join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                             join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                             where (SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%") || tb.SERIAL.Equals(txtfilter))
                             orderby tb.MA_CL ascending
                             select new
                             {
                                 MA_TB = tb.MA_TB,
                                 SERIAL = tb.SERIAL,
                                 TEN_TB = tb.TEN_TB,
                                 MA_CL = tb.MA_CL,
                                 MA_KIEU = tb.MA_KIEU,
                                 NGAY_BH = tb.NGAY_BH,
                                 THOI_GIAN_BH = tb.THOI_GIAN_BH,
                                 THOI_GIAN_BHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_BH), Convert.ToInt32(tb.THOI_GIAN_BH)),

                                 NGAY_KH = tb.NGAY_KH,
                                 THOI_GIAN_KH = tb.THOI_GIAN_KH,
                                 THOI_GIAN_KHCL = DungChung.TinhThoiGianKhauHao(Convert.ToDateTime(tb.NGAY_KH), Convert.ToInt32(tb.THOI_GIAN_KH)),
                                 TRANG_THAI = tb.TRANG_THAI,
                                 NGAY_TAO = tb.NGAY_TAO,
                                 NGAY_SUA = tb.NGAY_SUA,
                                 NGUYEN_GIA = tb.NGUYEN_GIA,
                                 TEN_CL = cl.TEN_CL,
                                 TEN_KIEU = k.TEN_KIEU,
                                 TEN_TT = tt.TEN_TT
                             });
                int total = query.ToList().Count;
                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
            else
            {
                var query = (from tb in this.DbContext.DM_THIET_BIs
                             join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                             join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                             join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                             where (SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%") || tb.SERIAL.Equals(txtfilter)) && tb.TRANG_THAI == trang_thai
                             orderby tb.MA_CL ascending
                             select new
                             {
                                 MA_TB = tb.MA_TB,
                                 SERIAL = tb.SERIAL,
                                 TEN_TB = tb.TEN_TB,
                                 MA_CL = tb.MA_CL,
                                 MA_KIEU = tb.MA_KIEU,
                                 NGAY_BH = tb.NGAY_BH,
                                 THOI_GIAN_BH = tb.THOI_GIAN_BH,
                                 THOI_GIAN_BHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_BH), Convert.ToInt32(tb.THOI_GIAN_BH)),

                                 NGAY_KH = tb.NGAY_KH,
                                 THOI_GIAN_KH = tb.THOI_GIAN_KH,
                                 THOI_GIAN_KHCL = DungChung.TinhThoiGianKhauHao(Convert.ToDateTime(tb.NGAY_KH), Convert.ToInt32(tb.THOI_GIAN_KH)),
                                 TRANG_THAI = tb.TRANG_THAI,
                                 NGAY_TAO = tb.NGAY_TAO,
                                 NGAY_SUA = tb.NGAY_SUA,
                                 NGUYEN_GIA = tb.NGUYEN_GIA,
                                 TEN_CL = cl.TEN_CL,
                                 TEN_KIEU = k.TEN_KIEU,
                                 TEN_TT = tt.TEN_TT
                             });
                int total = query.ToList().Count;
                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
        }


        public AjaxStoreResult TimKiemThietBiTheoMaNhapKho(string txtfilter, int start, int limit, string maNK)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            maNK = String.IsNullOrEmpty(maNK) ? string.Empty : maNK;
            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join nk_tb in this.DbContext.CT_NHAPKHOs on tb.MA_TB equals nk_tb.MA_TB
                         join nk in this.DbContext.DP_NHAPKHOs on nk_tb.MA_NK equals nk.MA_NK
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                         where (SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%") || tb.SERIAL.Equals(txtfilter)) && nk.MA_NK == maNK
                         orderby tb.MA_CL ascending
                         select new
                         {

                             tb.MA_TB,
                             tb.SERIAL,
                             tb.TEN_TB,
                             tb.MA_CL,
                             tb.MA_KIEU,
                             tb.NGAY_BH,
                             tb.THOI_GIAN_BH,
                             tb.NGAY_KH,
                             tb.THOI_GIAN_KH,
                             tb.TRANG_THAI,
                             tb.NGAY_TAO,
                             tb.NGAY_SUA,
                             tb.NGUYEN_GIA,
                             cl.TEN_CL,
                             k.TEN_KIEU,
                             tt.TEN_TT,
                             nk_tb.MA_CT,
                             nk.MA_NK
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }


        public AjaxStoreResult TimKiemThietBiTheoMaXuatKho(string txtfilter, int start, int limit, string maXK, string maPYC)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            maXK = String.IsNullOrEmpty(maXK) ? string.Empty : maXK;
            maPYC = String.IsNullOrEmpty(maPYC) ? string.Empty : maPYC;

            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join xk in this.DbContext.XUATKHO_TBs on tb.MA_TB equals xk.MA_TB
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                         where (SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%") || tb.SERIAL.Equals(txtfilter)) && xk.MA_XK == maXK
                         select new
                         {

                             MA_TB = tb.MA_TB,
                             SERIAL = tb.SERIAL,
                             TEN_TB = tb.TEN_TB,
                             MA_CL = tb.MA_CL,
                             MA_KIEU = tb.MA_KIEU,
                             NGAY_BH = tb.NGAY_BH,
                             THOI_GIAN_BH = tb.THOI_GIAN_BH,
                             NGAY_KH = tb.NGAY_KH,
                             THOI_GIAN_KH = tb.THOI_GIAN_KH,
                             TRANG_THAI = tb.TRANG_THAI,
                             NGAY_TAO = tb.NGAY_TAO,
                             NGAY_SUA = tb.NGAY_SUA,
                             NGUYEN_GIA = tb.NGUYEN_GIA,
                             TEN_CL = cl.TEN_CL,
                             TEN_KIEU = k.TEN_KIEU,
                             TEN_TT = tt.TEN_TT,
                             MA_XK = xk.MA_XK
                         });
            string mCL = string.Empty;
            int mK = 0;
            int sL = 0;
            List<PHIEUYEUCAU_THIETBI> aList = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(p => p.MA_PYC.Equals(maPYC)).ToList();
            if (aList.Count > 0)
            {
                mCL = aList[0].MA_CL;
                mK = Convert.ToInt32(aList[0].MA_KIEU);
                sL = Convert.ToInt32(aList[0].SO_LUONG);
            }
            var query1 = (from tb in this.DbContext.DM_THIET_BIs
                          join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                          join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                          join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                          where tb.MA_CL.Equals(mCL) && tb.MA_KIEU == mK && tb.TRANG_THAI == 3
                          select new
                          {

                              MA_TB = tb.MA_TB,
                              SERIAL = tb.SERIAL,
                              TEN_TB = tb.TEN_TB,
                              MA_CL = tb.MA_CL,
                              MA_KIEU = tb.MA_KIEU,
                              NGAY_BH = tb.NGAY_BH,
                              THOI_GIAN_BH = tb.THOI_GIAN_BH,
                              NGAY_KH = tb.NGAY_KH,
                              THOI_GIAN_KH = tb.THOI_GIAN_KH,
                              TRANG_THAI = tb.TRANG_THAI,
                              NGAY_TAO = tb.NGAY_TAO,
                              NGAY_SUA = tb.NGAY_SUA,
                              NGUYEN_GIA = tb.NGUYEN_GIA,
                              TEN_CL = cl.TEN_CL,
                              TEN_KIEU = k.TEN_KIEU,
                              TEN_TT = tt.TEN_TT,
                              MA_XK = ""
                          });

            var ds = query.Union(query1).OrderBy(o => o.MA_CL).Take(sL).ToList();

            int total = ds.Count;
            ds = ds.Skip(start).Take(limit).ToList();
            return new AjaxStoreResult(ds, total);
        }


        public AjaxStoreResult TimKiemTheoMaYeuCau(string maYC)
        {
            maYC = String.IsNullOrEmpty(maYC) ? string.Empty : maYC;
            var query = (from yc in this.DbContext.PHIEUYEUCAU_THIETBIs
                         join tb in this.DbContext.DM_THIET_BIs on yc.SERIAL equals tb.SERIAL
                         join cl in this.DbContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                         join nv in this.DbContext.NHANVIENs on yc.MA_NV equals nv.MA_NV
                         where yc.MA_PYC.Equals(maYC)
                         select new
                         {
                             yc.MA_PYC,
                             tb.MA_TB,
                             tb.SERIAL,
                             tb.TEN_TB,
                             cl.MA_CL,
                             cl.TEN_CL,
                             k.MA_KIEU,
                             k.TEN_KIEU,
                             nv.MA_NV,
                             nv.TEN_NV
                         });
            int total = query.ToList().Count;
            query = query.Skip(0).Take(total);
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsThietBi_TT(string ma_kieu, string ma_cl, string ma_tt)
        {
            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         where SqlMethods.Like(tb.MA_CL, "%" + LIB.ProcessStrVal(ma_cl) + "%")
                         || SqlMethods.Like(tb.MA_KIEU.ToString(), "%" + LIB.ProcessStrVal(ma_kieu) + "%")
                         orderby tb.MA_CL ascending
                         select new
                         {
                             tb.MA_TB,
                             tb.SERIAL,
                             tb.TEN_TB,
                             tb.MA_CL,
                             tb.MA_KIEU,
                             tb.NGAY_BH,
                             tb.THOI_GIAN_BH,
                             tb.NGAY_KH,
                             tb.THOI_GIAN_KH,
                             tb.TRANG_THAI,
                             tb.NGAY_TAO,
                             tb.NGAY_SUA,
                             cl.TEN_CL,
                             k.TEN_KIEU,
                             tb.NGUYEN_GIA
                         });
            int total = query.ToList().Count;
            return new AjaxStoreResult(query, total);
        }

        public AjaxStoreResult dsThietBi_XK(string ma_kieu, string ma_cl, string serial)
        {
            var query = (from tb in this.DbContext.DM_THIET_BIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         where SqlMethods.Like(tb.MA_CL, "%" + LIB.ProcessStrVal(ma_cl) + "%")
                         || SqlMethods.Like(tb.MA_KIEU.ToString(), "%" + LIB.ProcessStrVal(ma_kieu) + "%")
                         || SqlMethods.Like(tb.SERIAL.ToString(), "%" + LIB.ProcessStrVal(serial) + "%")
                         orderby tb.MA_CL ascending
                         select new
                         {
                             tb.MA_TB,
                             tb.SERIAL,
                             tb.TEN_TB,
                             tb.MA_CL,
                             tb.MA_KIEU,
                             tb.NGAY_BH,
                             tb.THOI_GIAN_BH,
                             tb.NGAY_KH,
                             tb.THOI_GIAN_KH,
                             tb.TRANG_THAI,
                             tb.NGAY_TAO,
                             tb.NGAY_SUA,
                             cl.TEN_CL,
                             k.TEN_KIEU,
                             tb.NGUYEN_GIA
                         });
            int total = query.ToList().Count;
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveThietBi(FormCollection values)
        {
            DM_THIET_BI dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_THIET_BI> aList = (from tb in this.DbContext.DM_THIET_BIs
                                           where tb.MA_TB.Equals(values["MA_TB"])
                                           select tb).ToList();
                if (String.IsNullOrEmpty(values["ID_MA_TB"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã thiết bị đã tồn tại, vui lòng nhập mã khác !";
                    }
                    else
                    {
                        dm = new DM_THIET_BI();
                        dm.MA_TB = values["MA_TB"];
                        dm.SERIAL = values["SERIAL"];
                        dm.TEN_TB = values["TEN_TB"];
                        dm.MA_CL = values["MA_CL_Value"];
                        dm.MA_KIEU = String.IsNullOrEmpty(values["MA_KIEU_Value"]) ? 0 : Convert.ToInt32(values["MA_KIEU_Value"]);
                        if (!String.IsNullOrEmpty(values["NGAY_BH"]))
                        {
                            dm.NGAY_BH = Convert.ToDateTime(values["NGAY_BH"]);
                        }
                        dm.THOI_GIAN_BH = String.IsNullOrEmpty(values["THOI_GIAN_BH"]) ? 0 : Convert.ToInt32(values["THOI_GIAN_BH"]);
                        if (!String.IsNullOrEmpty(values["NGAY_KH"]))
                        {
                            dm.NGAY_KH = Convert.ToDateTime(values["NGAY_KH"]);
                        }
                        dm.THOI_GIAN_KH = String.IsNullOrEmpty(values["THOI_GIAN_KH"]) ? 0 : Convert.ToInt32(values["THOI_GIAN_KH"]);

                        dm.TRANG_THAI = String.IsNullOrEmpty(values["TRANG_THAI_Value"]) ? 0 : Convert.ToInt32(values["TRANG_THAI_Value"]);
                        dm.NGAY_TAO = DateTime.Now;
                        dm.NGAY_SUA = DateTime.Now;
                        dm.MA_NV = string.Empty;

                        dm.NGUYEN_GIA = String.IsNullOrEmpty(values["NGUYEN_GIA"]) ? 0 : Convert.ToDecimal(values["NGUYEN_GIA"].Replace(",", "").Replace(".", ""));

                        this.DbContext.DM_THIET_BIs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.SERIAL = values["SERIAL"];
                        dm.TEN_TB = values["TEN_TB"];
                        dm.MA_CL = values["MA_CL_Value"];
                        dm.MA_KIEU = Convert.ToInt32(values["MA_KIEU_Value"]);
                        if (!String.IsNullOrEmpty(values["NGAY_BH"]))
                        {
                            dm.NGAY_BH = Convert.ToDateTime(values["NGAY_BH"]);
                        }
                        dm.THOI_GIAN_BH = String.IsNullOrEmpty(values["THOI_GIAN_BH"]) ? 0 : Convert.ToInt32(values["THOI_GIAN_BH"]);
                        if (!String.IsNullOrEmpty(values["NGAY_KH"]))
                        {
                            dm.NGAY_KH = Convert.ToDateTime(values["NGAY_KH"]);
                        }
                        dm.THOI_GIAN_KH = String.IsNullOrEmpty(values["THOI_GIAN_KH"]) ? 0 : Convert.ToInt32(values["THOI_GIAN_KH"]);
                        dm.TRANG_THAI = Convert.ToInt32(values["TRANG_THAI_Value"]);
                        dm.NGAY_SUA = DateTime.Now;
                        dm.MA_NV = string.Empty;
                        dm.NGUYEN_GIA = String.IsNullOrEmpty(values["NGUYEN_GIA"]) ? 0 : Convert.ToDecimal(values["NGUYEN_GIA"].Replace(",", "").Replace(".", ""));

                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxFormResult save_ThietBi_Ex(FormCollection values)
        {
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_CHUNGLOAI> aList_cl = this.DbContext.DM_CHUNGLOAIs.ToList();
                List<DM_KIEU> aList_k = this.DbContext.DM_KIEUs.ToList();
                List<DM_THIET_BI> aList_tb = this.DbContext.DM_THIET_BIs.ToList();

                HttpPostedFileBase postedFile = Request.Files["TEN_FILE"];
                string filename = postedFile.FileName;

                if (postedFile.ContentLength != 0 && (Path.GetExtension(filename) == ".xlsx" || Path.GetExtension(filename) == ".xsl" || Path.GetExtension(filename) == ".xls"))
                {
                    using (var excel = new ExcelPackage(postedFile.InputStream))
                    {
                        DM_CHUNGLOAI aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                        DM_KIEU aDM_KIEU = new DM_KIEU();
                        DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                        var worksheet = excel.Workbook.Worksheets.First();
                        for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                        {

                            string mcl = Convert.ToString(worksheet.Cells[row, 4].Value);
                            long mkieu = Convert.ToInt32(worksheet.Cells[row, 5].Value);

                            #region chung loai
                            List<DM_CHUNGLOAI> aTemp = aList_cl.Where(cl => cl.MA_CL.Equals(mcl)).ToList();
                            if (aTemp.Count > 0)
                            {
                                aDM_CHUNGLOAI = aTemp[0];
                                this.DbContext.SubmitChanges();
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(mcl))
                                {
                                    aDM_CHUNGLOAI = new DM_CHUNGLOAI();
                                    aDM_CHUNGLOAI.MA_CL = mcl;
                                    this.DbContext.DM_CHUNGLOAIs.InsertOnSubmit(aDM_CHUNGLOAI);
                                    this.DbContext.SubmitChanges();
                                }
                            }
                            #endregion

                            #region kieu
                            if (!String.IsNullOrEmpty(mcl))
                            {
                                //kieu 
                                List<DM_KIEU> aTemp_k = aList_k.Where(k => k.MA_KIEU == mkieu).ToList();
                                if (aTemp_k.Count > 0)
                                {
                                    aDM_KIEU = aTemp_k[0];
                                    aDM_KIEU.MA_CL = mcl;
                                    this.DbContext.SubmitChanges();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(mcl))
                                    {
                                        aDM_KIEU = new DM_KIEU();
                                        aDM_KIEU.MA_KIEU = mkieu;
                                        aDM_KIEU.MA_CL = mcl;
                                        this.DbContext.DM_KIEUs.InsertOnSubmit(aDM_KIEU);
                                        this.DbContext.SubmitChanges();
                                    }
                                }
                            }
                            #endregion

                            #region thiet bi
                            if (!String.IsNullOrEmpty(mcl) && mkieu > 0)
                            {
                                string mtb = Convert.ToString(worksheet.Cells[row, 1].Value);
                                List<DM_THIET_BI> aTemp_tb = aList_tb.Where(tb => tb.MA_TB.Equals(mtb)).ToList();
                                if (aTemp_tb.Count > 0)
                                {
                                    aDM_THIET_BI = aTemp_tb[0];
                                    aDM_THIET_BI.SERIAL = Convert.ToString(worksheet.Cells[row, 2].Value);
                                    aDM_THIET_BI.TEN_TB = Convert.ToString(worksheet.Cells[row, 3].Value);
                                    aDM_THIET_BI.MA_CL = mcl;
                                    aDM_THIET_BI.MA_KIEU = mkieu;
                                    if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 6].Value)))
                                    {
                                        aDM_THIET_BI.NGAY_BH = DungChung.ConvertStringToDate(Convert.ToString(worksheet.Cells[row, 6].Value), "dd/MM/yyyy");
                                    }
                                    if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 7].Value)))
                                    {
                                        aDM_THIET_BI.THOI_GIAN_BH = Convert.ToInt32(worksheet.Cells[row, 7].Value);
                                    }
                                    if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 8].Value)))
                                    {
                                        aDM_THIET_BI.NGAY_KH = DungChung.ConvertStringToDate(Convert.ToString(worksheet.Cells[row, 8].Value), "dd/MM/yyyy");
                                    }
                                    if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 9].Value)))
                                    {
                                        aDM_THIET_BI.THOI_GIAN_KH = Convert.ToInt32(worksheet.Cells[row, 9].Value);
                                    }
                                    if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 10].Value)))
                                    {
                                        aDM_THIET_BI.NGUYEN_GIA = Convert.ToDecimal(worksheet.Cells[row, 10].Value);
                                    }

                                    aDM_THIET_BI.TRANG_THAI = 1;
                                    aDM_THIET_BI.NGAY_TAO = DateTime.Now;
                                    aDM_THIET_BI.NGAY_SUA = DateTime.Now;
                                    this.DbContext.SubmitChanges();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(mtb))
                                    {
                                        aDM_THIET_BI = new DM_THIET_BI();
                                        aDM_THIET_BI.MA_TB = Convert.ToString(worksheet.Cells[row, 1].Value);
                                        aDM_THIET_BI.SERIAL = Convert.ToString(worksheet.Cells[row, 2].Value);
                                        aDM_THIET_BI.TEN_TB = Convert.ToString(worksheet.Cells[row, 3].Value);
                                        aDM_THIET_BI.MA_CL = mcl;
                                        aDM_THIET_BI.MA_KIEU = mkieu;
                                        if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 6].Value)))
                                        {
                                            aDM_THIET_BI.NGAY_BH = DungChung.ConvertStringToDate(Convert.ToString(worksheet.Cells[row, 6].Value), "dd/MM/yyyy");
                                        }
                                        if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 7].Value)))
                                        {
                                            aDM_THIET_BI.THOI_GIAN_BH = Convert.ToInt32(worksheet.Cells[row, 7].Value);
                                        }
                                        if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 8].Value)))
                                        {
                                            aDM_THIET_BI.NGAY_KH = DungChung.ConvertStringToDate(Convert.ToString(worksheet.Cells[row, 8].Value), "dd/MM/yyyy");
                                        }
                                        if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 9].Value)))
                                        {
                                            aDM_THIET_BI.THOI_GIAN_KH = Convert.ToInt32(worksheet.Cells[row, 9].Value);
                                        }
                                        if (!String.IsNullOrEmpty(Convert.ToString(worksheet.Cells[row, 10].Value)))
                                        {
                                            aDM_THIET_BI.NGUYEN_GIA = Convert.ToDecimal(worksheet.Cells[row, 10].Value);
                                        }

                                        aDM_THIET_BI.TRANG_THAI = 1;
                                        aDM_THIET_BI.NGAY_TAO = DateTime.Now;
                                        aDM_THIET_BI.NGAY_SUA = DateTime.Now;

                                        this.DbContext.DM_THIET_BIs.InsertOnSubmit(aDM_THIET_BI);
                                        this.DbContext.SubmitChanges();
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                    return response;
                }
                else
                {
                    response.Success = false;

                    response.ExtraParams["msg"] = "Vui lòng chỉ chọn file có đuôi mở rộng là ( .xlsx, .xsl, .xls ) !";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
                return response;
            }

        }

        public AjaxResult xoaThietBi(string id)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dmtb = (from tb in this.DbContext.DM_THIET_BIs
                            where tb.MA_TB.Equals(id)
                            select tb).First();
                this.DbContext.DM_THIET_BIs.DeleteOnSubmit(dmtb);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        public AjaxResult select_TB(string ma_tb)
        {
            AjaxResult result = new AjaxResult();
            JsonObject jobject = new JsonObject();
            try
            {
                var query = from p in this.DbContext.DM_THIET_BIs
                            join d in this.DbContext.DM_CHUNGLOAIs on p.MA_CL equals d.MA_CL into ps
                            from d in ps.DefaultIfEmpty()
                            join e in this.DbContext.DM_KIEUs on p.MA_KIEU equals e.MA_KIEU into ps1
                            from e in ps1.DefaultIfEmpty()
                            where p.MA_TB == ma_tb
                            select new
                            {
                                p.MA_TB,
                                p.TEN_TB,
                                p.SERIAL,
                                p.THOI_GIAN_BH,
                                p.THOI_GIAN_KH,
                                p.NGUYEN_GIA,
                                d.TEN_CL,
                                e.TEN_KIEU
                            };

                if (query.Count() > 0)
                {
                    jobject.Add("serial", query.First().SERIAL);
                    jobject.Add("bh", query.First().THOI_GIAN_BH);
                    jobject.Add("kh", query.First().THOI_GIAN_KH);
                    jobject.Add("gia", query.First().NGUYEN_GIA);
                    jobject.Add("ten_cl", query.First().TEN_CL);
                    jobject.Add("ten_kieu", query.First().TEN_KIEU);
                }
                else
                {
                    jobject.Add("serial", "");
                    jobject.Add("bh", "");
                    jobject.Add("kh", "");
                    jobject.Add("gia", "");
                    jobject.Add("ten_cl", "");
                    jobject.Add("ten_kieu", "");
                }
            }
            catch (Exception e)
            {
                result.ErrorMessage = e.ToString();
            }
            result.Result = jobject;
            return result;
        }

        public JsonResult SoLuongThietBiTonKho(string MA_CL, long MA_KIEU, int TT)
        {
            try
            {
                int sl = 0;
                if (TT == 0)
                {
                    sl = this.DbContext.DM_THIET_BIs.Where(tb => tb.MA_CL.Equals(MA_CL) && tb.MA_KIEU == MA_KIEU && tb.TRANG_THAI == 2).ToList().Count;
                }
                else
                {
                    sl = this.DbContext.DM_THIET_BIs.Where(tb => tb.MA_CL.Equals(MA_CL) && tb.MA_KIEU == MA_KIEU && (tb.TRANG_THAI == 2 || tb.TRANG_THAI == 3)).ToList().Count;
                }
                return Json(sl, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        #region Thống kê thiết bị
        public ActionResult ThongKeThietBi()
        {
            return View();
        }

        public AjaxStoreResult TimKiemThietBiTheoTrangThai(string txtfilter, int start, int limit, string trangthai)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            int trangthai1 = String.IsNullOrEmpty(trangthai) ? 0 : Convert.ToInt32(trangthai);


            if (trangthai1 == 0)
            {
                var query = (from tb in this.DbContext.DM_THIET_BIs
                             join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                             join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                             join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                             where tb.SERIAL.Equals(txtfilter) || SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                             orderby tb.MA_CL ascending
                             select new
                             {
                                 tb.MA_TB,
                                 tb.SERIAL,
                                 tb.TEN_TB,
                                 tb.MA_CL,
                                 tb.MA_KIEU,
                                 tb.NGAY_BH,
                                 tb.THOI_GIAN_BH,
                                 tb.NGAY_KH,
                                 tb.THOI_GIAN_KH,
                                 tb.TRANG_THAI,
                                 tt.TEN_TT,
                                 tb.NGAY_TAO,
                                 tb.NGAY_SUA,
                                 tb.NGUYEN_GIA,
                                 cl.TEN_CL,
                                 k.TEN_KIEU
                             });
                int total = query.ToList().Count;
                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
            else
            {
                var query = (from tb in this.DbContext.DM_THIET_BIs
                             join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                             join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                             join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                             where (tb.SERIAL.Equals(txtfilter) || SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%")) && tb.TRANG_THAI == trangthai1
                             orderby tb.MA_CL ascending
                             select new
                             {
                                 tb.MA_TB,
                                 tb.SERIAL,
                                 tb.TEN_TB,
                                 tb.MA_CL,
                                 tb.MA_KIEU,
                                 tb.NGAY_BH,
                                 tb.THOI_GIAN_BH,
                                 tb.NGAY_KH,
                                 tb.THOI_GIAN_KH,
                                 tb.TRANG_THAI,
                                 tt.TEN_TT,
                                 tb.NGAY_TAO,
                                 tb.NGAY_SUA,
                                 tb.NGUYEN_GIA,
                                 cl.TEN_CL,
                                 k.TEN_KIEU
                             });
                int total = query.ToList().Count;
                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
        }

        #endregion

        #endregion

        #region danh mục đơn vị bảo hành
        public ActionResult Dm_Donvibaohanh()
        {
            return View();
        }

        public AjaxStoreResult dsDonViBaoHanh(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            var query = (from dvbh in this.DbContext.DM_DON_VI_BAO_HANHs
                         where SqlMethods.Like(dvbh.TEN_DVBH, "%" + LIB.ProcessStrVal(txtfilter) + "%") || dvbh.MA_DVBH.Equals(txtfilter)
                         orderby dvbh.TEN_DVBH ascending
                         select new
                         {
                             dvbh.MA_DVBH,
                             dvbh.TEN_DVBH,
                             dvbh.DIA_CHI,
                             dvbh.SO_DIEN_THOAI,
                             dvbh.EMAIL,
                             dvbh.GHI_CHU
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveDonViBaoHanh(FormCollection values)
        {
            DM_DON_VI_BAO_HANH dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                List<DM_DON_VI_BAO_HANH> aList = (from dvbh in this.DbContext.DM_DON_VI_BAO_HANHs
                                                  where dvbh.MA_DVBH.Equals(values["MA_DVBH"])
                                                  select dvbh).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã đơn vị bảo hành đã tồn tại, Vui lòng nhập mã đơn vị bảo hành khác !";
                    }
                    else
                    {
                        dm = new DM_DON_VI_BAO_HANH();
                        dm.MA_DVBH = values["MA_DVBH"];
                        dm.TEN_DVBH = values["TEN_DVBH"];
                        dm.DIA_CHI = values["DIA_CHI"];
                        dm.SO_DIEN_THOAI = values["SO_DIEN_THOAI"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHI_CHU = values["GHI_CHU"];

                        this.DbContext.DM_DON_VI_BAO_HANHs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_DVBH = values["TEN_DVBH"];
                        dm.DIA_CHI = values["DIA_CHI"];
                        dm.SO_DIEN_THOAI = values["SO_DIEN_THOAI"];
                        dm.EMAIL = values["EMAIL"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDonViBaoHanh(string MA_DVBH)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dmdvbh = (from dvbh in this.DbContext.DM_DON_VI_BAO_HANHs
                              where dvbh.MA_DVBH.Equals(MA_DVBH)
                              select dvbh).First();
                this.DbContext.DM_DON_VI_BAO_HANHs.DeleteOnSubmit(dmdvbh);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion

        #region danh mục hợp đồng
        public ActionResult Dm_Hopdong()
        {
            return View();
        }

        public AjaxStoreResult dsHopdong(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            var query = (from hd in this.DbContext.DM_HOPDONGs
                         where SqlMethods.Like(hd.TEN_HD, "%" + LIB.ProcessStrVal(txtfilter) + "%") || hd.MA_HD.Equals(txtfilter)
                         orderby hd.NGAY_SUA ascending
                         select new
                         {
                             hd.MA_HD,
                             hd.TEN_HD,
                             hd.MA_SO,
                             hd.NGAY_BD,
                             hd.NGAY_KT,
                             hd.DAI_DIEN_A,
                             hd.DAI_DIEN_B,
                             hd.MO_TA,
                             hd.GHI_CHU,
                             hd.TRANG_THAI,
                             hd.NGAY_TAO,
                             hd.NGAY_SUA
                         });
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveHopDong(FormCollection values)
        {
            DM_HOPDONG dm;
            AjaxFormResult response = new AjaxFormResult();

            try
            {
                List<DM_HOPDONG> aList = (from hd in this.DbContext.DM_HOPDONGs
                                          where hd.MA_HD.Equals(values["MA_HD"])
                                          select hd).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã hợp đồng đã tồn tại, Vui lòng nhập mã hợp đồng khác !";
                    }
                    else
                    {
                        dm = new DM_HOPDONG();
                        dm.MA_HD = values["MA_HD"];
                        dm.TEN_HD = values["TEN_HD"];
                        dm.MA_SO = values["MA_SO"];
                        if (!String.IsNullOrEmpty(values["NGAY_BD"]))
                        {
                            dm.NGAY_BD = Convert.ToDateTime(values["NGAY_BD"]);
                        }
                        if (!String.IsNullOrEmpty(values["NGAY_KT"]))
                        {
                            dm.NGAY_KT = Convert.ToDateTime(values["NGAY_KT"]);
                        }
                        dm.DAI_DIEN_A = values["DAI_DIEN_A"];
                        dm.DAI_DIEN_B = values["DAI_DIEN_B"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        dm.TRANG_THAI = 1;
                        dm.NGAY_TAO = DateTime.Now;
                        dm.NGAY_SUA = DateTime.Now;
                        this.DbContext.DM_HOPDONGs.InsertOnSubmit(dm);
                        this.DbContext.SubmitChanges();
                    }
                }
                else
                {

                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_HD = values["TEN_HD"];
                        dm.MA_SO = values["MA_SO"];
                        if (!String.IsNullOrEmpty(values["NGAY_BD"]))
                        {
                            dm.NGAY_BD = Convert.ToDateTime(values["NGAY_BD"]);
                        }
                        if (!String.IsNullOrEmpty(values["NGAY_KT"]))
                        {
                            dm.NGAY_KT = Convert.ToDateTime(values["NGAY_KT"]);
                        }
                        dm.DAI_DIEN_A = values["DAI_DIEN_A"];
                        dm.DAI_DIEN_B = values["DAI_DIEN_B"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        dm.TRANG_THAI = 1;
                        dm.NGAY_SUA = DateTime.Now;

                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaHopDong(string MA_HD)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                var dmdh = (from hd in this.DbContext.DM_HOPDONGs
                            where hd.MA_HD.Equals(MA_HD)
                            select hd).First();
                this.DbContext.DM_HOPDONGs.DeleteOnSubmit(dmdh);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion


        #region yêu cầu thiết bị

        public AjaxStoreResult dsYeuCauThietBi(string txtfilter, string loai, int start, int limit)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             * Loai = 4 : Yêu cầu thanh lý thiết bị
             */

            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            int loai1 = String.IsNullOrEmpty(loai) ? 0 : Convert.ToInt32(loai);
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            if (loai1 == 0)
            {
                var query1 = (from pyc in this.DbContext.PHIEU_YEU_CAUs
                              join nsd in this.DbContext.NHANVIENs on pyc.MA_NV equals nsd.MA_NV
                              join yc in this.DbContext.PHIEUYEUCAU_THIETBIs on pyc.MA_PYC equals yc.MA_PYC
                              join cl in this.DbContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                              join k in this.DbContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                              where SqlMethods.Like(nsd.TEN_NV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || pyc.MA_PYC.Equals(txtfilter)

                              orderby pyc.LOAI
                              select new
                              {
                                  MA_PYC = pyc.MA_PYC,
                                  MA_NV = pyc.MA_NV,
                                  LY_DO = pyc.LY_DO,
                                  NGAY_DE_NGHI = pyc.NGAY_DE_NGHI,
                                  NGAY_CAP = pyc.NGAY_CAP,
                                  LOAI = pyc.LOAI,
                                  CHU_THICH = pyc.GHI_CHU,
                                  LOAI_DP = PYC_LOAI.HienThi(Convert.ToInt32(pyc.LOAI)),
                                  MO_TA = pyc.MO_TA,
                                  GHI_CHU_PYC = pyc.GHI_CHU,
                                  NGAY_TAO = pyc.NGAY_TAO,
                                  NGAY_SUA = pyc.NGAY_SUA,
                                  TRANG_THAI = pyc.TRANG_THAI,
                                  TRANG_THAI_DP = PYC_TRANG_THAI.HienThi(Convert.ToInt32(pyc.TRANG_THAI)),
                                  TEN_NV = nsd.TEN_NV,
                                  TEN_CL = cl.TEN_CL,
                                  TEN_KIEU = k.TEN_KIEU,
                                  SO_LUONG = yc.SO_LUONG,
                                  GHI_CHU = yc.GHI_CHU,
                                  ID1 = yc.MA_PYC,
                                  SERIAL = yc.SERIAL
                              });

                int total1 = query1.ToList().Count;
                query1 = query1.Skip(start).Take(limit);
                return new AjaxStoreResult(query1, total1);
            }
            else
            {
                var query = (from pyc in this.DbContext.PHIEU_YEU_CAUs
                             join nsd in this.DbContext.NHANVIENs on pyc.MA_NV equals nsd.MA_NV
                             join yc in this.DbContext.PHIEUYEUCAU_THIETBIs on pyc.MA_PYC equals yc.MA_PYC
                             join cl in this.DbContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                             join k in this.DbContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                             where pyc.LOAI == loai1 && SqlMethods.Like(nsd.TEN_NV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || pyc.MA_PYC.Equals(txtfilter)
                             orderby pyc.LOAI
                             select new
                             {
                                 MA_PYC = pyc.MA_PYC,
                                 MA_NV = pyc.MA_NV,
                                 LY_DO = pyc.LY_DO,
                                 NGAY_DE_NGHI = pyc.NGAY_DE_NGHI,
                                 NGAY_CAP = pyc.NGAY_CAP,
                                 LOAI = pyc.LOAI,
                                 LOAI_DP = PYC_LOAI.HienThi(Convert.ToInt32(pyc.LOAI)),
                                 MO_TA = pyc.MO_TA,
                                 GHI_CHU_PYC = pyc.GHI_CHU,
                                 NGAY_TAO = pyc.NGAY_TAO,
                                 NGAY_SUA = pyc.NGAY_SUA,
                                 TRANG_THAI = pyc.TRANG_THAI,
                                 TRANG_THAI_DP = PYC_TRANG_THAI.HienThi(Convert.ToInt32(pyc.TRANG_THAI)),
                                 TEN_NV = nsd.TEN_NV,
                                 TEN_CL = cl.TEN_CL,
                                 TEN_KIEU = k.TEN_KIEU,
                                 SO_LUONG = yc.SO_LUONG,
                                 GHI_CHU = yc.GHI_CHU,
                                 ID1 = yc.ID,
                                 SERIAL = yc.SERIAL
                             });

                int total = query.ToList().Count;
                query = query.Skip(start).Take(limit);
                return new AjaxStoreResult(query, total);
            }
        }

        public AjaxStoreResult dsYeuCauTongQuat(string txtfilter, string loai, string trangthai, int start, int limit)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             * Loai = 4 : Yêu cầu thanh lý thiết bị
             */

            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            int loai1 = String.IsNullOrEmpty(loai) ? 0 : Convert.ToInt32(loai);

            string[] ast = trangthai.Split(',');
            List<int> li = new List<int>();
            for (int i = 0; i < ast.Count(); i++)
            {
                li.Add(Convert.ToInt32(ast[i]));
            }
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            if (loai1 == 0)
            {

                var query1 = (from pyc in this.DbContext.PHIEU_YEU_CAUs
                              join nsd in this.DbContext.NHANVIENs on pyc.MA_NV equals nsd.MA_NV
                              where (SqlMethods.Like(nsd.TEN_NV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || pyc.MA_PYC.Equals(txtfilter))
                              && li.Contains(Convert.ToInt32(pyc.TRANG_THAI))
                              orderby pyc.LOAI
                              select new
                              {
                                  MA_PYC = pyc.MA_PYC,
                                  MA_NV = pyc.MA_NV,
                                  TEN_NV = nsd.TEN_NV,
                                  LY_DO = pyc.LY_DO,
                                  NGAY_DE_NGHI = pyc.NGAY_DE_NGHI,
                                  NGAY_CAP = pyc.NGAY_CAP,
                                  LOAI = pyc.LOAI,
                                  LOAI_DP = PYC_LOAI.HienThi(Convert.ToInt32(pyc.LOAI)),
                                  MO_TA = pyc.MO_TA,
                                  GHI_CHU = pyc.GHI_CHU,
                                  NGAY_TAO = pyc.NGAY_TAO,
                                  NGAY_SUA = pyc.NGAY_SUA,
                                  TRANG_THAI = pyc.TRANG_THAI,
                                  BAO_HANH = pyc.BAO_HANH,
                                  TRANG_THAI_DP = PYC_TRANG_THAI.HienThi(Convert.ToInt32(pyc.TRANG_THAI))
                              });

                int total1 = query1.ToList().Count;
                query1 = query1.Skip(start).Take(limit);
                return new AjaxStoreResult(query1, total1);

            }
            else
            {

                var query1 = (from pyc in this.DbContext.PHIEU_YEU_CAUs
                              join nsd in this.DbContext.NHANVIENs on pyc.MA_NV equals nsd.MA_NV
                              where (SqlMethods.Like(nsd.TEN_NV, "%" + LIB.ProcessStrVal(txtfilter) + "%") || pyc.MA_PYC.Equals(txtfilter))
                              && pyc.LOAI == loai1 && li.Contains(Convert.ToInt32(pyc.TRANG_THAI))
                              orderby pyc.LOAI
                              select new
                              {
                                  MA_PYC = pyc.MA_PYC,
                                  MA_NV = pyc.MA_NV,
                                  TEN_NV = nsd.TEN_NV,
                                  LY_DO = pyc.LY_DO,
                                  NGAY_DE_NGHI = pyc.NGAY_DE_NGHI,
                                  NGAY_CAP = pyc.NGAY_CAP,
                                  LOAI = pyc.LOAI,
                                  LOAI_DP = PYC_LOAI.HienThi(Convert.ToInt32(pyc.LOAI)),
                                  MO_TA = pyc.MO_TA,
                                  GHI_CHU = pyc.GHI_CHU,
                                  NGAY_TAO = pyc.NGAY_TAO,
                                  NGAY_SUA = pyc.NGAY_SUA,
                                  TRANG_THAI = pyc.TRANG_THAI,
                                  BAO_HANH = pyc.BAO_HANH,
                                  TRANG_THAI_DP = PYC_TRANG_THAI.HienThi(Convert.ToInt32(pyc.TRANG_THAI))
                              });

                int total1 = query1.ToList().Count;
                query1 = query1.Skip(start).Take(limit);
                return new AjaxStoreResult(query1, total1);

            }
        }


        //Dung trong cac truong hop yeu cau sua, thanh ly,thu hoi
        public AjaxStoreResult dsYeuCauChiTiet(string txtfilter, int start, int limit)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             * Loai = 4 : Yêu cầu thanh lý thiết bị
             */

            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from yc in this.DbContext.PHIEUYEUCAU_THIETBIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on yc.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on yc.MA_KIEU equals k.MA_KIEU
                         join nv in this.DbContext.NHANVIENs on yc.MA_NV equals nv.MA_NV

                         join tb in this.DbContext.DM_THIET_BIs on yc.SERIAL equals tb.SERIAL
                         into ps
                         from tb in ps.DefaultIfEmpty()


                         where yc.MA_PYC.Equals(txtfilter)
                         orderby yc.NGAY_SUA ascending
                         select new
                         {
                             yc.ID,
                             yc.SERIAL,
                             yc.MA_PYC,
                             yc.MA_CL,
                             yc.MA_KIEU,
                             yc.MO_TA,
                             yc.GHI_CHU,
                             yc.NGAY_TAO,
                             yc.NGAY_SUA,
                             yc.SO_LUONG,
                             cl.TEN_CL,
                             k.TEN_KIEU,
                             tb.MA_TB,
                             tb.TEN_TB,
                             nv.MA_NV,
                             nv.TEN_NV
                         });


            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);

        }

        public AjaxStoreResult TimKiemNhanVienTheo_SERIAL_TB(string SERIAL)
        {
            SERIAL = String.IsNullOrEmpty(SERIAL) ? string.Empty : SERIAL;

            var query = (from yc in this.DbContext.PHIEUYEUCAU_THIETBIs
                         join nv in this.DbContext.NHANVIENs on yc.MA_NV equals nv.MA_NV
                         where yc.SERIAL.Equals(SERIAL) && yc.SERIAL.Length > 0 && yc.TRANG_THAI == true
                         select new
                         {
                             nv.MA_NV,
                             nv.TEN_NV
                         });
            int total = query.ToList().Count;
            query = query.Skip(0).Take(total);
            return new AjaxStoreResult(query, total);

        }

        public AjaxResult ChoDuyet(string ID)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                where yc.MA_PYC.Equals(ID)
                                                select yc).First();
                aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.CHO_DUYET;
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        public AjaxFormResult ThamDinhYeuCau(FormCollection values, string MA_PYC, int TRANG_THAI)
        {
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                if (String.IsNullOrEmpty(MA_PYC))
                {
                    response.Success = false;
                    response.ExtraParams["msg"] = "Mã phiếu yêu cầu không tồn tại!";
                }
                else
                {
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                    where yc.MA_PYC.Equals(MA_PYC)
                                                    select yc).First();
                    aPHIEU_YEU_CAU.GHI_CHU = values["TD_GHI_CHU"];
                    aPHIEU_YEU_CAU.TRANG_THAI = TRANG_THAI;
                    this.DbContext.SubmitChanges();
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }


        public AjaxFormResult ThamDinhThayThe(FormCollection values, string MA_PYC, int TRANG_THAI, string json)
        {
            string maPYC_M = string.Empty;
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                if (String.IsNullOrEmpty(MA_PYC))
                {
                    response.Success = false;
                    response.ExtraParams["msg"] = "Mã phiếu yêu cầu không tồn tại!";
                }
                else
                {
                    //Ket thuc qua trinh yeu cau sua chua
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                    where yc.MA_PYC.Equals(MA_PYC)
                                                    select yc).First();
                    aPHIEU_YEU_CAU.GHI_CHU = values["TDTT_GHI_CHU"];
                    aPHIEU_YEU_CAU.TRANG_THAI = TRANG_THAI;
                    this.DbContext.SubmitChanges();

                    //Them moi yeu cau thu hoi thiet bi
                    List<PHIEU_YEU_CAU> aList = new List<PHIEU_YEU_CAU>();
                    maPYC_M = DungChung.TaoMaTuDong("PYCTH");
                    aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC_M)).ToList();
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC_M = DungChung.TaoMaTuDong("PYCTH");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC_M)).ToList();
                        }
                    }

                    PHIEU_YEU_CAU th = new PHIEU_YEU_CAU();
                    th.MA_PYC = maPYC_M;
                    th.MA_NV = aPHIEU_YEU_CAU.MA_NV;
                    th.LY_DO = aPHIEU_YEU_CAU.LY_DO;
                    th.NGAY_DE_NGHI = aPHIEU_YEU_CAU.NGAY_DE_NGHI;
                    th.NGAY_CAP = aPHIEU_YEU_CAU.NGAY_CAP;
                    th.LOAI = 3;
                    th.MO_TA = aPHIEU_YEU_CAU.MO_TA;
                    th.GHI_CHU = aPHIEU_YEU_CAU.GHI_CHU;
                    th.TRANG_THAI = 4;//da phe duyet
                    th.NGAY_TAO = DateTime.Now;
                    th.NGAY_SUA = DateTime.Now;


                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(th);
                    this.DbContext.SubmitChanges();

                    PHIEUYEUCAU_THIETBI aPHIEUYEUCAU_THIETBI;

                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {
                        aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                        aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                        aPHIEUYEUCAU_THIETBI.MA_PYC = maPYC_M;
                        aPHIEUYEUCAU_THIETBI.SERIAL = row["SERIAL"];
                        aPHIEUYEUCAU_THIETBI.MA_CL = row["MA_CL"];
                        aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty(row["MA_KIEU"]) ? 0 : Convert.ToInt32(row["MA_KIEU"]);
                        //aPHIEUYEUCAU_THIETBI.MO_TA
                        aPHIEUYEUCAU_THIETBI.GHI_CHU = row["GHI_CHU"];
                        aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                        aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                        aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                        aPHIEUYEUCAU_THIETBI.MA_NV = row["MA_NV"];
                        aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                        this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                        this.DbContext.SubmitChanges();
                    }

                    //Them moi qua trinh them moi thiet bi

                    maPYC_M = DungChung.TaoMaTuDong("PDNCM");
                    aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC_M)).ToList();
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC_M = DungChung.TaoMaTuDong("PDNCM");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC_M)).ToList();
                        }
                    }

                    PHIEU_YEU_CAU tm = new PHIEU_YEU_CAU();
                    tm.MA_PYC = maPYC_M;
                    tm.MA_NV = aPHIEU_YEU_CAU.MA_NV;
                    tm.LY_DO = aPHIEU_YEU_CAU.LY_DO;
                    tm.NGAY_DE_NGHI = aPHIEU_YEU_CAU.NGAY_DE_NGHI;
                    tm.NGAY_CAP = aPHIEU_YEU_CAU.NGAY_CAP;
                    tm.LOAI = 1;
                    tm.MO_TA = aPHIEU_YEU_CAU.MO_TA;
                    tm.GHI_CHU = aPHIEU_YEU_CAU.GHI_CHU;
                    tm.TRANG_THAI = 4;//da phe duyet
                    tm.NGAY_TAO = DateTime.Now;
                    tm.NGAY_SUA = DateTime.Now;

                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(tm);
                    this.DbContext.SubmitChanges();

                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {
                        aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                        aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                        aPHIEUYEUCAU_THIETBI.MA_PYC = maPYC_M;
                        aPHIEUYEUCAU_THIETBI.SERIAL = row["SERIAL"];
                        aPHIEUYEUCAU_THIETBI.MA_CL = row["MA_CL"];
                        aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty(row["MA_KIEU"]) ? 0 : Convert.ToInt32(row["MA_KIEU"]);
                        //aPHIEUYEUCAU_THIETBI.MO_TA
                        aPHIEUYEUCAU_THIETBI.GHI_CHU = row["GHI_CHU"];
                        aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                        aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                        aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                        aPHIEUYEUCAU_THIETBI.MA_NV = row["MA_NV"];
                        aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                        this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                        this.DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }


        public AjaxFormResult ThamDinhBaoHanh(FormCollection values, string MA_PYC, int TRANG_THAI, string json)
        {
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                if (String.IsNullOrEmpty(MA_PYC))
                {
                    response.Success = false;
                    response.ExtraParams["msg"] = "Mã phiếu yêu cầu không tồn tại!";
                }
                else
                {
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                    where yc.MA_PYC.Equals(MA_PYC)
                                                    select yc).First();

                    if (values["grBaoHanh_Group"].Equals("baohanh"))
                    {
                        aPHIEU_YEU_CAU.BAO_HANH = "Đi bảo hành thiết bị";
                    }
                    else
                    {
                        aPHIEU_YEU_CAU.BAO_HANH = "Sửa thiết bị ở ngoài";
                    }


                    aPHIEU_YEU_CAU.GHI_CHU = values["TDBH_GHI_CHU"];
                    aPHIEU_YEU_CAU.TRANG_THAI = TRANG_THAI;
                    this.DbContext.SubmitChanges();

                    List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                    DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {

                        string MA_TB = row["MA_TB"];
                        List<DM_THIET_BI> aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();
                        if (aListTemp.Count > 0)
                        {
                            aDM_THIET_BI = aListTemp[0];
                            aDM_THIET_BI.TRANG_THAI = 6;
                            this.DbContext.SubmitChanges();
                        }


                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }


        public AjaxFormResult HoanThanhSuaChua(FormCollection values, string MA_PYC, int TRANG_THAI, string json)
        {
            Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                if (String.IsNullOrEmpty(MA_PYC))
                {
                    response.Success = false;
                    response.ExtraParams["msg"] = "Mã phiếu yêu cầu không tồn tại!";
                }
                else
                {
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                    where yc.MA_PYC.Equals(MA_PYC)
                                                    select yc).First();

                    aPHIEU_YEU_CAU.GHI_CHU = values["TD_GHI_CHU"];
                    aPHIEU_YEU_CAU.TRANG_THAI = TRANG_THAI;
                    this.DbContext.SubmitChanges();

                    List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                    DM_THIET_BI aDM_THIET_BI = new DM_THIET_BI();
                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {
                        string MA_TB = row["MA_TB"];
                        List<DM_THIET_BI> aListTemp = dsThietBi.Where(tb => tb.MA_TB.Equals(MA_TB)).ToList();
                        if (aListTemp.Count > 0)
                        {
                            aDM_THIET_BI = aListTemp[0];
                            aDM_THIET_BI.TRANG_THAI = 4;
                            this.DbContext.SubmitChanges();
                        }


                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxFormResult KhoNhanLaiThietBi(FormCollection values, string MA_PYC, string json)
        {
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                if (String.IsNullOrEmpty(MA_PYC))
                {
                    response.Success = false;
                    response.ExtraParams["msg"] = "Mã phiếu yêu cầu không tồn tại!";
                }
                else
                {
                    PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                    where yc.MA_PYC.Equals(MA_PYC)
                                                    select yc).First();
                    aPHIEU_YEU_CAU.GHI_CHU = values["TD_GHI_CHU"];
                    aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.KET_THUC;//Ket thuc yeu cau
                    this.DbContext.SubmitChanges();

                    List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                    Dictionary<string, string>[] chitiet_ts = JSON.Deserialize<Dictionary<string, string>[]>(json);
                    foreach (Dictionary<string, string> row in chitiet_ts)
                    {
                        string mtb = row["MA_TB"];
                        List<DM_THIET_BI> tb = dsThietBi.Where(t => t.MA_TB.Equals(mtb)).ToList();
                        if (tb.Count > 0)
                        {
                            DM_THIET_BI aDM_THIET_BI = tb[0];
                            aDM_THIET_BI.TRANG_THAI = 2;
                            this.DbContext.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult ChoCapThietBi(string MA_PYC)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                where yc.MA_PYC.Equals(MA_PYC)
                                                select yc).First();
                aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.CHO_CAP_TB;
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        public AjaxResult DaCapThietBi(string ID)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                PHIEU_YEU_CAU aPHIEU_YEU_CAU = (from yc in this.DbContext.PHIEU_YEU_CAUs
                                                where yc.MA_PYC.Equals(ID)
                                                select yc).First();
                aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.DA_CAP_TB;
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaYeuCauThietBi(string MA_PYC)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                if (!String.IsNullOrEmpty(MA_PYC))
                {
                    var cv = (from pyc in this.DbContext.PHIEU_YEU_CAUs
                              where pyc.MA_PYC == MA_PYC
                              select pyc).First();
                    this.DbContext.PHIEU_YEU_CAUs.DeleteOnSubmit(cv);

                    List<PHIEUYEUCAU_THIETBI> aList = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(MA_PYC)).ToList();
                    foreach (PHIEUYEUCAU_THIETBI pyctb in aList)
                    {
                        this.DbContext.PHIEUYEUCAU_THIETBIs.DeleteOnSubmit(pyctb);
                    }
                    this.DbContext.SubmitChanges();
                }
                else
                {
                    response.ErrorMessage = "Không có mã phiếu yêu cầu cần xóa.";
                }

            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        [HttpPost]
        public JsonResult AnHienChucNang(int trangthai, int loai)
        {
            bool choduyet = false;

            bool khongduyet = false;

            bool daduyet = false;

            bool thaythe = false;
            bool tusua = false;
            bool baohanh = false;

            if (loai == 1)
            {
                if (trangthai == 1)
                {
                    choduyet = true; khongduyet = true; daduyet = true;
                }
                else if (trangthai == 2)
                {
                    khongduyet = true; daduyet = true;
                }
                else
                {
                    choduyet = false; khongduyet = false; daduyet = false;
                }
            }
            else if (loai == 2)
            {
                if (trangthai == 1)
                {
                    choduyet = true; thaythe = true; tusua = true; baohanh = true;
                }
                else if (trangthai == 2)
                {
                    thaythe = true; tusua = true; baohanh = true;
                }
                else
                {
                    choduyet = false; thaythe = false; tusua = false; baohanh = false;
                }
            }
            else if (loai == 3 || loai == 4)
            {
                if (trangthai == 1)
                {
                    choduyet = true; khongduyet = true; daduyet = true;
                }
                else if (trangthai == 2)
                {
                    khongduyet = true; daduyet = true;
                }
                else
                {
                    choduyet = false; khongduyet = false; daduyet = false;
                }
            }
            else
            {
                choduyet = false; khongduyet = false; daduyet = false;

            }
            return Json(new
            {
                CHO_DUYET = choduyet,
                KHONG_DUYET = khongduyet,
                DA_DUYET = daduyet,
                THAY_THE = thaythe,
                TU_SUA = tusua,
                BAO_HANH = baohanh
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AnHienChucNang_XuatKho(int trangthai, int loai)
        {
            bool CCTB = false;
            bool TM = false; bool TM_BC = false; bool SC = false; bool SC_BC = false; bool TH = false;
            bool TH_BC = false;

            if (loai == 1) //Cap moi
            {

                if (trangthai == 1)
                {
                }
                else if (trangthai == 2)
                {
                }
                else if (trangthai == 3)
                {
                }
                else if (trangthai == 4)
                {
                    CCTB = true;
                    TM = true;
                }
                else if (trangthai == 5)
                {
                    TM = true;
                }
                else if (trangthai == 6)
                {
                    TM_BC = true;
                }
                else
                {
                }
            }
            else if (loai == 2) // Sua
            {

                if (trangthai == 1)
                {
                }
                else if (trangthai == 2)
                {
                }
                else if (trangthai == 3)
                {
                }
                else if (trangthai == 4)
                {
                    SC = true;
                }
                else if (trangthai == 5)
                {
                    SC = true;
                }
                else if (trangthai == 6)
                {
                    SC_BC = true;
                }
                else
                {
                }
            }
            else if (loai == 3)//Thu hoi
            {
                if (trangthai == 1)
                {
                }
                else if (trangthai == 2)
                {
                }
                else if (trangthai == 3)
                {
                }
                else if (trangthai == 4)
                {
                }
                else if (trangthai == 5)
                {
                }
                else if (trangthai == 6)
                {
                }
                else
                {
                }
            }
            else if (loai == 4)//Thanh ly
            {

                if (trangthai == 1)
                {
                }
                else if (trangthai == 2)
                {
                }
                else if (trangthai == 3)
                {
                }
                else if (trangthai == 4)
                {
                    TH = true;
                }
                else if (trangthai == 5)
                {
                    TH = true;
                }
                else if (trangthai == 10)
                {
                    TH_BC = true;
                }
                else
                {
                }
            }
            else
            {

            }
            return Json(new
            {
                CCTB = CCTB,
                TM = TM,
                TM_BC = TM_BC,
                SC = SC,
                SC_BC = SC_BC,
                TH = TH,
                TH_BC = TH_BC
            }, JsonRequestBehavior.AllowGet);
        }



        #region yêu cầu cấp mới thiết bị
        public ActionResult PhieuYeuCau()
        {
            return View();
        }
        public AjaxFormResult saveYeuCauCapMoi(FormCollection values, string gpAllData)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             */
            PHIEU_YEU_CAU aPHIEU_YEU_CAU;
            PHIEUYEUCAU_THIETBI aPHIEUYEUCAU_THIETBI;
            JObject aJObject = JObject.Parse("{DS:" + gpAllData + "}");
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<PHIEU_YEU_CAU> aList = new List<PHIEU_YEU_CAU>();
                aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(values["MA_PYC"])).ToList();
                if (String.IsNullOrEmpty(values["ID"]))
                {
                    string maPYC = values["MA_PYC"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC = DungChung.TaoMaTuDong("PDNCM");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC)).ToList();
                        }
                    }
                    #region
                    aPHIEU_YEU_CAU = new PHIEU_YEU_CAU();
                    aPHIEU_YEU_CAU.MA_PYC = maPYC;
                    aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];

                    aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];

                    if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                    {
                        aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                    }
                    //aPHIEU_YEU_CAU.NGAY_CAP
                    aPHIEU_YEU_CAU.LOAI = PYC_LOAI.YEU_CAU_MOI;
                    aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];
                    //aPHIEU_YEU_CAU.GHI_CHU = values["GHI_CHU_PYC"];
                    aPHIEU_YEU_CAU.NGAY_TAO = DateTime.Now;
                    aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                    aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(aPHIEU_YEU_CAU);
                    this.DbContext.SubmitChanges();
                    #endregion

                    #region
                    foreach (var item in aJObject["DS"])
                    {
                        aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                        aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                        aPHIEUYEUCAU_THIETBI.MA_PYC = maPYC;
                        aPHIEUYEUCAU_THIETBI.SERIAL = string.Empty;
                        string mcl = (string)item["MA_CL"];
                        aPHIEUYEUCAU_THIETBI.MA_CL = mcl;
                        int mk = String.IsNullOrEmpty(Convert.ToString(item["MA_KIEU"])) ? 0 : Convert.ToInt32(Convert.ToString(item["MA_KIEU"]));
                        aPHIEUYEUCAU_THIETBI.MA_KIEU = mk;
                        //aPHIEUYEUCAU_THIETBI.MO_TA
                        aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                        aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                        aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                        int sl = String.IsNullOrEmpty(Convert.ToString(item["SO_LUONG"])) ? 0 : Convert.ToInt32(Convert.ToString(item["SO_LUONG"]));
                        aPHIEUYEUCAU_THIETBI.SO_LUONG = sl;
                        aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                        aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                        this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                        this.DbContext.SubmitChanges();
                        List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.Where(tb => tb.MA_CL.Equals(mcl) && tb.MA_KIEU == mk && tb.TRANG_THAI == 2).ToList();
                        for (int i = 0; i < sl; i++)
                        {
                            if (i <= dsThietBi.Count)
                            {
                                DM_THIET_BI aDM_THIET_BI = dsThietBi[i];
                                aDM_THIET_BI.TRANG_THAI = 3;
                                this.DbContext.SubmitChanges();
                            }
                        }

                    }
                    #endregion

                }
                else
                {
                    if (aList.Count > 0)
                    {
                        #region

                        aPHIEU_YEU_CAU = aList[0];
                        aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];

                        aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];

                        if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                        {
                            aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                        }
                        aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];

                        aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                        this.DbContext.SubmitChanges();

                        #endregion

                        #region

                        List<PHIEUYEUCAU_THIETBI> ads = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(yc => yc.MA_PYC.Equals(values["MA_PYC"])).ToList();
                        foreach (PHIEUYEUCAU_THIETBI PYCTB in ads)
                        {
                            string mcl = PYCTB.MA_CL;
                            int mk = Convert.ToInt32(PYCTB.MA_KIEU);
                            int sl = Convert.ToInt32(PYCTB.SO_LUONG);

                            List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.Where(tb => tb.MA_CL.Equals(mcl) && tb.MA_KIEU == mk && tb.TRANG_THAI == 3).ToList();
                            for (int i = 0; i < sl; i++)
                            {
                                if (i <= dsThietBi.Count)
                                {
                                    DM_THIET_BI aDM_THIET_BI = dsThietBi[i];
                                    aDM_THIET_BI.TRANG_THAI = 2;
                                    this.DbContext.SubmitChanges();
                                }
                            }

                            this.DbContext.PHIEUYEUCAU_THIETBIs.DeleteOnSubmit(PYCTB);
                            this.DbContext.SubmitChanges();
                        }

                        foreach (var item in aJObject["DS"])
                        {
                            aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                            aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                            aPHIEUYEUCAU_THIETBI.MA_PYC = values["MA_PYC"];
                            aPHIEUYEUCAU_THIETBI.SERIAL = string.Empty;
                            string mcl = (string)item["MA_CL"];
                            aPHIEUYEUCAU_THIETBI.MA_CL = mcl;
                            int mk = String.IsNullOrEmpty(Convert.ToString(item["MA_KIEU"])) ? 0 : Convert.ToInt32(Convert.ToString(item["MA_KIEU"]));
                            aPHIEUYEUCAU_THIETBI.MA_KIEU = mk;
                            //aPHIEUYEUCAU_THIETBI.MO_TA
                            aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                            aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                            int sl = String.IsNullOrEmpty(Convert.ToString(item["SO_LUONG"])) ? 0 : Convert.ToInt32(Convert.ToString(item["SO_LUONG"]));
                            aPHIEUYEUCAU_THIETBI.SO_LUONG = sl;
                            aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                            aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                            this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                            this.DbContext.SubmitChanges();
                            List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.Where(tb => tb.MA_CL.Equals(mcl) && tb.MA_KIEU == mk && tb.TRANG_THAI == 2).ToList();
                            for (int i = 0; i < sl; i++)
                            {
                                if (i <= dsThietBi.Count)
                                {
                                    DM_THIET_BI aDM_THIET_BI = dsThietBi[i];
                                    aDM_THIET_BI.TRANG_THAI = 3;
                                    this.DbContext.SubmitChanges();
                                }
                            }

                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }
        #endregion

        #region yêu cầu sửa thiết bị
        public ActionResult PhieuYeuCauSua()
        {
            return View();
        }

        public AjaxFormResult saveYeuCauSua(FormCollection values, string gpAllData)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             * Loai = 4 : Yêu cầu thu hồi thanh lý
             */
            PHIEU_YEU_CAU aPHIEU_YEU_CAU;
            PHIEUYEUCAU_THIETBI aPHIEUYEUCAU_THIETBI;
            JObject aJObject = JObject.Parse("{DS:" + gpAllData + "}");
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<PHIEU_YEU_CAU> aList = new List<PHIEU_YEU_CAU>();
                aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(values["MA_PYC"])).ToList();
                if (String.IsNullOrEmpty(values["ID"]))
                {
                    string maPYC = values["MA_PYC"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC = DungChung.TaoMaTuDong("PYCS");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC)).ToList();
                        }
                    }
                    #region
                    aPHIEU_YEU_CAU = new PHIEU_YEU_CAU();
                    aPHIEU_YEU_CAU.MA_PYC = maPYC;
                    aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];

                    int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                    if (ld > 0 && ld < 6)
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                    }
                    else
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                    }

                    if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                    {
                        aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                    }
                    //aPHIEU_YEU_CAU.NGAY_CAP
                    aPHIEU_YEU_CAU.LOAI = PYC_LOAI.YEU_CAU_SUA;
                    aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];
                    //aPHIEU_YEU_CAU.GHI_CHU = values["GHI_CHU_PYC"];
                    aPHIEU_YEU_CAU.NGAY_TAO = DateTime.Now;
                    aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                    aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(aPHIEU_YEU_CAU);
                    this.DbContext.SubmitChanges();
                    #endregion

                    #region
                    foreach (var item in aJObject["DS"])
                    {
                        string mpyc = maPYC;
                        string serial = (string)item["SERIAL"];
                        List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(mpyc) && y.SERIAL.Equals(serial)).ToList();
                        if (kt.Count <= 0)
                        {
                            aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                            aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                            aPHIEUYEUCAU_THIETBI.MA_PYC = mpyc;
                            aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                            aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                            aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                            //aPHIEUYEUCAU_THIETBI.MO_TA
                            aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                            aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                            aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                            aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                            this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                            this.DbContext.SubmitChanges();
                        }

                    }
                    #endregion

                }
                else
                {
                    if (aList.Count > 0)
                    {
                        #region

                        aPHIEU_YEU_CAU = aList[0];
                        aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];
                        int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                        if (ld > 0 && ld < 6)
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                        }
                        else
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                        }
                        if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                        {
                            aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                        }
                        aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];

                        aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                        this.DbContext.SubmitChanges();

                        #endregion

                        #region

                        List<PHIEUYEUCAU_THIETBI> ads = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(yc => yc.MA_PYC.Equals(values["MA_PYC"])).ToList();
                        foreach (PHIEUYEUCAU_THIETBI PYCTB in ads)
                        {
                            this.DbContext.PHIEUYEUCAU_THIETBIs.DeleteOnSubmit(PYCTB);
                            this.DbContext.SubmitChanges();
                        }

                        foreach (var item in aJObject["DS"])
                        {
                            string mpyc = values["MA_PYC"];
                            string serial = (string)item["SERIAL"];
                            List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(mpyc) && y.SERIAL.Equals(serial)).ToList();
                            if (kt.Count <= 0)
                            {
                                aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                                aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                                aPHIEUYEUCAU_THIETBI.MA_PYC = mpyc;
                                aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                                aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                                aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                                //aPHIEUYEUCAU_THIETBI.MO_TA
                                aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                                aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                                aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                                aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                                this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                                this.DbContext.SubmitChanges();
                            }

                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        #endregion

        #region yeu cau thu hoi thiet bi
        public ActionResult PhieuYeuCauThuHoi()
        {
            return View();
        }
        public AjaxFormResult saveYeuCauThuHoi(FormCollection values, string gpAllData)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             */
            PHIEU_YEU_CAU aPHIEU_YEU_CAU;
            PHIEUYEUCAU_THIETBI aPHIEUYEUCAU_THIETBI;
            JObject aJObject = JObject.Parse("{DS:" + gpAllData + "}");
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<PHIEU_YEU_CAU> aList = new List<PHIEU_YEU_CAU>();
                aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(values["MA_PYC"])).ToList();
                if (String.IsNullOrEmpty(values["ID"]))
                {
                    string maPYC = values["MA_PYC"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC = DungChung.TaoMaTuDong("PYCTH");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC)).ToList();
                        }
                    }
                    #region
                    aPHIEU_YEU_CAU = new PHIEU_YEU_CAU();
                    aPHIEU_YEU_CAU.MA_PYC = maPYC;
                    aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];

                    int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                    if (ld > 0 && ld < 6)
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                    }
                    else
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                    }

                    if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                    {
                        aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                    }
                    //aPHIEU_YEU_CAU.NGAY_CAP
                    aPHIEU_YEU_CAU.LOAI = PYC_LOAI.YEU_CAU_THU_HOI;
                    aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];
                    //aPHIEU_YEU_CAU.GHI_CHU = values["GHI_CHU_PYC"];
                    aPHIEU_YEU_CAU.NGAY_TAO = DateTime.Now;
                    aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                    aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(aPHIEU_YEU_CAU);
                    this.DbContext.SubmitChanges();
                    #endregion

                    #region
                    foreach (var item in aJObject["DS"])
                    {
                        string serial = (string)item["SERIAL"];
                        List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(maPYC) && y.SERIAL.Equals(serial)).ToList();
                        if (kt.Count <= 0)
                        {
                            aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                            aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                            aPHIEUYEUCAU_THIETBI.MA_PYC = maPYC;
                            aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                            aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                            aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                            //aPHIEUYEUCAU_THIETBI.MO_TA
                            aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                            aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                            aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                            aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                            this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                            this.DbContext.SubmitChanges();
                        }
                    }
                    #endregion

                }
                else
                {
                    if (aList.Count > 0)
                    {
                        #region

                        aPHIEU_YEU_CAU = aList[0];
                        aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];
                        int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                        if (ld > 0 && ld < 6)
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                        }
                        else
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                        }
                        if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                        {
                            aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                        }
                        aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];

                        aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                        this.DbContext.SubmitChanges();

                        #endregion

                        #region

                        List<PHIEUYEUCAU_THIETBI> ads = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(yc => yc.MA_PYC.Equals(values["MA_PYC"])).ToList();
                        foreach (PHIEUYEUCAU_THIETBI PYCTB in ads)
                        {
                            this.DbContext.PHIEUYEUCAU_THIETBIs.DeleteOnSubmit(PYCTB);
                            this.DbContext.SubmitChanges();
                        }

                        foreach (var item in aJObject["DS"])
                        {
                            string mpyc = values["MA_PYC"];
                            string serial = (string)item["SERIAL"];
                            List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(mpyc) && y.SERIAL.Equals(serial)).ToList();
                            if (kt.Count <= 0)
                            {
                                aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                                aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                                aPHIEUYEUCAU_THIETBI.MA_PYC = mpyc;
                                aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                                aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                                aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                                //aPHIEUYEUCAU_THIETBI.MO_TA
                                aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                                aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                                aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                                aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                                this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                                this.DbContext.SubmitChanges();
                            }

                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }
        #endregion

        #region yeu cau thanh ly thiet bi
        public ActionResult PhieuYeuCauThanhLy()
        {
            return View();
        }
        public AjaxFormResult saveYeuCauThanhLy(FormCollection values, string gpAllData)
        {
            /*
             * Ghi chú:
             * Loai = 1 : Yêu cầu cấp mới thiết bị
             * Loai = 2 : Yêu cầu sửa chữa thiết bị
             * Loai = 3 : Yêu cầu thu hồi thiết bị
             */
            PHIEU_YEU_CAU aPHIEU_YEU_CAU;
            PHIEUYEUCAU_THIETBI aPHIEUYEUCAU_THIETBI;
            JObject aJObject = JObject.Parse("{DS:" + gpAllData + "}");
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_THIET_BI> dsThietBi = this.DbContext.DM_THIET_BIs.ToList();
                string maNV = string.Empty;
                if (Request.Cookies["remember"] != null)
                {
                    maNV = Request.Cookies["remember"].Values.Get(2);
                }
                else
                {
                    maNV = string.Empty;
                }

                List<PHIEU_YEU_CAU> aList = new List<PHIEU_YEU_CAU>();
                aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(values["MA_PYC"])).ToList();
                if (String.IsNullOrEmpty(values["ID"]))
                {
                    string maPYC = values["MA_PYC"];
                    if (aList.Count > 0)
                    {
                        while (aList.Count <= 0)
                        {
                            maPYC = DungChung.TaoMaTuDong("PYCTL");
                            aList = this.DbContext.PHIEU_YEU_CAUs.Where(p => p.MA_PYC.Equals(maPYC)).ToList();
                        }
                    }
                    #region
                    aPHIEU_YEU_CAU = new PHIEU_YEU_CAU();
                    aPHIEU_YEU_CAU.MA_PYC = maPYC;
                    aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];

                    int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                    if (ld > 0 && ld < 6)
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                    }
                    else
                    {
                        aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                    }

                    if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                    {
                        aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                    }
                    //aPHIEU_YEU_CAU.NGAY_CAP
                    aPHIEU_YEU_CAU.LOAI = PYC_LOAI.YEU_CAU_THANH_LY;
                    aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];
                    //aPHIEU_YEU_CAU.GHI_CHU = values["GHI_CHU_PYC"];
                    aPHIEU_YEU_CAU.NGAY_TAO = DateTime.Now;
                    aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                    aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                    this.DbContext.PHIEU_YEU_CAUs.InsertOnSubmit(aPHIEU_YEU_CAU);
                    this.DbContext.SubmitChanges();
                    #endregion

                    #region
                    foreach (var item in aJObject["DS"])
                    {
                        string serial = (string)item["SERIAL"];
                        List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(maPYC) && y.SERIAL.Equals(serial)).ToList();
                        if (kt.Count <= 0)
                        {
                            aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                            aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                            aPHIEUYEUCAU_THIETBI.MA_PYC = maPYC;
                            aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                            aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                            aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                            //aPHIEUYEUCAU_THIETBI.MO_TA
                            aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                            aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                            aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                            aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                            aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                            this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                            this.DbContext.SubmitChanges();


                            List<DM_THIET_BI> aDM_THIET_BI = dsThietBi.Where(tb => tb.SERIAL.Equals(serial)).ToList();
                            if (aDM_THIET_BI.Count > 0)
                            {
                                DM_THIET_BI tb = aDM_THIET_BI[0];
                                tb.TRANG_THAI = 9;
                                this.DbContext.SubmitChanges();
                            }
                        }
                    }
                    #endregion

                }
                else
                {
                    if (aList.Count > 0)
                    {
                        #region

                        aPHIEU_YEU_CAU = aList[0];
                        aPHIEU_YEU_CAU.MA_NV = values["MA_NV_Value"];
                        int ld = String.IsNullOrEmpty(values["ID_LD_Value"]) ? 0 : Convert.ToInt32(values["ID_LD_Value"]);

                        if (ld > 0 && ld < 6)
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["ID_LD"];
                        }
                        else
                        {
                            aPHIEU_YEU_CAU.LY_DO = values["LY_DO"];
                        }
                        if (!String.IsNullOrEmpty(values["NGAY_DE_NGHI"]))
                        {
                            aPHIEU_YEU_CAU.NGAY_DE_NGHI = Convert.ToDateTime(values["NGAY_DE_NGHI"]);
                        }
                        aPHIEU_YEU_CAU.MO_TA = values["GHI_CHU_PYC"];

                        aPHIEU_YEU_CAU.NGAY_SUA = DateTime.Now;
                        aPHIEU_YEU_CAU.TRANG_THAI = PYC_TRANG_THAI.YEU_CAU_MOI;
                        this.DbContext.SubmitChanges();

                        #endregion

                        #region

                        List<PHIEUYEUCAU_THIETBI> ads = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(yc => yc.MA_PYC.Equals(values["MA_PYC"])).ToList();
                        foreach (PHIEUYEUCAU_THIETBI PYCTB in ads)
                        {
                            this.DbContext.PHIEUYEUCAU_THIETBIs.DeleteOnSubmit(PYCTB);
                            this.DbContext.SubmitChanges();
                        }

                        foreach (var item in aJObject["DS"])
                        {
                            string mpyc = values["MA_PYC"];
                            string serial = (string)item["SERIAL"];
                            List<PHIEUYEUCAU_THIETBI> kt = this.DbContext.PHIEUYEUCAU_THIETBIs.Where(y => y.MA_PYC.Equals(mpyc) && y.SERIAL.Equals(serial)).ToList();
                            if (kt.Count <= 0)
                            {
                                aPHIEUYEUCAU_THIETBI = new PHIEUYEUCAU_THIETBI();
                                aPHIEUYEUCAU_THIETBI.ID = Guid.NewGuid().ToString();
                                aPHIEUYEUCAU_THIETBI.MA_PYC = mpyc;
                                aPHIEUYEUCAU_THIETBI.SERIAL = serial;
                                aPHIEUYEUCAU_THIETBI.MA_CL = (string)item["MA_CL"];
                                aPHIEUYEUCAU_THIETBI.MA_KIEU = String.IsNullOrEmpty((string)item["MA_KIEU"]) ? 0 : Convert.ToInt32((string)item["MA_KIEU"]);
                                //aPHIEUYEUCAU_THIETBI.MO_TA
                                aPHIEUYEUCAU_THIETBI.GHI_CHU = (string)item["GHI_CHU"];
                                aPHIEUYEUCAU_THIETBI.NGAY_TAO = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.NGAY_SUA = DateTime.Now;
                                aPHIEUYEUCAU_THIETBI.SO_LUONG = 1;
                                aPHIEUYEUCAU_THIETBI.MA_NV = (string)item["MA_NV"];
                                aPHIEUYEUCAU_THIETBI.TRANG_THAI = true;
                                this.DbContext.PHIEUYEUCAU_THIETBIs.InsertOnSubmit(aPHIEUYEUCAU_THIETBI);
                                this.DbContext.SubmitChanges();

                                List<DM_THIET_BI> aDM_THIET_BI = dsThietBi.Where(tb => tb.SERIAL.Equals(serial)).ToList();
                                if (aDM_THIET_BI.Count > 0)
                                {
                                    DM_THIET_BI tb = aDM_THIET_BI[0];
                                    tb.TRANG_THAI = 9;
                                    this.DbContext.SubmitChanges();
                                }
                            }

                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }
        #endregion

        public ActionResult BaoHanh()
        {
            return View();
        }

        #endregion



        #region "Danh mục đơn vị tính"

        public ActionResult Dm_DonViTinh()
        {
            return View();
        }

        public AjaxStoreResult dsDonViTinh(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from dvt in this.DbContext.DM_DON_VI_TINHs
                         where SqlMethods.Like(dvt.TEN_DVT, "%" + LIB.ProcessStrVal(txtfilter) + "%") || dvt.MA_DVT.Equals(txtfilter)
                         select dvt);
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveDonViTinh(FormCollection values)
        {
            DM_DON_VI_TINH dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_DON_VI_TINH> aList = DbContext.DM_DON_VI_TINHs.Where(dvt => dvt.MA_DVT.Equals(values["MA_DVT"])).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    dm = new DM_DON_VI_TINH();
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã đơn vị tính đã tồn tài, vui lòng nhập mã khác !";
                    }
                    else
                    {
                        dm.MA_DVT = values["MA_DVT"];
                        dm.TEN_DVT = values["TEN_DVT"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        DbContext.DM_DON_VI_TINHs.InsertOnSubmit(dm);
                        DbContext.SubmitChanges();
                    }
                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_DVT = values["TEN_DVT"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        DbContext.SubmitChanges();
                    }
                }

            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaDonViTinh(string MA_DVT)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                DM_DON_VI_TINH aDM_DON_VI_TINH = (from dvt in this.DbContext.DM_DON_VI_TINHs
                                                  where dvt.MA_DVT == MA_DVT
                                                  select dvt).First();
                this.DbContext.DM_DON_VI_TINHs.DeleteOnSubmit(aDM_DON_VI_TINH);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        [HttpPost]
        public JsonResult KiemTraMaDVT(string ma)
        {
            try
            {
                List<DM_DON_VI_TINH> aList = this.DbContext.DM_DON_VI_TINHs.Where(d => d.MA_DVT.Equals(ma)).ToList();
                if (aList.Count > 0)
                {
                    return Json("TonTai", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Chua", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region "Danh mục nguồn kinh phí"

        public ActionResult Dm_NguonKinhPhi()
        {
            return View();
        }

        public AjaxStoreResult dsNguonKinhPhi(string txtfilter, int start, int limit)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;

            var query = (from nkp in this.DbContext.DM_NGUON_KINH_PHIs
                         where SqlMethods.Like(nkp.TEN_NKP, "%" + LIB.ProcessStrVal(txtfilter) + "%") || nkp.MA_NKP.Equals(txtfilter)
                         select nkp);
            int total = query.ToList().Count;
            query = query.Skip(start).Take(limit);
            return new AjaxStoreResult(query, total);
        }

        public AjaxFormResult saveNguonKinhPhi(FormCollection values)
        {
            DM_NGUON_KINH_PHI dm;
            AjaxFormResult response = new AjaxFormResult();
            try
            {
                List<DM_NGUON_KINH_PHI> aList = DbContext.DM_NGUON_KINH_PHIs.Where(nlp => nlp.MA_NKP.Equals(values["MA_NKP"])).ToList();
                if (String.IsNullOrEmpty(values["id"]))
                {
                    dm = new DM_NGUON_KINH_PHI();
                    if (aList.Count > 0)
                    {
                        response.Success = false;
                        response.ExtraParams["msg"] = "Mã nguồn kinh phí đã tồn tại, vui lòng nhập mã khác !";
                    }
                    else
                    {
                        dm.MA_NKP = values["MA_NKP"];
                        dm.TEN_NKP = values["TEN_NKP"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        DbContext.DM_NGUON_KINH_PHIs.InsertOnSubmit(dm);
                        DbContext.SubmitChanges();
                    }

                }
                else
                {
                    if (aList.Count > 0)
                    {
                        dm = aList[0];
                        dm.TEN_NKP = values["TEN_NKP"];
                        dm.MO_TA = values["MO_TA"];
                        dm.GHI_CHU = values["GHI_CHU"];
                        DbContext.SubmitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }
            return response;
        }

        public AjaxResult xoaNguonKinhPhi(string MA_NKP)
        {
            AjaxResult response = new AjaxResult();
            try
            {
                DM_NGUON_KINH_PHI aDM_NGUON_KINH_PHI = (from nkp in this.DbContext.DM_NGUON_KINH_PHIs
                                                        where nkp.MA_NKP == MA_NKP
                                                        select nkp).First();
                this.DbContext.DM_NGUON_KINH_PHIs.DeleteOnSubmit(aDM_NGUON_KINH_PHI);
                this.DbContext.SubmitChanges();
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.ToString();
            }
            return response;
        }

        #endregion



        public ActionResult PheDuyetYeuCau()
        {
            return View();
        }

        public ActionResult InPhieu()
        {
            return View();
        }

        public JsonResult TaoMaTuDong(string Ma)
        {
            try
            {
                string ma = DungChung.TaoMaTuDong(Ma);
                return Json(ma, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}
