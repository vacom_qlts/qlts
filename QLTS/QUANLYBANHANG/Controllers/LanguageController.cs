﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QUANLYBANHANG.Models;

namespace QUANLYBANHANG.Controllers
{
    public class LanguageController : BaseDataController
    {
        //
        // GET: /Language/

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeLanguage(string language)
        {
            ACCOUNT user = (from p in this.DbContext.ACCOUNTs
                            where p.Username == User.Identity.Name
                            select p).First();
            user.Language = language;            
            this.DbContext.SubmitChanges();

            Session["CurrentLanguage"] = new System.Globalization.CultureInfo(language);
            return this.RedirectToAction("Index", "Home");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeTheme(string theme)
        {
            ACCOUNT user = (from p in this.DbContext.ACCOUNTs
                            where p.Username == User.Identity.Name
                            select p).First();
            user.Theme = theme;
            this.DbContext.SubmitChanges();
            if (!string.IsNullOrEmpty(Session["themes"].ToString())) {
                Session["themes"] = null;
                Session["themes"] = theme;
            }           
            return this.RedirectToAction("Index", "Home");
        }

    }
}
