﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using QUANLYBANHANG.Models;
using CrystalDecisions.Shared.Json;
using System.Data.Linq.SqlClient;
using VACOMLIB;
using QUANLYBANHANG.Resources;

namespace QUANLYBANHANG.Controllers
{
    public class BaocaoController : BaseDataController
    {
        //
        // GET: /Baocao/

        public ActionResult Baocao1()
        {
            return View();
        }

        #region bang ke tong hop thiet bi
        public ActionResult BangKeTongHopThietBi()
        {
            return View();
        }


        public AjaxStoreResult dsBangKeTongHopThietBi(string txtfilter, int start, int limit,string trangthai)
        {
            txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
            start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
            limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
            int tt1 = String.IsNullOrEmpty(trangthai) ? 0 : Convert.ToInt32(trangthai);

            List<THIET_BI_EN> dsThietBi = (from tb in this.DbContext.DM_THIET_BIs
                         join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                         join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                         join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT

                         where (SqlMethods.Like(tb.TEN_TB, "%" + LIB.ProcessStrVal(txtfilter) + "%") 
                         || tb.SERIAL.Equals(txtfilter) || tb.MA_TB.Equals(txtfilter)) && (tt1 == 0 ? tb.TRANG_THAI > 0 : tb.TRANG_THAI.Equals(tt1))

                         orderby tt.MA_TT
                         select new THIET_BI_EN()
                         {
                             MA_TB=tb.MA_TB,
                             SERIAL=tb.SERIAL,
                             TEN_TB=tb.TEN_TB,
                             MA_CL=tb.MA_CL,
                             MA_KIEU=tb.MA_KIEU,
                             NGAY_BH=tb.NGAY_BH,
                             THOI_GIAN_BH=tb.THOI_GIAN_BH,
                             THOI_GIAN_BHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_BH), Convert.ToInt32(tb.THOI_GIAN_BH)),

                             NGAY_KH=tb.NGAY_KH,
                             THOI_GIAN_KH=tb.THOI_GIAN_KH,
                             THOI_GIAN_KHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_KH), Convert.ToInt32(tb.THOI_GIAN_KH)),
                             TRANG_THAI=tb.TRANG_THAI,
                             NGUYEN_GIA=tb.NGUYEN_GIA,
                             TEN_CL=cl.TEN_CL,
                             TEN_KIEU=k.TEN_KIEU,
                             TEN_TT=tt.TEN_TT
                         }).ToList();

            Session["BANG_KE_TB"] = dsThietBi;

            int total = dsThietBi.ToList().Count;
            dsThietBi = dsThietBi.Skip(start).Take(limit).ToList();
            return new AjaxStoreResult(dsThietBi, total);
        }
        #endregion

        #region tinh hinh su dung thiet bi
            public ActionResult TinhHinhSuDungThietBi()
            {
                return View();
            }


            public AjaxStoreResult dsTinhHinhSuDungThietBi(string txtfilter, int start, int limit, string donvi,string phongban)
            {
                txtfilter = String.IsNullOrEmpty(txtfilter) ? string.Empty : txtfilter;
                start = String.IsNullOrEmpty(Convert.ToString(start)) ? 0 : start;
                limit = String.IsNullOrEmpty(Convert.ToString(limit)) ? 0 : limit;
                donvi = String.IsNullOrEmpty(donvi) ? string.Empty : donvi;
                phongban = String.IsNullOrEmpty(phongban) ? string.Empty : phongban;

                List<THIET_BI_EN> dsThietBi = (
                                               from xk in this.DbContext.XUATKHO_TBs
                                               join pyc in this.DbContext.PHIEUYEUCAU_THIETBIs on xk.MA_PYC equals pyc.MA_PYC
                                               join tb in this.DbContext.DM_THIET_BIs on xk.MA_TB equals tb.MA_TB
                                               join cl in this.DbContext.DM_CHUNGLOAIs on tb.MA_CL equals cl.MA_CL
                                               join k in this.DbContext.DM_KIEUs on tb.MA_KIEU equals k.MA_KIEU
                                               join tt in this.DbContext.DM_TRANGTHAIs on tb.TRANG_THAI equals tt.MA_TT
                                               join nv in this.DbContext.NHANVIENs on pyc.MA_NV equals nv.MA_NV
                                               join dv in this.DbContext.DM_DONVIs on nv.MA_DV equals dv.MA_DV
                                               join pb in this.DbContext.DM_PHONG_BANs on nv.MA_PB equals pb.MA_PB


                                               where (SqlMethods.Like(nv.TEN_NV, "%" + LIB.ProcessStrVal(txtfilter) + "%")
                                               || nv.MA_NV.Equals(txtfilter))
                                               &&(String.IsNullOrEmpty(donvi) ? dv.MA_DV.Contains(donvi) : dv.MA_DV.Equals(donvi))
                                               && (String.IsNullOrEmpty(phongban) ? pb.MA_PB.Contains(phongban) : pb.MA_PB.Equals(phongban))
                                               
                                               orderby pb.TEN_PB
                                               select new THIET_BI_EN()
                                               {
                                                   MA_TB = tb.MA_TB,
                                                   SERIAL = tb.SERIAL,
                                                   TEN_TB = tb.TEN_TB,
                                                   MA_CL = tb.MA_CL,
                                                   MA_KIEU = tb.MA_KIEU,
                                                   NGAY_BH = tb.NGAY_BH,
                                                   THOI_GIAN_BH = tb.THOI_GIAN_BH,
                                                   THOI_GIAN_BHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_BH), Convert.ToInt32(tb.THOI_GIAN_BH)),

                                                   NGAY_KH = tb.NGAY_KH,
                                                   THOI_GIAN_KH = tb.THOI_GIAN_KH,
                                                   THOI_GIAN_KHCL = DungChung.TinhThoiGianBaoHanh(Convert.ToDateTime(tb.NGAY_KH), Convert.ToInt32(tb.THOI_GIAN_KH)),
                                                   TRANG_THAI = tb.TRANG_THAI,
                                                   NGUYEN_GIA = tb.NGUYEN_GIA,
                                                   TEN_CL = cl.TEN_CL,
                                                   TEN_KIEU = k.TEN_KIEU,
                                                   TEN_TT = tt.TEN_TT,
                                                   TEN_DV=dv.TEN_DV,
                                                   TEN_PB=pb.TEN_PB,
                                                   TEN_NV=nv.TEN_NV
                                               }).ToList();

                Session["TINH_HINH_SDTB"] = dsThietBi;

                int total = dsThietBi.ToList().Count;
                dsThietBi = dsThietBi.Skip(start).Take(limit).ToList();
                return new AjaxStoreResult(dsThietBi, total);
            }
            
        #endregion

    }
}
