﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Xml;

using QUANLYBANHANG.Models;
using Ext.Net;
using System.Data.Linq.SqlClient;
using VACOMLIB;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using QUANLYBANHANG.Resources;
using Newtonsoft.Json.Linq;
using QUANLYBANHANG.Controllers;
using System.Text.RegularExpressions;

namespace QUANLYBANHANG.Resources
{
    public static class DungChung
    {
        public static string TinhThoiGianBaoHanh(DateTime NBH,int TG)
        {
            string text = string.Empty;
            int ngay = NBH.AddMonths(TG).Subtract(DateTime.Now).Days;
            if (ngay < 0)
            {
                text = "Hết hạn bảo hành";
            }
            else
            {
                int thang = ngay / 30;
                if (thang > 0)
                {
                    text =Convert.ToString(thang)+ " tháng";
                }
                else
                {
                    text = Convert.ToString(ngay) + " ngày";
                }
            }
            return text;
        }

        public static string TinhThoiGianKhauHao(DateTime NKH, int TG)
        {
            string text = string.Empty;
            int ngay = NKH.AddMonths(TG).Subtract(DateTime.Now).Days;
            if (ngay < 0)
            {
                text = "Hết thời gian khấu hao";
            }
            else
            {
                int thang = ngay / 30;
                if (thang > 0)
                {
                    text = Convert.ToString(thang) + " tháng";
                }
                else
                {
                    text = Convert.ToString(ngay) + " ngày";
                }
            }
            return text;
        }

        public static DateTime ConvertStringToDate(string date, string format)
        {
            try
            {
                //format =dd/MM/yyyy
                return DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                ex.ToString();
                DateTime aDate = new DateTime();
                return aDate;
            }
        }

        public static bool IsNumber(string pText)
        {
            try
            {
                Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
                return regex.IsMatch(pText);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }

        }

        //tao ma tu dong
        public static string TaoMaTuDong(string Loai)
        {
            string maPhieu = string.Empty;
            string Ma = string.Empty;
            try
            {
                string path = HttpContext.Current.Server.MapPath("~") + "Library";
                if (System.IO.File.Exists(path + "\\DinhDangMa.xml"))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(path + "\\DinhDangMa.xml");
                    XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/MaSo/MaPhieu");
                    string TienTo = "", PhanCach = "", HauTo = "", HauTo1 = "";
                    int trangthai = 0;

                    foreach (XmlNode node in nodeList)
                    {
                        string tLoai = node.Attributes["Loai"].InnerText;
                        if (tLoai.Equals(Loai))
                        {
                            TienTo = node.Attributes["TienTo"].InnerText;
                            PhanCach = node.Attributes["PhanCach"].InnerText;
                            HauTo = node.Attributes["HauTo"].InnerText;
                            break;
                        }
                        else
                        {
                            maPhieu = string.Empty;
                        }
                    }

                    BaseDataController aBaseDataController = new BaseDataController();
                    List<PHIEU_YEU_CAU> aList = aBaseDataController.DbContext.PHIEU_YEU_CAUs
                        .Where(p => p.MA_PYC.Contains((TienTo+PhanCach+DateTime.Now.Year.ToString()+PhanCach))).OrderByDescending(p=>p.MA_PYC).ToList();
                    if (aList.Count > 0)
                    {
                        Ma = aList[0].MA_PYC;
                        int ht1= aList[0].MA_PYC.Length;
                        int ht2 = Convert.ToInt32(HauTo);

                        if (ht1 >= ht2)
                        {

                            if (IsNumber(Ma.Substring((ht1-ht2),ht2)) == true)
                            {
                                HauTo1 = Ma.Substring((ht1 - ht2), ht2);
                                trangthai = 1;//tao ra ma phieu tiep theo
                            }
                            else
                            {
                                trangthai = 0;//tao ra ma phieu dau tien la 1
                            }
                        }
                        else
                        {
                            trangthai = 0;//tao ra ma phieu dau tien la 1
                        }
                    }
                    else
                    {
                        trangthai = 0;//tao ra ma phieu dau tien la 1
                    }

                    TienTo = TienTo + PhanCach + DateTime.Now.Year.ToString() + PhanCach;
                    if (trangthai == 1)
                    {
                        decimal ht = Convert.ToDecimal(HauTo1);
                        int dodai = Convert.ToInt32(HauTo);
                        ht = ht + 1;
                        for (int j = 1; j <= dodai; j++)
                        {
                            if (Convert.ToString(ht).Length == j)
                            {
                                if ((dodai - j) == 0)
                                {
                                    HauTo = Convert.ToString(ht);
                                }
                                else
                                {
                                    string ll = string.Empty;
                                    for (int l = 0; l < (dodai - j); l++)
                                    {
                                        ll = ll + "0";
                                    }
                                    HauTo = ll + Convert.ToString(ht);
                                }
                                break;
                            }
                        }
                        maPhieu = TienTo + HauTo;
                    }
                    else
                    {
                        string ll = string.Empty;
                        int dodai = Convert.ToInt32(HauTo);
                        for (int l = 0; l < (dodai - 1); l++)
                        {
                            ll = ll + "0";
                        }
                        maPhieu = TienTo + ll + "1";
                    }
                }
                else
                {
                    maPhieu = string.Empty;
                }
                return maPhieu;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return maPhieu;
            }
        }
    }
}