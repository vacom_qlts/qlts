﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QUANLYBANHANG.Resources
{
    public static class PYC_TRANG_THAI
    {
        public static int YEU_CAU_MOI = 1;//Phiếu yêu cầu mới
        public static int CHO_DUYET = 2;//Đang chờ phê duyệt
        public static int KHONG_DUYET = 3;//Không phê duyệt
        public static int DA_DUYET = 4;//Đã phê duyệt
        public static int CHO_CAP_TB = 5;//Chờ cấp thiết bị
        public static int DA_CAP_TB = 6;//Đã cấp thiết bị

        public static int BAO_HANH = 7;//Duyệt đi bảo hành thiết bị
        public static int SUA_CHUA = 8;//Duyệt sửa chữa thiết bị
        public static int THU_HOI_TB = 9;//Kho đã nhận lại thiết bị
        public static int DA_CAP_TLTB = 10;//Kho đã cấp thanh lý thiết bị

        public static int KET_THUC = 15;//Kết thúc yêu cầu





        public static string HienThi(int gt)
        {
            string dp=string.Empty;
            if(gt==1)
            {
                dp = "Phiếu yêu cầu mới";
            }
            else if(gt==2)
            {
                dp = "Đang chờ phê duyệt";
            }
            else if (gt == 3)
            {
                dp = "Không phê duyệt";
            }
            else if (gt == 4)
            {
                dp = "Đã phê duyệt";
            }
            else if (gt == 5)
            {
                dp = "Chờ kho cấp thiết bị.";
            }
            else if (gt == 6)
            {
                dp = "Kho đã cấp thiết bị";
            }
            else if (gt == 7)
            {
                dp = "Bảo hành thiết bị";
            }
            else if (gt == 8)
            {
                dp = "Sửa chữa thiết bị";
            }
            else if(gt==9)
            {
                dp="Kho đã nhận lại thiết bị";
            }
            else if(gt==10)
            {
                dp = "Kho đã cấp thanh lý thiết bị";
            }
            else if (gt == 15)
            {
                dp = "Kết thúc yêu cầu";
            }
            else
            {
                dp = "Không xác định";
            }
            return dp;
        }
    }
}