﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QUANLYBANHANG.Models
{
    public class THIET_BI_EN : DM_THIET_BI
    {
        public String TEN_DV { get; set; }
        public String TEN_PB { get; set; }
        public String TEN_NV { get; set; }
        public String TEN_CL { get; set;}
        public String TEN_KIEU { get; set; }
        public String TEN_TT { get; set; }
        public String THOI_GIAN_BHCL { get; set; }
        public String THOI_GIAN_KHCL { get; set; }
    }
}